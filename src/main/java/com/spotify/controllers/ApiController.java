package com.spotify.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.Date;

import java.util.*;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import com.spotify.entities.*;
import com.spotify.entityManagers.SongCollectionManager;
import com.spotify.entityManagers.UserManager;
import com.spotify.forms.*;
import com.spotify.services.*;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.*;

import com.spotify.ApiResponse;



@RestController
@RequestMapping("/api")
@MultipartConfig(maxFileSize = 1024*1024*32)
public class ApiController {

	public static String UPLOAD_DIR = "src/main/resources/static/images/";
    public static String UPLOAD_DIR2 = "src/main/resources/static/songs/";
    public static String STATUS_ERROR = "error";
    public static String STATUS_OK = "ok";
    public static String SUCCESS = "Success";
    public static String PLAYLIST_NO_ID = "There is no playlist with this ID.";
    public static String ARTIST_NO_ID = "There is no artist with this ID.";
    public static String PLAYLIST_CREATION_ERROR = "Could not create playlist.";
    public static String NOT_LOGGED_IN = "Cannot perform this action as the user is not logged in.";
    public static String NO_PLAYLIST_SESSION = "no playlist in session";
    public static String OLD_PASSWORD_ERROR = "Old password that has been entered is invalid";
    public static String NEW_PASSWORD_ERROR = "Password was not able to be changed.Please try again";
    public static String ALBUM_ID_NOT_EXIST = "There is no album associated with the provided id.";
    public static String ERROR_CREATING_SONG = "There was an error adding the song to the database.";
    public static String NO_LANG_CHANGE = "Language picked is already the user's language.";
    public static String ERROR_CREATING_USER = "There was an error creating the user.";
    
    @Autowired
    @Qualifier("playlistServices")
    protected PlaylistServices playlistServices; 
    @Autowired
    @Qualifier("songServices")
    protected SongServices songServices;
    @Autowired
    @Qualifier("userServices")
    protected UserServices userServices; 
    @Autowired
    @Qualifier("generalServices")
    protected GeneralServices generalServices;
    @Autowired
    @Qualifier("artistServices")
    protected ArtistServices artistServices;
    @Autowired
    @Qualifier("albumServices")
    protected AlbumServices albumServices;
    private UserManager um = UserManager.getUserManager();
    private SongCollectionManager scm = SongCollectionManager.getSongCollectionManager();

    @RequestMapping(value = "/playlist/{id}", method= RequestMethod.GET)
    public Object getPlaylist(@PathVariable int id, HttpSession httpSession) {
        Playlist playlist = playlistServices.getPlaylist(id);
        
        if (playlist == null) {
            return new ApiResponse(STATUS_ERROR, PLAYLIST_NO_ID);
        }
        playlist.setSongs(songServices.getSongsByPlaylistId(playlist.getPlaylistId()));
        /* 
         * gets a list of artist that belong to this song
         * also gets the album that this song belongs to
         * */
        for(Song s : playlist.getSongs()) {
        	s.setArtists(artistServices.getArtistBySong(s.getId()));
        }
        playlist.setOwners((userServices.getOwner(playlist.getPlaylistId())));
        playlist.setFollowers(generalServices.getFollowers(playlist.getPlaylistId(), 'p'));
        playlist.setFollowCount(playlist.getFollowers().size() - 1);
        httpSession.setAttribute("playlist", playlist);
        return playlist;
    }

    @RequestMapping(value = "/showAdvertisements", method= RequestMethod.GET)
    public Object showAdvertisements(HttpSession httpSession) {
        return generalServices.getAdvertisements();

    }

    @RequestMapping(value = "/removeAdvertisement", method= RequestMethod.POST)
    public Object removeAdvertisements(@RequestBody String id, HttpSession httpSession) {

        System.out.println(id);
        JSONObject obj;
        int adId = 0;
        Advertisement ad = null;


        try
        {
            obj = new JSONObject(id);
            adId = obj.getInt("adId");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
         ad = generalServices.getAdvertisement(adId);
        return generalServices.removeAdvertisement(ad);

    }

    @RequestMapping(value = "/createAdvertisement", method = RequestMethod.POST)
    public Object createAdvertisement(HttpSession httpSession, HttpServletRequest req, @Valid AddAdvertisementForm advertisementForm, HttpServletResponse res) {

        User user = null;
        int val = 0;
        if (httpSession.getAttribute("user") == null){
            return new ApiResponse(STATUS_ERROR, NOT_LOGGED_IN);
        }
        user = (User) httpSession.getAttribute("user");
        Advertisement ad = new Advertisement();
        ad.setStatus(0);
        String fileName = advertisementForm.getAdvertisementImage().getOriginalFilename();
        String filePath = UPLOAD_DIR + fileName;
        String userDir = System.getProperty("user.dir") + "/";
        File image = new File(userDir + filePath);
        String newFileName = "";
        try {
            if (!image.exists()) {
                image.createNewFile();
            }
            else {
                int i = 1;
                String fileNameNoExtension = fileName.substring(0, fileName.lastIndexOf("."));
                String fileNameExtension = fileName.substring(fileName.lastIndexOf("."));
                newFileName = fileNameNoExtension + i;
                image = new File(userDir + UPLOAD_DIR + newFileName + fileNameExtension);
                while(image.exists()) {
                    i += 1;
                    newFileName = fileNameNoExtension + i;
                    image = new File(userDir + UPLOAD_DIR + newFileName + fileNameExtension);
                }
                image.createNewFile();
                fileName = newFileName + fileNameExtension;
            }
           advertisementForm.getAdvertisementImage().transferTo(image);
            ad.setImageLoc(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        generalServices.createAdvertisement(ad);
            return ad;
        }




    @RequestMapping(value = "/user", method= RequestMethod.GET)
    public Object getUserInformation(HttpSession httpSession) {
        User currentUser = (User) httpSession.getAttribute("user");
        Map<String,Object> userInfo=new HashMap<String, Object>();
        List<Playlist> playlists = new ArrayList<Playlist>();
        playlists = (List<Playlist>) playlistServices.getPlaylists(currentUser.getId());
                for (Playlist playlist : playlists){
                    playlist.setOwners((userServices.getOwner(playlist.getPlaylistId())));
                    playlist.setSongs(songServices.getSongsByPlaylistId(playlist.getPlaylistId()));
                }
        userInfo.put("playlists",  playlists);
        userInfo.put("firstName", currentUser.getFirstName());
        userInfo.put("lastName", currentUser.getLastName());
        userInfo.put("userID", currentUser.getId());
        userInfo.put("premium", currentUser.isPremium());
        userInfo.put("admin", currentUser.isAdmin());
        userInfo.put("isArtist", userServices.isArtist(currentUser.getId()));
        userInfo.put("artistInfo", userServices.getArtistByUserId(currentUser.getId()));
        userInfo.put("email", currentUser.getEmail());
        userInfo.put("library", currentUser.getLibrary());
        userInfo.put("queue", currentUser.getQueue());
        userInfo.put("dob", currentUser.getDob());
        userInfo.put("lang", currentUser.getLang());
        userInfo.put("lastSong", currentUser.getLastSong());
        userInfo.put("followingArtist", userServices.getFollowingArtist(currentUser.getId()));
        userInfo.put("friends", userServices.getFollowing((currentUser.getId())));
        System.out.println(Boolean.toString((currentUser.isPremium())));
        return userInfo;
    }

    @RequestMapping(value = "/newPlaylist", method = RequestMethod.POST)
    public Object newPlaylist(HttpSession httpSession, HttpServletRequest req, @Valid CreatePlaylistForm playlistForm, HttpServletResponse res) {
        User user = null;
        int val = 0;
    	if (httpSession.getAttribute("user") == null){
            return new ApiResponse(STATUS_ERROR, NOT_LOGGED_IN);
        }       
        user = (User) httpSession.getAttribute("user");
        val = playlistServices.createPlaylistService(playlistForm, user);
        if (val == -1) {
            return new ApiResponse(STATUS_ERROR, PLAYLIST_CREATION_ERROR);
        }
        else {
            Playlist playlist = playlistServices.getPlaylist(val);

                playlist.setOwners((userServices.getOwner(playlist.getPlaylistId())));
                playlist.setSongs(songServices.getSongsByPlaylistId(playlist.getPlaylistId()));
//            Map<String, String> playlistId = new HashMap<String, String>();
//            playlistId.put("playlistId", val + "");
//            playlistId.put("imageLoc", playlist.getImageLoc());
//            playlistId.put("songs", playlist.getSongs());

            return playlist;
        }
    }

    @RequestMapping(value = "/addProfileImage", method = RequestMethod.POST)
    public Object addProfileImage(HttpSession httpSession, HttpServletRequest req, @Valid AddProfileImageForm profileImageForm, HttpServletResponse res) {
        User user = null;
        user = (User) httpSession.getAttribute("user");
        int val = 0;
        if (httpSession.getAttribute("user") == null){
            return "";
        }

        String fileName = profileImageForm.getProfileImage().getOriginalFilename();
        String filePath = UPLOAD_DIR + fileName;
        String userDir = System.getProperty("user.dir") + "/";
        File image = new File(userDir + filePath);
        String newFileName = "";
        try {
            if (!image.exists()) {
                image.createNewFile();
            }
            else {
                int i = 1;
                String fileNameNoExtension = fileName.substring(0, fileName.lastIndexOf("."));
                String fileNameExtension = fileName.substring(fileName.lastIndexOf("."));
                newFileName = fileNameNoExtension + i;
                image = new File(userDir + UPLOAD_DIR + newFileName + fileNameExtension);
                while(image.exists()) {
                    i += 1;
                    newFileName = fileNameNoExtension + i;
                    image = new File(userDir + UPLOAD_DIR + newFileName + fileNameExtension);
                }
                image.createNewFile();
                fileName = newFileName + fileNameExtension;
            }
            profileImageForm.getProfileImage().transferTo(image);
            user.setUserImageLoc(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        userServices.updateUser(user);
//        String file = "{\"fileName\": \"" + fileName + "\"}";
//        JSONObject obj;
//        obj= new JSONObject(file);

            return user;
        }
    @RequestMapping(value = "/setPlaylistImage", method = RequestMethod.POST)
    public Object setPlaylistImage(HttpSession httpSession, HttpServletRequest req, @Valid UpdatePlaylistForm updatePlaylistForm, HttpServletResponse res) {
        Playlist p = (Playlist) httpSession.getAttribute("playlist");

        String fileName = updatePlaylistForm.getPlaylistImage().getOriginalFilename();
        String filePath = UPLOAD_DIR + fileName;
        String userDir = System.getProperty("user.dir") + "/";
        File image = new File(userDir + filePath);
        String newFileName = "";
        try {
            if (!image.exists()) {
                image.createNewFile();
            }
            else {
                int i = 1;
                String fileNameNoExtension = fileName.substring(0, fileName.lastIndexOf("."));
                String fileNameExtension = fileName.substring(fileName.lastIndexOf("."));
                newFileName = fileNameNoExtension + i;
                image = new File(userDir + UPLOAD_DIR + newFileName + fileNameExtension);
                while(image.exists()) {
                    i += 1;
                    newFileName = fileNameNoExtension + i;
                    image = new File(userDir + UPLOAD_DIR + newFileName + fileNameExtension);
                }
                image.createNewFile();
                fileName = newFileName + fileNameExtension;
            }
            updatePlaylistForm.getPlaylistImage().transferTo(image);
            p.setImageLoc(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
//        String file = "{\"fileName\": \"" + fileName + "\"}";
//        JSONObject obj;
//        obj= new JSONObject(file);

        return p;
    }
    @RequestMapping(value = "/removeProfilePic", method = RequestMethod.POST)
    public String  removeProfilePic(HttpSession httpSession, HttpServletRequest req, HttpServletResponse res) {
        User user = null;
        user = (User) httpSession.getAttribute("user");
        user.setUserImageLoc("");
        userServices.updateUser(user);

        return "";
    }

    @RequestMapping(value = "/getArtistAlbums", method = RequestMethod.GET)
    public Object  getArtistAlbums(HttpSession httpSession, HttpServletRequest req, HttpServletResponse res) {
        User u = null;
        Artist a = null;

        u = (User) httpSession.getAttribute("user");
        a = userServices.getArtistByUserId(u.getId());

        return artistServices.getArtistAlbumList(a.getArtistID());

    }


    @RequestMapping(value = "/newSong", method = RequestMethod.POST)
    public Object newSong(HttpSession httpSession, HttpServletRequest req, @Valid AddSongForm addSongForm) {
        User user = null;
        if (httpSession.getAttribute("user") == null){
            return new ApiResponse(STATUS_ERROR, NOT_LOGGED_IN);
        }
        //Should add a check to see if the user is an artist

        int albumId = addSongForm.getALBUM_ID();
        Song s = new Song();
        s.setTitle(addSongForm.getSongTitle());
        s.setLength(addSongForm.getSongLength());
            Date songDate = new Date(new java.util.Date().getTime());
            s.setDateAdded(songDate);
        s.setLyrics(addSongForm.getLyrics());
        s.setNumPlays(0);
//        s.setSongLocation(addSongForm.getSongFile().getName());
        String fileName = addSongForm.getSongFile().getOriginalFilename();
        String filePath = UPLOAD_DIR2 + fileName;
        String userDir = System.getProperty("user.dir") + "/";
        File image = new File(userDir + filePath);
        String newFileName = "";
        try {
            if (!image.exists()) {
                image.createNewFile();
            }
            else {
                int i = 1;
                String fileNameNoExtension = fileName.substring(0, fileName.lastIndexOf("."));
                String fileNameExtension = fileName.substring(fileName.lastIndexOf("."));
                newFileName = fileNameNoExtension + i;
                image = new File(userDir + UPLOAD_DIR2 + newFileName + fileNameExtension);
                while(image.exists()) {
                    i += 1;
                    newFileName = fileNameNoExtension + i;
                    image = new File(userDir + UPLOAD_DIR2 + newFileName + fileNameExtension);
                }
                image.createNewFile();
                fileName = newFileName + fileNameExtension;
            }
            addSongForm.getSongFile().transferTo(image);
            s.setSongLocation(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Album album = albumServices.getAlbumById(albumId);
        if (album == null)
            return new ApiResponse(STATUS_ERROR, ALBUM_ID_NOT_EXIST);
        s.setAlbum(album);
        int val = songServices.createSong(s);
        if (val == -1)
            return new ApiResponse(STATUS_ERROR, ERROR_CREATING_SONG);

        return val;


    }
    
    @RequestMapping(value = "/unfollowPlaylist", method = RequestMethod.POST)
    public Object unfollowPlaylist(HttpSession httpSession, HttpServletRequest req, HttpServletResponse res) {
        Playlist p = null;
        User u = null;
        boolean result = false;
        
    	if (httpSession.getAttribute("playlist") == null){
            return new ApiResponse(STATUS_ERROR, NO_PLAYLIST_SESSION);
        }
        p = (Playlist) httpSession.getAttribute("playlist");
        u = (User) httpSession.getAttribute("user");
        result = generalServices.unfollow(p.getPlaylistId(), u.getId(), 'p');
        return result;
    }
    
    @RequestMapping(value = "/followPlaylist", method = RequestMethod.POST)
    public Object followPlaylist(HttpSession httpSession, HttpServletRequest req, HttpServletResponse res) {
    	Playlist p = null;
        User u = null;
        boolean result = false;
        
        if (httpSession.getAttribute("playlist") == null){
            return new ApiResponse(STATUS_ERROR, NO_PLAYLIST_SESSION);
        }
        p = (Playlist) httpSession.getAttribute("playlist");
        u = (User) httpSession.getAttribute("user");
        result = generalServices.follow(u, null, p, null);
        return result;
    }
    
    @RequestMapping(value = "/renamePlaylist", method = RequestMethod.POST)
    public Object renamePlaylist(HttpSession httpSession, HttpServletRequest req, HttpServletResponse res, @RequestBody RenamePlaylistForm renamePlaylistForm) {
    	Playlist p = null;
        User u = null;
        boolean result = false;
        
        if (httpSession.getAttribute("playlist") == null){
            return new ApiResponse(STATUS_ERROR, NO_PLAYLIST_SESSION);
        }
        p = (Playlist) httpSession.getAttribute("playlist");
        u = (User) httpSession.getAttribute("user");
        result = playlistServices.renamePlaylist(u, p, renamePlaylistForm.getNewPlaylistName());
        return result;
    }
    
    @RequestMapping(value = "/addSongToPlaylist", method = RequestMethod.POST)
    public Object addSongToPlaylist(@RequestBody String id, HttpSession httpSession, HttpServletRequest req, HttpServletResponse res) {
        System.out.println(id);
        JSONObject obj;
        int sid = 0;
        int pid = 0;

        try
        {
            obj = new JSONObject(id);
            sid = obj.getInt("songId");
            pid = obj.getInt("playlistId");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
//    	int pid = Integer.parseInt((String) req.getAttribute("playlistId"));
//    	int sid = Integer.parseInt((String) req.getAttribute("songId"));
    	Song s = songServices.getSongById(sid);
    	Playlist p = playlistServices.getPlaylist(pid);
    	boolean result = generalServices.addSongToCollection(p, null, s);
        return result;
    }
    
    @RequestMapping(value = "/removeSongFromPlaylist", method = RequestMethod.POST)
    public Object removeSongFromPlaylist(@RequestBody String id,HttpSession httpSession, HttpServletRequest req, HttpServletResponse res) throws IOException {
    	Playlist p = null;
        Song s = null;
        boolean result = false;
        JSONObject obj;
        int sid = 0;
        
        if (httpSession.getAttribute("playlist") == null){
            return new ApiResponse(STATUS_ERROR, NO_PLAYLIST_SESSION);
        }
        p = (Playlist) httpSession.getAttribute("playlist");
        
		try 
		{
			obj = new JSONObject(id);
			sid = obj.getInt("song");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        System.out.println(sid);
        result = generalServices.removeSongFromCollection(sid, p.getPlaylistId(), 'p');
        return result;
    }
    
    @RequestMapping(value = "/unfollowUser", method = RequestMethod.POST)
    public Object unfollowUser(@RequestBody String id,HttpSession httpSession, HttpServletRequest req, HttpServletResponse res) throws IOException {
        User u = null;
    	boolean result = false;
        JSONObject obj;
        int uid = 0;
        
        if (httpSession.getAttribute("user") == null){
            return new ApiResponse(STATUS_ERROR, NO_PLAYLIST_SESSION);
        }
        u= (User) httpSession.getAttribute("user");
		try 
		{
			obj = new JSONObject(id);
			uid = obj.getInt("unfollowId");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        System.out.println(uid);
        result = generalServices.unfollow(uid, u.getId(), 'u');
        return result;
    }
    
    @RequestMapping(value = "/followUser", method = RequestMethod.POST)
    public Object followUser(@RequestBody String id,HttpSession httpSession, HttpServletRequest req, HttpServletResponse res) throws IOException {
        User u = null;
        User tempUser;
    	boolean result = false;
        JSONObject obj;
        int uid = 0;
        
        if (httpSession.getAttribute("user") == null){
            return new ApiResponse(STATUS_ERROR, NO_PLAYLIST_SESSION);
        }
        u= (User) httpSession.getAttribute("user");
		try 
		{
			obj = new JSONObject(id);
			uid = obj.getInt("followId");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		tempUser = userServices.getUserById(uid);	
        result = generalServices.follow(u, tempUser, null, null);
        return result;
    }
    
    @RequestMapping(value = "/playSong", produces = { "audio/mpeg" }, method = RequestMethod.GET)
    public Object playSong(HttpSession httpSession, HttpServletRequest req, HttpServletResponse res) throws IOException {
        FileInputStream song = new FileInputStream("/home/scott/SpotifySample.mp3");
        byte buffer[] = new byte[250000];
        song.read(buffer);
        return buffer;
    }
    
    @RequestMapping(value = "/upgradeToPremium", method = RequestMethod.POST)
    public Object upgradeToPremium(HttpSession httpSession, HttpServletRequest req, HttpServletResponse res, @RequestBody PremiumUserForm premiumUserForm) {
        User u = null;
        boolean result = false;
        
        u = (User) httpSession.getAttribute("user");
        System.out.println(u);
        result = userServices.upgradePremium(u , premiumUserForm);
        if(result){
        	httpSession.setAttribute("user", u);
        	System.out.println(u.isPremium());
        }
        return result;
    }

    @RequestMapping(value = "/cancelPremium", method = RequestMethod.POST)
    public String cancelPremium(HttpSession httpSession, HttpServletRequest req, HttpServletResponse res) {
        User u = null;
        boolean result = false;

        u = (User) httpSession.getAttribute("user");
        System.out.println(u);
        result = userServices.downgradePremium(u);
        if(result){
            httpSession.setAttribute("user", u);
            System.out.println(u.isPremium());
        }
        return "";
    }

    @RequestMapping(value="/deleteAccount", method = RequestMethod.GET)
    public String deleteAccount(HttpSession httpSession, HttpServletResponse res)
    {
    	System.out.println("entering deleting method");
    	User u = null;
    	boolean result = false;
    	
    	u = (User) httpSession.getAttribute("user");
    	result = userServices.deleteAccount(u);	
    	try 
    	{
			res.sendRedirect("/logout");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
    }
    
    @RequestMapping(value = "/artist/{id}", method= RequestMethod.GET)
    public Object getArtistById(@PathVariable int id, HttpSession httpSession) {
        Artist artist = artistServices.getArtistById(id);
        if (artist == null) {
            return new ApiResponse(STATUS_ERROR, ARTIST_NO_ID);
        }    
        artist.setConcerts(artistServices.getArtistConcertList(artist.getArtistID()));
        artist.setAlbums(artistServices.getArtistAlbumList(artist.getArtistID()));
        artist.setPopularSong(songServices.getSongByArtistId(artist.getArtistID()));
        for(Song s :artist.getPopularSong()){
        	s.setArtists(artistServices.getArtistBySong(s.getId()));
        }
        return artist;
    }
    
    @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
    public Object changePassword(@RequestBody String data, HttpSession httpSession,HttpServletRequest req, HttpServletResponse res)
    {
    	User tempUser = null;
    	boolean result = false;
    	tempUser = (User) httpSession.getAttribute("user");

        JSONObject obj;
        int sid = 0;
        int pid = 0;
        String userNewPass ="";
        String userOldPass = "";
        String vCode = "";
        try
        {
            obj = new JSONObject(data);
            userNewPass = obj.getString("newPassword");
            userOldPass = obj.getString("oldPassword");
            vCode = obj.getString("vCode");

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return new ApiResponse(STATUS_ERROR, NEW_PASSWORD_ERROR);
        }

    	System.out.println(tempUser.getPassCode());

    	if(!(vCode.equals(tempUser.getPassCode()))){
            return new ApiResponse(STATUS_ERROR, NEW_PASSWORD_ERROR);

        }

        //first compares old password and makes sure that it is valid
        System.out.println(tempUser.getPassword());
    	if(userServices.verifyUser(tempUser.getEmail(), userOldPass) == null){
    		return new ApiResponse(STATUS_ERROR, OLD_PASSWORD_ERROR);
    	}
    	
    	//change the old password to the new password
    	result = userServices.changePassword(tempUser, userNewPass);
	    if(result == true){

            userServices.updateUser(tempUser);
            httpSession.setAttribute("user", tempUser);

            return result;
	    }
	    else{
	    	return new ApiResponse(STATUS_ERROR, NEW_PASSWORD_ERROR);
	    }
    }

    @RequestMapping(value = "/changePasswordRequest", method = RequestMethod.POST)
    public String changePasswordRequest(HttpSession httpSession,HttpServletRequest req, HttpServletResponse res) throws MessagingException {
        User u = null;
        u = (User) httpSession.getAttribute("user");
        JavaMailSenderImpl sender = new JavaMailSenderImpl();
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        sender.setJavaMailProperties(props);
        sender.setHost("smtp.gmail.com");
        sender.setPort(587);

        sender.setUsername("teameagles209@gmail.com");
        sender.setPassword("t3AmEagl3s");
        SecureRandom random = new SecureRandom();
        byte[] newSalt = new byte[16];
        random.nextBytes(newSalt);
        u.setPassCode(newSalt.toString());
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        helper.setTo(u.getEmail());
        helper.setText(newSalt.toString());
        helper.setSubject("Spotify Password Code");

        sender.send(message);
        userServices.updateUser(u);
        httpSession.setAttribute("user", u);
        System.out.println(u.getPassCode());

        return "";
    }
    
    @RequestMapping(value = "/userPage/{id}", method = RequestMethod.GET)
    public Object getUserPage(@PathVariable int id, HttpSession httpSession){
    	User tempUser = userServices.getUserById(id);
    	GeneralUser gu = new GeneralUser(tempUser,userServices.getFollowingArtist(id),
    			userServices.getFollowing(id),generalServices.getFollowers(id, 'u'),
    			userServices.getFollowingPlaylist(id), userServices.getPublicPlaylist(id));
    	return gu;	
    }
    
    @RequestMapping(value="/updateUser", method = RequestMethod.POST)
    public Object updateUser(@RequestBody String data, HttpSession httpSession, HttpServletRequest req, HttpServletResponse res)
    {
    	System.out.println("entering update method");
    	System.out.println(data);
    	User u = null;
    	Date dob = null;
    	JSONObject obj;
    	//Convert the string provided into date
	   	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    	
    	String newFName ="";
    	String newLName = "";
    	String newDOB = "";
		try 
		{
			obj = new JSONObject(data);
			newFName = obj.getString("firstName");
			newLName = obj.getString("lastName");
			newDOB = obj.getString("dob");
			dob = new java.sql.Date(dateFormat.parse(newDOB).getTime());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (ParseException e1){
			System.out.println("error Occured during parsing of date");
		} 		
    	u = (User) httpSession.getAttribute("user");
    	u.setFirstName(newFName);
    	u.setLastName(newLName);
    	u.setDob(dob);
    	userServices.updateUser(u);
    	httpSession.setAttribute("user", u);
    	return u;
    }
    
    @RequestMapping(value="/updateArtist", method = RequestMethod.POST)
    public Object updateArtist(@RequestBody String data, HttpSession httpSession, HttpServletRequest req, HttpServletResponse res)
    {
    	System.out.println("entering update method");
    	System.out.println(data);
    	User u = null;
    	Artist a = null;
    	Date dob = null;
    	JSONObject obj;
    	//Convert the string provided into date
	   	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    	
    	String newFName ="";
    	String newLName = "";
    	String newDOB = "";
    	String newBio = "";
    	String newArtistName = "";
		try 
		{
			obj = new JSONObject(data);
			newFName = obj.getString("firstName");
			newLName = obj.getString("lastName");
			newDOB = obj.getString("dob");
			newBio = obj.getString("bio");
			newArtistName = obj.getString("artistName");
			
			dob = new java.sql.Date(dateFormat.parse(newDOB).getTime());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (ParseException e1){
			System.out.println("error Occured during parsing of date");
		} 		
    	u = (User) httpSession.getAttribute("user");
    	a = userServices.getArtistByUserId(u.getId());
    	u.setFirstName(newFName);
    	u.setLastName(newLName);
    	u.setDob(dob);
    	a.setName(newArtistName);
    	a.setBio(newBio);
    	a.setUser(u);
    	userServices.updateUser(u);
    	artistServices.updateArtist(a);
    	httpSession.setAttribute("user", u);
    	return a;
    }

    @RequestMapping(value = "/search/{searchTerm}", method = RequestMethod.GET)
    public Object search(@PathVariable String searchTerm, HttpSession httpSession, SearchForm searchForm, HttpServletRequest req) {
        if (httpSession.getAttribute("user") == null){
            return new ApiResponse(STATUS_ERROR, NOT_LOGGED_IN);
        }

//        String searchTerm = searchForm.getSearchTerm();
        HashMap<String, Object> results = generalServices.getSearchResults(searchTerm);
        return results;

    }

    @RequestMapping(value = "/changeLanguage", method = RequestMethod.POST)
    public Object setLangauge(HttpSession httpSession, @RequestBody LangForm langForm, HttpServletRequest req) {
        User user = (User) httpSession.getAttribute("user");
        if (user == null){
            return new ApiResponse(STATUS_ERROR, NOT_LOGGED_IN);
        }

        Lang newLang = Lang.values()[langForm.getLangNum()];
        if (newLang == user.getLang()) {
            return new ApiResponse(STATUS_ERROR, NO_LANG_CHANGE);
        }
        boolean val = generalServices.setLang(user, newLang);
        return val;
    }
    
    @RequestMapping(value = "/removeSongFromLibrary", method = RequestMethod.POST)
    public Object removeSongFromLibrary(@RequestBody String id,HttpSession httpSession, HttpServletRequest req, HttpServletResponse res) throws IOException {
    	Playlist p = null;
        boolean result = false;
        JSONObject obj;
        int sid = 0;
        int pid = 0;

        try
        {
            obj = new JSONObject(id);
            sid = obj.getInt("songId");
            pid = obj.getInt("playlistId");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        p = playlistServices.getPlaylist(pid);
        result = generalServices.removeSongFromCollection(sid, p.getPlaylistId(), 'p');
        return result;
    }
    
    @RequestMapping(value = "/album/{id}", method= RequestMethod.GET)
    public Object getAlbum(@PathVariable int id, HttpSession httpSession) {
        Album a = albumServices.getAlbumById(id);
        System.out.println(a.getArtist().getName());
        a.setSongs(songServices.getSongByAlbumId(a.getAlbumId()));
        /* 
         * gets a list of artist that belong to this song
         * also gets the album that this song belongs to
         * */
        for(Song s : a.getSongs()) {
        	s.setArtists(artistServices.getArtistBySong(s.getId()));
        }
        httpSession.setAttribute("album", a);
        return a;
    }


    @RequestMapping(value = "/createUser", method = RequestMethod.POST)
    public Object createUser(HttpSession httpSession, HttpServletRequest req, @RequestBody CreateUserForm createUserForm) {
        User tempUser = null;
        Password tempPassword = null;
        Playlist newLibrary = null;
        Playlist newQueue = null;
        Date dob = null;

        //Convert the string provided into date
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date = new java.util.Date();
        String userEmail = createUserForm.getEmail();
        String dobString = createUserForm.getDate();
        String userPass = createUserForm.getPassword1();
        String firstName = createUserForm.getFirstName();
        String lastName = createUserForm.getLastName();
        String groupName = createUserForm.getGroupName();
        String bio = createUserForm.getBio();
        System.out.println(dobString);
        boolean isArtist = createUserForm.isArtist();

        try {
            dob = new java.sql.Date(dateFormat.parse(dobString).getTime());
        } catch (ParseException e1) {
            return "signupPage.html";
        }
        newLibrary = new Playlist(new java.sql.Date(date.getTime()), true, firstName + " Library", " ");
        newQueue = new Playlist(new java.sql.Date(date.getTime()), true, firstName + " Queue", " ");
        tempPassword = new Password(userPass);
        tempUser = new User(userEmail, tempPassword, firstName, lastName, dob, newLibrary, newQueue);
        if (userServices.createUser(tempUser)) {
            tempUser.getId();
            httpSession.setAttribute("user", tempUser);
            um.addUser(tempUser);
            scm.addCollection(tempUser.getLibrary());
            if (isArtist) {
                Artist a = new Artist();
                a.setName(groupName);
                a.setUser(tempUser);
                a.setListenCount(0);
                a.setRank(1);
                a.setFollowCount(0);
                a.setRoyalties(0.0);
                a.setBio(bio);
                boolean val = artistServices.createArtist(a);
                return val;
            }
            return true;

        } else {
            return new ApiResponse(STATUS_ERROR, ERROR_CREATING_USER);
        }
    }

    @RequestMapping(value = "/unfollowArtist", method = RequestMethod.POST)
    public Object unfollowArtist(@RequestBody String id,HttpSession httpSession, HttpServletRequest req, HttpServletResponse res) throws IOException {
        User u = null;
    	boolean result = false;
        JSONObject obj;
        int uid = 0;
        
        if (httpSession.getAttribute("user") == null){
            return new ApiResponse(STATUS_ERROR, NO_PLAYLIST_SESSION);
        }
        u= (User) httpSession.getAttribute("user");
		try 
		{
			obj = new JSONObject(id);
			uid = obj.getInt("unfollowId");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        System.out.println(uid);
        result = generalServices.unfollow(uid, u.getId(), 'a');
        return result;
    }
    
    @RequestMapping(value = "/followArtist", method = RequestMethod.POST)
    public Object followArtist(@RequestBody String id,HttpSession httpSession, HttpServletRequest req, HttpServletResponse res) throws IOException {
        User u = null;
        Artist tempArtist;
    	boolean result = false;
        JSONObject obj;
        int aid = 0;
        
        if (httpSession.getAttribute("user") == null){
            return new ApiResponse(STATUS_ERROR, NO_PLAYLIST_SESSION);
        }
        u= (User) httpSession.getAttribute("user");
		try 
		{
			obj = new JSONObject(id);
			aid = obj.getInt("followId");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		tempArtist = artistServices.getArtistById(aid);	
        result = generalServices.follow(u, null, null, tempArtist);
        return result;
    }

    @RequestMapping(value="/banAccount", method = RequestMethod.POST)
    public Object banAccount(@RequestBody String id, HttpSession httpSession, HttpServletResponse res)
    {
    	User u = null;
    	JSONObject obj;
        int uid = 0;
     
		try 
		{
			obj = new JSONObject(id);
			uid = obj.getInt("banId");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	u = userServices.getUserById(uid);
    	u.setBan(true);
    	return userServices.updateUser(u);	
    }
    
    @RequestMapping(value="/adminDeleteAccount", method = RequestMethod.POST)
    public Object adminDeleteAccount(@RequestBody String id, HttpSession httpSession, HttpServletResponse res)
    {
    	User u = null;
    	JSONObject obj;
        int uid = 0;
     
		try 
		{
			obj = new JSONObject(id);
			uid = obj.getInt("id");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	u = userServices.getUserById(uid);
    	u.setStatus(true);
    	return userServices.deleteAccount(u);	
    }
    
    @RequestMapping(value="/deleteSong", method = RequestMethod.POST)
    public Object deleteSong(@RequestBody String id, HttpSession httpSession, HttpServletResponse res)
    {
    	Song s = null;
    	JSONObject obj;
        int sid = 0;
     
		try 
		{
			obj = new JSONObject(id);
			sid = obj.getInt("songId");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	return songServices.deleteSong(sid);	
    }
    
    @RequestMapping(value="/unbanAccount", method = RequestMethod.POST)
    public Object unbanAccount(@RequestBody String id, HttpSession httpSession, HttpServletResponse res)
    {
    	User u = null;
    	JSONObject obj;
        int uid = 0;
     
		try 
		{
			obj = new JSONObject(id);
			uid = obj.getInt("unbanId");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	u = userServices.getUserById(uid);
    	u.setBan(false);
    	return userServices.updateUser(u);	
    }
    
    @RequestMapping(value = "/lastPlayedSong", method = RequestMethod.POST)
    public Object lastPlayedSong(@RequestBody String id,HttpSession httpSession, HttpServletRequest req, HttpServletResponse res) throws IOException {
        Song s = null;
        User u = null;
        boolean result = false;
        JSONObject obj;
        int sid = 0;
        
        u = (User) httpSession.getAttribute("user");
		try 
		{
			obj = new JSONObject(id);
			sid = obj.getInt("song");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		s = songServices.getSongById(sid);
		s.addPlay();
		songServices.updateSong(s);
		u.setLastSong(s);
        System.out.println(sid);
        result = userServices.updateUser(u);
        httpSession.setAttribute("user", u);
        return result;
    }
    
    @RequestMapping(value = "/getAlbums", method = RequestMethod.GET)
    public Object getAlbums(HttpSession httpSession, HttpServletRequest req, HttpServletResponse res) throws IOException {
        User u = null;

        u = (User) httpSession.getAttribute("user");
        return userServices.getLibraryAlbums(u.getLibrary().getPlaylistId());
    }
    
    @RequestMapping(value = "/getArtists", method = RequestMethod.GET)
    public Object getArtists(HttpSession httpSession, HttpServletRequest req, HttpServletResponse res) throws IOException {
        User u = null;

        u = (User) httpSession.getAttribute("user");
        List<Artist> artists = userServices.getLibraryArtists(u.getLibrary().getPlaylistId());
        for (Artist a : artists) {
            a.setAlbums(artistServices.getArtistAlbumList(a.getArtistID()));
        }
        return artists;
    }
    
    @RequestMapping(value = "/getHomepage", method = RequestMethod.GET)
    public Object getHomepage(HttpSession httpSession, HttpServletRequest req, HttpServletResponse res) throws IOException {
    	Map<String,Object> homePageInfo=new HashMap<String, Object>();
       
    	homePageInfo.put("popularAlbumList",  albumServices.getPopularAlbums());
    	List<Song> songs = songServices.getPopSongs();
    	for(Song s : songs) {
    	    s.setArtists(artistServices.getArtistBySong(s.getId()));
        }
    	homePageInfo.put("popularSongList", songs);
        return homePageInfo;
    }

    @RequestMapping(value = "/getStats", method = RequestMethod.GET)
    public Object getTotalViews(HttpSession httpSession, HttpServletRequest req) throws IOException {
        return generalServices.getAllStats();
    }
    
    @RequestMapping(value = "/getArtistStats", method = RequestMethod.GET)
    public Object getArtistStats(HttpSession httpSession, HttpServletRequest req) throws IOException {
    	User u = null;
    	Artist a = null;
    	Map<String,Object> artistInfo=new HashMap<String, Object>();
    	
    	u = (User) httpSession.getAttribute("user");
    	a = userServices.getArtistByUserId(u.getId());
    	
    	artistInfo.put("songList", songServices.getSongByArtistIdNoLimit(a.getArtistID()));
    	artistInfo.put("artist", a);
    
    	return artistInfo;
    }
}

