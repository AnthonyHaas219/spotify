#!/usr/bin/env python
# -*- coding: utf-8 -*-
import unicodecsv, MySQLdb, traceback, sys
import sys
# reload(sys)
# sys.setdefaultencoding('UTF8')

dob = "2017-11-27"
dateAdded = "2017-11-27"

conn = MySQLdb.connect(host="mysql3.cs.stonybrook.edu",
                       user="eagles",
                       passwd="changeit",
                       db="eagles",)

conn.set_character_set('utf8')

def insertArtist(artistName):

    global cursor, conn, dob, artistIdCombos
    email = artistName + "@gmail.com"
    ids = []
    if artistName in artistIdCombos:
        return artistIdCombos[artistName]
    try:
        cursor.execute(
            "INSERT INTO users (USER_ID, email, firstName, lastName, dob, loggedIn, admin, userImageLoc, LIBRARY_ID, "
            "QUEUE_ID, premium, status, lang, passCode) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            (None, email, artistName, artistName, dob, 0, 0, None, 1, 1, 0, 0, 0, None))
        conn.commit()
    except:
        conn.rollback()
        traceback.print_exc()
        print("fail")
        sys.exit()
    try:
        userId = cursor.lastrowid
        cursor.execute(
            "INSERT INTO artists (ARTIST_ID, groupName, USER_ID, listenCount, artistRank, followCount, royalty, bio)"
            "VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
            (None, artistName, userId, 0, 1, 0, 0, ""))
        conn.commit()
        ids.append(userId)
        ids.append(cursor.lastrowid)
    except:
        conn.rollback()
        traceback.print_exc()
        print("fail on inserting artist")
        sys.exit()
    if artistName not in artistIdCombos:
        artistIdCombos[artistName] = ids
    print("success importing artists")
    return ids


def insertArtists(row):
    global artistNames
    artists = []
    # artist = row["Artist"]
    artist = row[0]
    artistsIds = insertArtist(artist)
    artists.append(artistsIds)
    # featArtist = row["FeaturingArtist"]
    featArtist = row[5]
    if len(featArtist) != 0:
        featArtistIds = insertArtist(featArtist)
        artists.append(featArtistIds)
    return artists

def insertAlbum(row, artistId):
    # album = row["Album"]
    album = row[2]
    if album not in albumNames:
        albumNames.append(album)
    albumId = insertAlbumDB(album, artistId)
    return albumId

def insertAlbumDB(albumName, artistId):
    global cursor, conn, dateAdded, albumIdCombos
    if albumName in albumIdCombos:
        return albumIdCombos[albumName]
    try:
        cursor.execute(
            "INSERT INTO albums (ALBUM_ID, title, imageLoc, dateAdded, ARTIST_ID) VALUES (%s, %s, %s, %s, %s)",
            (None, albumName, albumName + ".jpg", dateAdded, artistId))
        conn.commit()
    except:
        conn.rollback()
        traceback.print_exc()
        print("fail")
        sys.exit()
    insertedAlbumId = cursor.lastrowid
    if albumName not in albumIdCombos:
        albumIdCombos[albumName] = insertedAlbumId
    return insertedAlbumId


def insertOwner(artistUserId, albumId):
    global cursor, conn
    try:
        cursor.execute(
            "INSERT INTO owner (OWNER_ID, USER_ID, PLAYLIST_ID, ALBUM_ID) VALUES (%s, %s, %s, %s)",
            (None, artistUserId, None, albumId))
        conn.commit()
    except:
        conn.rollback()
        traceback.print_exc()
        print("Failure")
        sys.exit()

def get_sec(time_str):
    if time_str.count(":") == 2:
        h, m, s = time_str.split(':')
        return int(h) * 3600 + int(m) * 60 + int(s)
    else:
        m, s = time_str.split(":")
        return int(m) * 60 + int(s)


def insertSong(row, albumId):
    global cursor, conn, songAlbumCombos, dateAdded
    # album = row["Album"]
    album = row[2]
    # song = row["songName"]
    song = row[4]
    # songLength = get_sec(row["songLength"])
    songLength = get_sec(row[6])
    lyrics = ""
    try:
        with open("lyrics/" + song + ".txt") as lyricsFile:
            lyrics = lyricsFile.read()
    except IOError:
        lyrics = ""
    try:
        cursor.execute(
            "INSERT INTO songs (SONG_ID, songTitle, songLength, dateAdded, ALBUM_ID, numberOfPlays, songLocation, lyric) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
            (None, song, songLength, dateAdded, albumId, 0, "", lyrics))
        conn.commit()
    except:
        conn.rollback()
        traceback.print_exc()
        print("fail")
        sys.exit()
    return cursor.lastrowid


def insertArtistsOfSong(artistsIds, songId):
    global cursor, conn
    mainArtistId = artistIds[0][1]
    featArtistId = None
    if len(artistIds) == 2:
        featArtistId = artistIds[1][1]
    try:
        cursor.execute(
            "INSERT INTO artistofsongs (SONG_ID, ARTIST_ID) VALUES (%s, %s)",
            (songId, mainArtistId))
        conn.commit()
    except:
        conn.rollback()
        traceback.print_exc()
        print("Failure")
        sys.exit()
    if featArtistId is not None:
        try:
            cursor.execute(
                "INSERT INTO artistofsongs (SONG_ID, ARTIST_ID) VALUES (%s, %s)",
                (songId, featArtistId))
            conn.commit()
        except:
            conn.rollback()
            traceback.print_exc()
            print("Failure")
            sys.exit()

def insertSongInCollection(songId, albumId):
    global cursor, conn
    try:
        cursor.execute(
            "INSERT INTO songincollection (SONG_COLLECTION_ID, SONG_ID, ALBUM_ID, PLAYLIST_ID) VALUES (%s, %s, %s, %s)",
            (None, songId, albumId, None))
        conn.commit()
    except:
        conn.rollback()
        traceback.print_exc()
        print("Failure")
        sys.exit()


cursor = conn.cursor()
cursor.execute('SET NAMES utf8;')
cursor.execute('SET CHARACTER SET utf8;')
cursor.execute('SET character_set_connection=utf8;')

artistNames = []
albumNames = []
songAlbumCombos = []
artistIdCombos = {}
albumIdCombos = {}
with open("spotify.csv") as csvfile:
    # spotifyData = csv.DictReader(csvfile)
    reader = unicodecsv.reader(csvfile, encoding='utf-8')
    row = reader.next()
    row = reader.next()
    # for row in spotifyData:
    while row is not None:
        artistIds = insertArtists(row)
        albumId = insertAlbum(row, artistIds[0][1])
        insertOwner(artistIds[0][0], albumId)
        songId = insertSong(row, albumId)
        insertArtistsOfSong(artistIds, songId)
        insertSongInCollection(songId, albumId)
        row = reader.next()







