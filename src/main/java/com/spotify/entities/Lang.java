package com.spotify.entities;

public enum Lang {
        ENGLISH,
        SPANISH,
        ITALIAN,
        FRENCH,
        CHINESE,
        JAPANESE
}
