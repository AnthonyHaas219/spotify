package com.spotify.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.sql.Date;

import javax.persistence.*;

@Entity
@Table(name = "users")
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public class User{
	
	@Id
	@Column(name = "USER_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@Transient
	private Password password;
	@Column(name = "firstName")
	private String firstName;
	@Column(name = "lastName")
	private String lastName;
	@Column(name = "passCode")
	private String passCode;
	private Lang lang;
	@Column(name = "dob")
	private Date dob;
	@Column(name = "loggedIn")
	private boolean loggedIn;
	@Column(name = "admin")
	private boolean admin;
	@Column(name = "email")
	private String email;
	@Column(name = "userImageLoc")
	private String userImageLoc;
	@Column(name = "premium")
	private boolean premium;
	@Column(name = "ban")
	private boolean ban;
	@OneToOne
	@JoinColumn(name = "LIBRARY_ID", nullable = false, referencedColumnName = "PLAYLIST_ID")
	private Playlist library;
	@OneToOne
	@JoinColumn(name = "QUEUE_ID", nullable = false, referencedColumnName = "PLAYLIST_ID")
	private Playlist queue;
	@Column(name="status")
	private boolean status;
	@OneToOne
	@JoinColumn(name = "lastSong", referencedColumnName = "SONG_ID")
	private Song lastSong;
	
	public User ()
	{
		this.id = 0;
		this.admin = false;
		this.password = null;
		this.firstName = "";
		this.lastName = "";
		this.dob = null;
		this.loggedIn = false;
		this.userImageLoc = "";
		this.email = "";
		this.library = null;
		this.premium = false;
		this.status = false;
		this.queue = null;
		this.lang = Lang.ENGLISH;
		this.lastSong = null;
		this.ban = false;
	}
	
	public User(String username, Password pass, String fName, String lName, Date newDob, Playlist library, Playlist queue)
	{
		this.email = username;
		this.password = pass;
		this.firstName = fName;
		this.lastName = lName;
		this.dob = newDob;
		this.admin = false;
		this.loggedIn = true;
		this.userImageLoc = "";
		this.library = library;
		this.premium = false;
		this.status = false;
		this.queue = queue;
		this.lang = Lang.ENGLISH;
		this.lastSong = null;
		this.ban = false;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public Password getPassword() {
		return password;
	}
	
	public void setPassword(Password password) {
		this.password = password;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public Date getDob() {
		return dob;
	}
	
	public void setDob(Date dob) {
		this.dob = dob;
	}
	
	public boolean isLoggedIn() {
		return loggedIn;
	}
	
	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	
	public boolean isAdmin() {
		return admin;
	}
	
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getUserImageLoc() {
		return userImageLoc;
	}
	
	public void setUserImageLoc(String userImageLoc) {
		this.userImageLoc = userImageLoc;
	}

	public boolean isPremium() {
		return premium;
	}

	public Playlist getLibrary() {
		return library;
	}

	public void setPremium(boolean premium) {
		this.premium = premium;
	}

	public void setLibrary(Playlist library) {
		this.library = library;
	}
	
	public void setStatus(boolean status){
		this.status = status;
	}
	
	public boolean getStatus(){
		return this.status;
	}

	public Playlist getQueue() {
		return queue;
	}

	public void setQueue(Playlist queue) {
		this.queue = queue;
	}

	@Enumerated(EnumType.ORDINAL)
	public Lang getLang(){ return lang; }

	public void setLang(Lang lang){ this.lang = lang; }

	public Song getLastSong() {
		return lastSong;
	}

	public void setLastSong(Song lastSong) {
		this.lastSong = lastSong;
	}

	public String getPassCode() {
		return passCode;
	}

	public void setPassCode(String passCode) {
		this.passCode = passCode;
	}

	public boolean isBan() {
		return ban;
	}

	public void setBan(boolean ban) {
		this.ban = ban;
	}
}
