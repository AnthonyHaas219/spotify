package com.spotify.entities;

import javax.persistence.*;

@Entity
@Table(name = "advertisements")
public class Advertisement {

    @Id
    @Column(name="idadvertisements")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int adId;

    @Column(name = "imageLoc")
    private String imageLoc;

    @Column(name = "status")
    private int status;

    public int getAdId() {
        return adId;
    }

    public void setAdId(int albumId) {
        this.adId = adId;
    }


    public String getImageLoc() {
        return imageLoc;
    }

    public void setImageLoc(String imageLoc) {
        this.imageLoc = imageLoc;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
