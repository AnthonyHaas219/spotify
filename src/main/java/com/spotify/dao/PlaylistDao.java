package com.spotify.dao;

import java.util.List;

import com.spotify.entities.Playlist;
import com.spotify.entities.User;
import com.spotify.forms.CreatePlaylistForm;

public interface PlaylistDao {
    public Playlist getPlaylist(int id);
    public List<Playlist> getPlaylists(int userId);
    public int addPlaylist(CreatePlaylistForm playlistForm, User user);
    public boolean renamePlaylist(User u, Playlist p, String newName);
    public boolean deletePlaylist(int id);

   public boolean updatePlaylist(Playlist p);
}
