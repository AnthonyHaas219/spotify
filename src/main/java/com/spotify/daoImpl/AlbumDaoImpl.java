package com.spotify.daoImpl;

import java.util.List;

import com.spotify.entityManagers.SongCollectionManager;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spotify.dao.AbstractDao;
import com.spotify.dao.AlbumDao;
import com.spotify.entities.Album;

@Repository("albumDao")
@Transactional
public class AlbumDaoImpl extends AbstractDao implements AlbumDao{
	
	public Album getAlbumById(int id) {
		Album a = (Album) getSession().get(Album.class, id);
		return a;
	}

	@Override
	public boolean createAlbum(Album newAlbum) {
		try {
			getSession().saveOrUpdate(newAlbum);
			return true;
		}
		catch(HibernateException e) {
			return false;
		}
	}

	@Override
	public Album getAlbum(int id) {
		SongCollectionManager songCollectionManager = SongCollectionManager.getSongCollectionManager();
		Album album = songCollectionManager.getAlbum(id);
		if (album != null) {
			return album;
		}
		album = (Album) getSession().get(Album.class, id);
		songCollectionManager.addCollection(album);
		return album;
	}

	@Override
	public List<Album> getFullAlbumList() {
		try{
			String sql = "from Album";
			Query q = getSession().createQuery(sql);
			q.executeUpdate();
			List<Album> result = q.list();
			return result.isEmpty() ? null : result;
			
		}
		catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Album> getPopularAlbums() {
		String sql = "from Album a where a.albumId in "
				+ "(select s.album.albumId from Song s order by s.numPlays)";
		Query q = getSession().createQuery(sql);
		q.setMaxResults(10);
		List<Album> result = q.list();
		return result;
	}
}
