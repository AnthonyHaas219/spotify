package com.spotify.forms;

public class RenamePlaylistForm {

	private String newPlaylistName;

    public String getNewPlaylistName() {
        return newPlaylistName;
    }

    public void setNewPlaylistName(String newPlaylistName) {
        this.newPlaylistName = newPlaylistName;
    }
}
