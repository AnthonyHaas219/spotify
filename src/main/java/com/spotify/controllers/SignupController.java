package com.spotify.controllers;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.spotify.entities.Password;
import com.spotify.entities.Playlist;
import com.spotify.entities.User;
import com.spotify.entityManagers.SongCollectionManager;
import com.spotify.entityManagers.UserManager;
import com.spotify.services.UserServices;

@RestController
public class SignupController {
	
	 @Autowired
	 @Qualifier("userServices")
	 private UserServices us; 
	 private UserManager um = UserManager.getUserManager();
	 private SongCollectionManager scm = SongCollectionManager.getSongCollectionManager();
 
	 @RequestMapping(value = "/signup", method = RequestMethod.POST)
	 public String signUp(HttpSession httpSession,HttpServletRequest req, HttpServletResponse res) 
	 {
	 	User tempUser = null;
	   	Password tempPassword = null;
	   	Playlist newLibrary = null;
	   	Playlist newQueue = null;
	   	Date dob = null;
	   	
	   	//Convert the string provided into date
	   	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	   	java.util.Date date = new java.util.Date();;
	   	String userEmail = req.getParameter("email");
	   	String dobString = req.getParameter("date");
	   	String userPass = req.getParameter("Password1");
	   	String firstName = req.getParameter("FirstName");
	   	String lastName = req.getParameter("LastName");	 	   	
		try{
			dob = new java.sql.Date(dateFormat.parse(dobString).getTime());
		} 
		catch (ParseException e1){
			return "signupPage.html";
		} 	
		newLibrary = new Playlist(new java.sql.Date(date.getTime()), true, firstName+" Library", " ");
		newQueue = new Playlist(new java.sql.Date(date.getTime()), true, firstName+" Queue", " ");
	   	tempPassword = new Password(userPass);
	   	tempUser = new User(userEmail, tempPassword, firstName, lastName, dob, newLibrary, newQueue);
	   	try{
	    	if(us.createUser(tempUser)){
	    		tempUser.getId();
	    		httpSession.setAttribute("user", tempUser);
	    		um.addUser(tempUser);
	    		scm.addCollection(tempUser.getLibrary());
	    		res.sendRedirect("/browse");
	    	}
	    	else{
	    		res.sendRedirect("signupPage.html");
	    	}
		} 
    	catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
    }
}