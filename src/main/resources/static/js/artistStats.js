angular.module("mySpotify").controller("artistStatsCtrl", function ($scope, $rootScope, $http, $routeParams, $dialogs) {

    $scope.loadInfo = function() {
        $http.get("/api/getArtistStats").success(function (response) {
            $scope.songList = response.songList;
            $scope.artist = response.artist;
            $scope.totalRevenue = 0.0;
            console.log($scope.songList);
            console.log($scope.artist);
            angular.element(".rightBar").css('margin-top', '-20px');
            for(var i=0;i<$scope.songList.length;i++){
                $scope.songList[i].songRev = $scope.songList[i].numPlays * .05;
                $scope.songList[i].songRev = Number($scope.songList[i].songRev).toFixed(2);
                $scope.totalRevenue += ($scope.songList[i].numPlays * .05)
            }
            $scope.totalRevenue = Number($scope.totalRevenue).toFixed(2);
            console.log($scope.totalRevenue)
        });
    }

        $scope.deleteSong = function(id){
            $http({
                  url: '/api/deleteSong',
                  method: "POST",
                  data: { "songId": id}
            })
            .then(function(response) {
                for(i= 0; i<$scope.songList.length; i++){
                    if (id === $scope.songList[i].id){
                        console.log($scope.songList[i])
                        $scope.songList.splice(i, 1);
                        break;
                    }
                }
                $scope.totalRevenue = 0.0;
                for(var i=0;i<$scope.songList.length;i++){
                    $scope.totalRevenue+=($scope.songList[i].numPlays*.05)
                }
                $scope.totalRevenue =+$scope.totalRevenue.toFixed(2);
                console.log($scope.totalRevenue)
            });
        }
});