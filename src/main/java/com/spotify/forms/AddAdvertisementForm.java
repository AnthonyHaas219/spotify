package com.spotify.forms;

import org.springframework.web.multipart.MultipartFile;


public class AddAdvertisementForm {

    private MultipartFile advertisementImage;
    public MultipartFile getAdvertisementImage() {
        return advertisementImage;
    }

    public void setAdvertisementImage(MultipartFile advertisementImage) {
        this.advertisementImage = advertisementImage;
    }

}