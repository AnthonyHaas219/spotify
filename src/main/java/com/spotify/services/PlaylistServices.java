package com.spotify.services;

import com.spotify.entities.Playlist;
import com.spotify.entities.User;
import com.spotify.forms.CreatePlaylistForm;

public interface PlaylistServices {
	public int createPlaylistService(CreatePlaylistForm playlistForm, User user);
	public boolean removePlaylist(int id, int uid, char type);
	public Playlist getPlaylist(int id);
	public Object getPlaylists(int userId);
	public boolean renamePlaylist(User u, Playlist p, String newName);

    public boolean updatePlaylist(Playlist p);
}
