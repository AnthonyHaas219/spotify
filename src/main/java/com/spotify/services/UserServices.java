package com.spotify.services;

import java.util.List;

import com.spotify.entities.Album;
import com.spotify.entities.Artist;
import com.spotify.entities.Playlist;
import com.spotify.entities.User;
import com.spotify.forms.PremiumUserForm;

public interface UserServices {
	public User verifyUser(String email, String pass);
	public boolean switchLogin(User u);
	public boolean createUser(User newUser);
	public List<User> getOwner(int id);
	public boolean upgradePremium(User u, PremiumUserForm puf);
	public boolean downgradePremium(User u);
	public boolean deleteAccount(User u);
	public boolean changePassword(User u, String newPass);
	public User getUserById(int id);
	public List<Artist> getFollowingArtist(int uid);
	public List<Playlist> getFollowingPlaylist(int uid);
	public List<User> getFollowing(int id);
	public List<Playlist> getPublicPlaylist(int id);
	public boolean updateUser(User u);
	public List<Album> getLibraryAlbums(int id);
	public List<Artist> getLibraryArtists(int id);
	public boolean isArtist(int id);
	public Artist getArtistByUserId(int id);
}
