package com.spotify.services;

import java.util.List;

import com.spotify.entities.Song;

public interface SongServices {
	public List<Song> getSongsByPlaylistId(int id);
	public Song getSongById(int songId);
	public int createSong(Song s);
	public List<Song> getSongByArtistId(int aid);
	public List<Song> getSongByAlbumId(int aid);
	public List<Song> getPopSongs();
	public boolean deleteSong(int id);
	public List<Song> getSongByArtistIdNoLimit(int aid);
	public boolean updateSong(Song s);
}
