angular.module("mySpotify").controller("albumCtrl", function($scope, $rootScope, $http, $routeParams) {

    var id = parseInt($routeParams.id);

    $scope.loadInfo = function() {
        $http.get("api/album/" + id).success(function (response) {
            $scope.title = response.title;
            $scope.imageFile = response.imageLoc;
            $scope.albumSongs = response.songs;
            $scope.artist = response.artist;
            $scope.dataAdded = response.dateAdded;
            $scope.savedAlbum = false;
            $scope.albumSongs.inLibrary = false;
             $scope.songs = response.songs;
             $scope.songOrder = JSON.parse(JSON.stringify($scope.songs));
             $scope.$parent.songs = response.popularSong;
             $scope.$parent.songOrder = JSON.parse(JSON.stringify($scope.songs));
             var x = 0;
            for(x=0; x<$scope.albumSongs.length; x++){
                console.log(response.songs[x].length)
                $scope.albumSongs[x].length = $scope.$parent.convertTime(response.songs[x].length);
            }

            console.log("Artist object" + $scope.artist);
            console.log("Artist name=" +$scope.artist.name);
            var i=0;
            var j=0;
            var counter = 0;

            for(j=0;j<$scope.albumSongs.length;j++)
            {
                $scope.albumSongs[j].inLibrary = false;

                //If there are more than two artists, this if statement removes the firsst artist bc we don't need to show the first artist
                //bc they own the album. I think this is how we'd do it
//                console.log($scope.albumSongs[j]);
//                console.log($scope.albumSongs[j].artists);
                if ($scope.albumSongs[j].artists.length > 1) {
                    for (var k = 0; k < $scope.albumSongs[j].artists.length; k++) {
                        if ($scope.albumSongs[j].artists[k].name === $scope.artist.name) {
                            $scope.albumSongs[j].artists.splice(k, 1);
                        }
                    }
                }
                else {
                    $scope.albumSongs[j].artists[0].name = "";
                }
            }

            for(i=0;i<$scope.$parent.library.songs.length;i++){
                for(j=0;j<$scope.albumSongs.length;j++)
                {
                    var song = $scope.albumSongs[j];
                    if($scope.$parent.library.songs[i].id == song.id){
                        song.inLibrary = true;
                        console.log($scope.$parent.library.songs[i].id);
                        console.log(song.id);
                        counter++;
                        console.log(counter);
                        break;
                    }
                    console.log(song.id);
                }
                $scope.$parent.library.songs[i].id
            }
            console.log(counter);
            if(counter == $scope.albumSongs.length)
            {
                $scope.savedAlbum = true;
            }
//             i=0;
//                        console.log(response.songs);
//                        for(i=0; i<$scope.songs.length; i++){
//                            $scope.songs[i].length = $scope.$parent.convertTime(response.songs[i].length);
//                        }
//              i=0;
//                         for(i=0; i<$scope.response.songs.length; i++){
//                             $scope.$parent.songs[i].length = $scope.$parent.convertTime(response.songs[i].length);
//                         }
        });
    }

    $scope.addAlbumToLibrary = function(){
        var library = $scope.$parent.library;
        var i = 0;
        var j = 0;
        var counter = 0;
        var added = false;

        for(i = 0; i < $scope.albumSongs.length;i++){
            var song = $scope.albumSongs[i];
            if($scope.$parent.library.songs.length > 0)
            {
                for(j=0; j < $scope.$parent.library.songs.length;j++)
                {
                    if($scope.$parent.library.songs[j].id == song.id)
                    {
                        console.log("already in library");
                        counter++;
                        added = true;
                        song.inLibrary = true;
                        break;
                    }
                }
                if(added == false)
                {
                    $http({
                        url: '/api/addSongToPlaylist',
                        method: "POST",
                        data: { "songId" : song.id,
                            "playlistId" : library.playlistId
                        }
                    })
                    .then(function(response) {
                        console.log("added to library")
                    });
                    $scope.$parent.library.songs.push(song)
                    counter++;
                    song.inLibrary = true;
                }
                added = false;
            }
            else{
                $http({
                        url: '/api/addSongToPlaylist',
                        method: "POST",
                        data: { "songId" : song.id,
                            "playlistId" : library.playlistId
                        }
                })
                .then(function(response) {
                    console.log("added to library")
                });
                $scope.$parent.library.songs.push(song);
                counter++;
                song.inLibrary = true;
            }
        }
        console.log(counter);
        console.log($scope.albumSongs.length)
        if(counter == $scope.albumSongs.length)
        {
            $scope.savedAlbum = true;
        }
    }

    $scope.removeAlbumFromLibrary = function(){
        var library = $scope.library;
        var i = 0;
        var j = 0;
        console.log(library.playlistId)
        for(i = 0; i < $scope.albumSongs.length;i++){
            var song = $scope.albumSongs[i];
            $http({
                  url: '/api/removeSongFromLibrary',
                  method: "POST",
                  data: { "songId" : song.id,
                        "playlistId" : library.playlistId
                }
            })
            .then(function(response) {
                console.log("remove from library")
            });
            // success
            for(j= 0; j<$scope.$parent.library.songs.length; j++){
                if (song.id == $scope.$parent.library.songs[j].id){
                    $scope.library.songs.splice(j, 1);
                }
            }
            song.inLibrary = false;
        }
        $scope.savedAlbum = false;
    }
});