package com.spotify.entities;

import java.sql.Date;
import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "concerts")
public class Concert {
	
	@Id
	@Column(name="CONCERT_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@Column(name="concertName")
	private String name;
	@Column(name="concertDate")
	private Date date;
	@Column(name="concertBegTime")
	private Time begTime;
	@Column(name="concertEndTime")
	private Time endTime;
	@Column(name="location")
	private String location;
	@Column(name="concertImage")
	private String concertImage;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public Time getBegTime() {
		return begTime;
	}
	
	public void setBegTime(Time begTime) {
		this.begTime = begTime;
	}
	
	public Time getEndTime() {
		return endTime;
	}
	
	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}
	
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public String getConcertImage() {
		return concertImage;
	}
	
	public void setConcertImage(String concertImage) {
		this.concertImage = concertImage;
	}
}
