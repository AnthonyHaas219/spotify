package com.spotify.servicesImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spotify.dao.UserDao;
import com.spotify.entities.Album;
import com.spotify.entities.Artist;
import com.spotify.entities.Password;
import com.spotify.entities.Payment;
import com.spotify.entities.Playlist;
import com.spotify.entities.User;
import com.spotify.forms.PremiumUserForm;
import com.spotify.services.UserServices;

@Service("userServices")
@Transactional
public class UserServicesImpl implements UserServices{
	
	@Autowired
	@Qualifier("userDao")
	private UserDao userDao;
	
	public User verifyUser(String email, String pass) {
		User u = userDao.getUser(email);
		Password p = userDao.getPassword(u);
		if(u == null || p == null) {
			return null;
		}
		/*if user is deactivated will not pass back user*/
		if(u.getStatus() || u.isBan()){
			System.out.println("This user is deactivated.");
			return null;
		}
		if(p.comparePassword(pass))  {
			userDao.updateLogin(u);
			return u;
		}
		return null;
	}
	
	public boolean switchLogin(User u)
	{
		return userDao.updateLogin(u);
	}
	
	public boolean createUser(User newUser)
	{
		if(userDao.checkEmailAvailability(newUser.getEmail())) {
			return userDao.createUserDao(newUser);
		}
		return false;
	}

	public List<User> getOwner(int id)
	{
		return userDao.getOwner(id);
	}
	
	@Override
	public boolean upgradePremium(User u, PremiumUserForm puf) {
		Payment tempPayment = new Payment(puf.getCreditCardNumber(), puf.getExpMonth(), puf.getExpYear(), puf.getSec(), puf.getBillAddr1(),
				puf.getBillAddr2(), puf.getBillCity(), puf.getBillState(), puf.getBillZip(), u);
		return userDao.upgradePremium(tempPayment, u);
	}
	
	@Override
	public boolean downgradePremium(User u) {
		return userDao.downgradePremium(u);
	}

	@Override
	public boolean deleteAccount(User u) {
		u.setStatus(true);
		return userDao.deleteAccount(u);
	}
	
	public boolean changePassword(User u, String newPass){
		Password p = userDao.getPassword(u);
		p.changePassword(newPass);
		return true;
	}
	
	public User getUserById(int id){
		return userDao.getUserById(id);
	}

	@Override
	public List<Artist> getFollowingArtist(int uid) {
		// TODO Auto-generated method stub
		return userDao.getFollowingArtist(uid);
	}

	@Override
	public List<Playlist> getFollowingPlaylist(int uid) {
		// TODO Auto-generated method stub
		return userDao.getFollowingPlaylist(uid);
	}

	@Override
	public List<User> getFollowing(int id) {
		// TODO Auto-generated method stub
		return userDao.getFollowing(id);
	}

	@Override
	public List<Playlist> getPublicPlaylist(int id) {
		// TODO Auto-generated method stub
		return userDao.getPublicPlaylist(id);
	}
	
	public boolean updateUser(User u){
		return userDao.updateUser(u);
	}

	@Override
	public List<Album> getLibraryAlbums(int id) {
		// TODO Auto-generated method stub
		return userDao.getLibraryAlbums(id);
	}
	
	public List<Artist> getLibraryArtists(int id){
		return userDao.getLibraryArtists(id);
	}

	@Override
	public boolean isArtist(int id) {
		// TODO Auto-generated method stub
		return userDao.isArtist(id);
	}
	
	public Artist getArtistByUserId(int id){
		return userDao.getArtistByUserId(id);
	}
}
