package com.spotify.services;

import java.util.List;

import com.spotify.entities.Album;

public interface AlbumServices {
	public Album getAlbumById(int id);
	public List<Album> getFullAlbumList();
	public List<Album> getPopularAlbums();
}
