package com.spotify.dao;

import java.util.List;

import com.spotify.entities.Album;
import com.spotify.entities.Artist;
import com.spotify.entities.Concert;
import com.spotify.entities.Song;

public interface ArtistDao {
	public Artist getArtistById(int id);
	public boolean updateArtist();
	public List<Album> getArtistAlbumList(int aid);
	public boolean createArtist(Artist newArtist);
	public boolean removeArtist(int id);
	public List<Artist> getArtistBySong(int sid);
	public Artist getArtistByName(String name);
	public List<Concert> getArtistConcertList(int id);
	public boolean updateArtist(Artist a);
}
