package com.spotify.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public abstract class SongCollection {

	@Transient
	private List<User> owners;
	@Column(name="title")
	private String title;
	@Column(name="imageLoc")
	private String imageLoc;
	@Transient
	private List<Song> songs;
	
	public SongCollection()
	{
		this.owners = new ArrayList<User>();
		this.songs = new ArrayList<Song>();
		this.title = "";
		this.imageLoc = "";
		
	}
	
	public SongCollection(String title, String image)
	{
		this.owners = new ArrayList<User>();
		this.songs = new ArrayList<Song>();
		this.title = title;
		this.imageLoc = image;
		
	}

	public List<User> getOwners() {
		return owners;
	}
	
	public void setOwners(List<User> owners) {
		this.owners = owners;
	}
	
	public List<Song> getSongs() {
		return songs;
	}
	
	public void setSongs(List<Song> songs) {
		this.songs = songs;
	}
	
	/*used to add song to collection*/
	public void addSong(Song s) {
		this.songs.add(s);
	}
	
	/*used to remove song from list*/
	public void removeSong(Song s) {
		this.songs.remove(s);
	}
	
	/*checks to see if list is empty*/
	public boolean isEmpty() {
		return this.songs.isEmpty();
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getImageLoc() {
		return imageLoc;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setImageLoc(String imageLoc) {
		this.imageLoc = imageLoc;
	}	
}
