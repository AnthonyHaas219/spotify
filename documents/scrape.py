import os
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import urllib
import time
from selenium.webdriver.chrome.options import Options
import csv
options = webdriver.ChromeOptions() 
options.add_argument("home/scott/.config/google-chrome/Default")
chromedriver = "/home/scott/Downloads/chromedriver"
os.environ["webdriver.chrome.driver"] = chromedriver
driver = webdriver.Chrome(chromedriver, chrome_options=options)
driver.implicitly_wait(1)
url = 'https://www.google.nl/'
list = []
with open("spotifySongs.csv", "rb") as f:
	reader = csv.reader(f)
	test = True
	for row in reader:
		if test == True:
			test = False
			continue

		if(row[2] not in list):
			driver.get(url)
			try:
				search = driver.find_element_by_name('q')
				search.send_keys(row[0] + " " + row[2] + " " + row[4] + " musixmatch lyrics" )
				search.send_keys(Keys.RETURN)
			except Exception:
				continue
			try:
				driver.find_element_by_xpath('//a[starts-with(@href,"https://www.musixmatch.com/lyrics")]').click()
			except Exception:
				try:
					driver.find_element_by_xpath('//a[starts-with(@href,"https://www.musixmatch.com/album")]').click()
				except Exception:
					print("oh well")
			try:
				driver.find_element_by_id("track_0").click()
			except Exception:
				print("cool")
			try:
				driver.find_element_by_class("recaptcha-checkbox-checkmark").click()
			except Exception:
				print("ok")
				try:
					div = driver.find_element_by_class_name("banner-album-image-desktop")
				
					img = div.find_element_by_tag_name("img")
					# print(img)
					src = img.get_attribute('src')
					print(src)

					# download the image
					urllib.urlretrieve(src, "images/" + row[2] + ".jpg")
					list.append(row[2])
				except Exception:
					print("oh well")