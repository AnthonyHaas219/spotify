package com.spotify.entityManagers;

import java.util.ArrayList;
import java.util.List;

import com.spotify.entities.User;

public class UserManager {
	private static UserManager um;
	private List<User> userList;

	private UserManager() {
		userList = new ArrayList<User>();
	}
	
	public static UserManager getUserManager() {
		if(um == null) {
			um = new UserManager();
	    }
		return um;
	}
	
	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}
	
	/*search through user manager to return the user through id
	 * returns null if no user can be found
	 * */
	public User getUser(int id) {
		for(User u : userList) {
			if(u.getId() == id) {
				return u;
			}
		}
		return null;
	}
	
	/* search through user manager to remove the user through id
	 * returns null if no user can be removed
	 * */	
	public User removeUser(int id) {
		User tempUser = null;
		for(User u : userList) {
			if(u.getId() == id) {
				tempUser = u;
			}
		}
		if(tempUser != null) {
			userList.remove(tempUser);
		}
		return tempUser;
	}
	
	/* search through user manager to remove the user through id
	 * returns null if no user can be removed
	 * */	
	public void addUser(User u) {
		this.userList.add(u);
	}
	
}
