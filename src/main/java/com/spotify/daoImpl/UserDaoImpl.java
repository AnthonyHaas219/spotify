package com.spotify.daoImpl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spotify.dao.AbstractDao;
import com.spotify.dao.UserDao;
import com.spotify.entities.Album;
import com.spotify.entities.Artist;
import com.spotify.entities.Password;
import com.spotify.entities.Payment;
import com.spotify.entities.Playlist;
import com.spotify.entities.User;
import com.spotify.entityManagers.UserManager;

@Repository("userDao")
@Transactional
public class UserDaoImpl extends AbstractDao implements UserDao{
	
	private UserManager um = UserManager.getUserManager();
	
	public User getUser(String email) {
		String sql = "from User where email = :uEmail ";
		Query q = getSession().createQuery(sql)
				.setParameter("uEmail", email);
		List<User> result = q.list();
		return result.isEmpty() ? null : result.get(0);
	}

	public Password getPassword(User user) {
		if(user == null){
			return null;
		}
		String sql = "from Password where USER_ID = :id ";
		Query q = getSession().createQuery(sql);
		q.setParameter("id", user.getId());
		List<Password> result = q.list();
		return result.isEmpty() ? null : result.get(0);
	}
	
	public boolean updateLogin(User u){		
		if(u.isLoggedIn()) {
			u.setLoggedIn(false);
		}
		else {
			u.setLoggedIn(true);
		}
		try{
			getSession().saveOrUpdate(u);
			return true; 
		}
		catch(Exception e){
			return false;
		}
	}
	
	public boolean createUserDao(User u){
		if(u == null) {
			return false;
		}
		try{
			getSession().saveOrUpdate(u.getLibrary());
			getSession().saveOrUpdate(u.getQueue());
			getSession().saveOrUpdate(u);
			u.getPassword().setUser(u);
			getSession().saveOrUpdate(u.getPassword());
			return true;
		}
		catch(HibernateException e){
			return false;
		}
	}
	
	public boolean checkEmailAvailability(String newUserEmail) {
		String sql = "from User where email = :uEmail ";
		Query q = getSession().createQuery(sql)
				.setParameter("uEmail", newUserEmail);
		List<User> result = q.list();
		return result.isEmpty() ? true : false;
	}

	public List<User> getOwner(int id){
		String sql = "from User where id in (select o.u.id from Owner o where o.p.playlistId = :pid)";
		Query q = getSession().createQuery(sql)
				.setParameter("pid", id);
		List<User> result = q.list();
		/*cycle through users in list and add to user manager if it doesn't exist*/
		for(User u : result){
			if(um.getUser(u.getId()) == null){
				um.addUser(u);
			}
		}
		return result;
	}

	@Override
	public boolean upgradePremium(Payment info, User u) {
		try{
			/*update user to reflect premium user and persist*/
			u.setPremium(true);
			getSession().saveOrUpdate(u);
			/*save payment info*/
			info.setUser(u);
			getSession().saveOrUpdate(info);
			return true;
		}
		catch(HibernateException e){
			return false;
		}
	}

	@Override
	public boolean downgradePremium(User u) {
		try{
			u.setPremium(false);
			getSession().saveOrUpdate(u);
			return true;
		}
		catch(HibernateException e){
			return false;
		}
	}

	@Override
	public boolean deleteAccount(User u) {
		try{
			getSession().saveOrUpdate(u);
			return true;
		}
		catch(HibernateException e){
			return false;
		}
	}

	@Override
	public boolean changePassword(User u) {
		try{
			getSession().saveOrUpdate(u.getPassword());
			return true;
		}
		catch(HibernateException e){
			return false;
		}
	}

	@Override
	public User getUserById(int id) {
		return (User) getSession().get(User.class, id);
	}

	@Override
	public List<Artist> getFollowingArtist(int uid) {
		String sql = "from Artist a where a.artistID in (select f.a.artistID from Follow f where f.following.id = :id) and a.user.status != 1 and a.user.ban !=1";
		Query q = getSession().createQuery(sql)
				.setParameter("id", uid);
		List<Artist> result = q.list();
		return result;
	}

	@Override
	public List<Playlist> getFollowingPlaylist(int uid) {
		String sql = "from Playlist where playlistId in (select f.p.playlistId from Follow f where f.following.id = :id)";
		Query q = getSession().createQuery(sql)
				.setParameter("id", uid);
		List<Playlist> result = q.list();
		return result;
	}

	@Override
	public List<User> getFollowing(int id) {
		String sql = "from User where id in (select f.follower.id from Follow f where f.following.id = :id) and status != 1 and ban !=1";
		Query q = getSession().createQuery(sql)
				.setParameter("id", id);
		List<User> result = q.list();
		return result;
	}

	@Override
	public List<Playlist> getPublicPlaylist(int id) {
		// TODO Auto-generated method stub
		String sql = "from Playlist p where p.playlistId in (select o.p.playlistId from Owner o where o.u.id = :id) and p.playlistPrivate = 0";
		Query q = getSession().createQuery(sql)
				.setParameter("id", id);
		List<Playlist> result = q.list();
		return result;
	}
	
	public boolean updateUser(User u)
	{
		try{
			getSession().saveOrUpdate(u);
			return true; 
		}
		catch(Exception e){
			return false;
		}
	}

	@Override
	public List<Album> getLibraryAlbums(int id) {
		String sql = "";
		sql = "from Album a where a.albumId in "
				+ "(select s.album.albumId from Song s where s.id in"
				+ "(select soc.s.id from SongInCollection soc where soc.p.playlistId = :id)))";
		Query q = getSession().createQuery(sql)
				.setParameter("id", id);
		List<Album> result = q.list();
		return result;
	}
	
	@Override
	public List<Artist> getLibraryArtists(int id) {
		String sql = "";
		sql = "from Artist a where a.artistID in "
				+ "(select aof.a.artistID from ArtistOfSong aof where aof.s.id in"
				+ "(select soc.s.id from SongInCollection soc where soc.p.playlistId = :id)))";
		Query q = getSession().createQuery(sql)
				.setParameter("id", id);
		List<Artist> result = q.list();
		return result;
	}

	@Override
	public boolean isArtist(int id) {
		String sql = "from Artist where user.id = :id ";
		Query q = getSession().createQuery(sql)
				.setParameter("id", id);
		List<Artist> result = q.list();
		return result.isEmpty() ? false : true;
	}
	
	public Artist getArtistByUserId(int id){
		String sql = "from Artist where user.id = :id ";
		Query q = getSession().createQuery(sql)
				.setParameter("id", id);
		List<Artist> result = q.list();
		return result.isEmpty() ? null : (Artist) result.get(0);
	}
}
