angular.module("mySpotify").controller("albumsCtrl", function($scope, $rootScope, $http, $routeParams) {
    $scope.loadInfo = function() {
        $http.get("api/getAlbums/").success(function (response) {
            $scope.albumList = response;
   		 });
	}
});