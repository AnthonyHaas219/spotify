package com.spotify.dao;

import java.util.List;

import com.spotify.entities.Album;

public interface AlbumDao {
	public Album getAlbumById(int id);
	public Album getAlbum(int id);
	public boolean createAlbum(Album newAlbum);
	public List<Album> getFullAlbumList();
	public List<Album> getPopularAlbums();
}
