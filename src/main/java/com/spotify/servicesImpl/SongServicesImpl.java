package com.spotify.servicesImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spotify.dao.SongDao;
import com.spotify.entities.Song;
import com.spotify.services.SongServices;

@Service("songServices")
@Transactional
public class SongServicesImpl implements SongServices{
	
	@Autowired
	@Qualifier("songDao")
	SongDao songDao;
	
	//get list of song based on playlist id passed
	public List<Song> getSongsByPlaylistId(int id) {
		return songDao.getSongsByPlaylistId(id);
	}
	
	public Song getSongById(int songId) {
		return songDao.getSongById(songId);
	}
	
	public List<Song> getSongByArtistId(int aid) {
		return songDao.getSongByArtistId(aid);
	}

	@Override
	public int createSong(Song s) {
		return songDao.createSong(s);
	}
	@Override
	public List<Song> getSongByAlbumId(int aid) {
		return songDao.getSongByAlbumId(aid);
	}

	@Override
	public List<Song> getPopSongs() {
		// TODO Auto-generated method stub
		return songDao.getPopSongs();
	}

	@Override
	public boolean deleteSong(int id) {
		// TODO Auto-generated method stub
		return songDao.deleteSong(id);
	}
	
	public List<Song> getSongByArtistIdNoLimit(int aid){
		return songDao.getSongByArtistIdNoLimit(aid);
	}
	public boolean updateSong(Song s){
		return songDao.updateSong(s);
	}
}
