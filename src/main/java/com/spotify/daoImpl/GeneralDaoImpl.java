package com.spotify.daoImpl;

import java.lang.reflect.Array;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.spotify.entities.*;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spotify.dao.AbstractDao;
import com.spotify.dao.GeneralDao;

@Repository("generalDao")
@Transactional
public class GeneralDaoImpl extends AbstractDao implements GeneralDao {

	@Override
	public boolean unfollow(int id, int uid, char type){
		String sql = "";
		switch(type){
			case 'u':
				sql = "delete from Follow f where f.following.id = :uid AND f.follower.id = :id";
				break;
			case 'p':
				sql = "delete from Follow f where f.following.id = :uid AND f.p.playlistId = :id";
				break;
			case 'a':
				sql = "delete from Follow f where f.following.id = :uid AND f.a.artistID = :id";
				break;
			default:
				return false;
		}
    	Query q = getSession().createQuery(sql)
    			.setParameter("uid", uid)
    			.setParameter("id", id);
    	if(q.executeUpdate() > 0) {
    		return true;
    	}
    	return false;
	}
	
	public boolean follow(User following, User follower, Playlist p, Artist a) {
		Follow newFollow = new Follow(following, follower, p, a);
		try {
			getSession().saveOrUpdate(newFollow);
            return true;
        }
        catch (HibernateException e) {
            return false;
        }
	}
	
	public boolean addSongToCollection(Playlist p, Album a, Song s) {
		SongInCollection sc = new SongInCollection(s, a, p);
		try {
			getSession().saveOrUpdate(sc);
			return true;
		}
		catch(HibernateException e) {
			return false;
		}	
	}
	
	public boolean removeSongFromCollection(int songId, int id, char type) {
		String sql = "";
		switch(type) {
			case 'a':
				sql = "delete from SongInCollection sc where sc.s.id = :songId AND sc.a.albumId = :id";
				break;
			case 'p':
				sql = "delete from SongInCollection sc where sc.s.id = :songId AND sc.p.playlistId = :id";
				break;
			default:
				return false;
		}
		Query q = getSession().createQuery(sql)
		  			.setParameter("songId", songId)
		  			.setParameter("id", id);
		if(q.executeUpdate() > 0) {
			return true;
		}
		return false;	
	}
	
	public boolean createOwner(User tempUser,  Playlist tempPlaylist, Album tempAlbum) {
		Owner tempOwner = new Owner(tempUser, tempPlaylist, tempAlbum);
		try {
			getSession().saveOrUpdate(tempOwner);
			return true;
		}
		catch(HibernateException e) {
			return false;
		}
	}
	
	public List<User> getFollowers(int id, char type) {
		String sql = "";
		switch(type){
			case 'u':
				sql = "from User u where u.id in (select f.following.id from Follow f where f.follower.id = :id) and u.status != 1 and u.ban != 1";
				break;
			case 'p':
				sql = "from User u where u.id in (select f.following.id from Follow f where f.p.playlistId = :id) and u.status != 1 and u.ban != 1";
				break;
			case 'a':
				sql = "from User u where u.id in (select f.following.id from Follow f where f.a.artistID = :id) and u.status != 1 and u.ban != 1";
				break;
			default:
				return null;
		}
		Query q =  getSession().createQuery(sql)
				.setParameter("id", id);
		@SuppressWarnings("unchecked")
		List<User> result = q.list();
		return result;
	}

	@Override
	public HashMap<String,Object> search(String searchTerm) {
		HashMap<String, Object> searchResults = new HashMap<>();

		Query albumQuery = getSession().createSQLQuery("SELECT * from albums WHERE MATCH(title) AGAINST ('*" + searchTerm + "*' IN BOOLEAN MODE);")
				.addEntity("albums", Album.class);
		ArrayList<Album> albumResults = (ArrayList) albumQuery.list();
		searchResults.put("albums", albumResults);

		ArrayList<Integer> artistIds = new ArrayList<>();
		Query artistQuery = getSession().createSQLQuery("SELECT * from artists a, users u WHERE MATCH(a.groupName) "
				+ "AGAINST ('*" + searchTerm + "*' IN BOOLEAN MODE) and a.user_id = u.user_id and u.status != 1 and u.ban != 1;")
				.addEntity("artists", Artist.class);
		/*Query artistQuery = getSession().createSQLQuery("SELECT * from artists WHERE MATCH(groupName) AGAINST ('*" + searchTerm + "*' IN BOOLEAN MODE);")
				.addEntity("artists", Artist.class);*/
		ArrayList<Artist> artistResults = (ArrayList) artistQuery.list();
		for (Artist artistResult : artistResults) {
			artistIds.add(artistResult.getUser().getId());
		}
		searchResults.put("artists", artistResults);
		
		ArrayList<Integer> artistnotBanIds = new ArrayList<>();
		Query artistNotBanQuery = getSession().createSQLQuery("SELECT * from artists a, users u WHERE MATCH(a.groupName) "
				+ "AGAINST ('*" + searchTerm + "*' IN BOOLEAN MODE) and a.user_id = u.user_id and u.status != 1;")
				.addEntity("artists", Artist.class);
		ArrayList<Artist> artistNotBanResults = (ArrayList) artistNotBanQuery.list();
		for (Artist artistResult : artistNotBanResults) {
			artistnotBanIds.add(artistResult.getUser().getId());
		}
		searchResults.put("artistsNotBan", artistNotBanResults);

		Query playlistQuery = getSession().createSQLQuery("SELECT * from playlists WHERE MATCH(title) AGAINST ('*" + searchTerm + "*' IN BOOLEAN MODE) AND private = 0;")
				.addEntity("playlists", Playlist.class);
		ArrayList<Playlist> playlistResults = (ArrayList) playlistQuery.list();
		searchResults.put("playlists", playlistResults);

		Query songQuery = getSession().createSQLQuery("SELECT * from songs WHERE MATCH(songTitle) AGAINST ('*" + searchTerm + "*' IN BOOLEAN MODE);")
				.addEntity("songs", Song.class);
		ArrayList<Song> songResults = (ArrayList) songQuery.list();
		searchResults.put("songs", songResults);

		Query userQuery = getSession().createSQLQuery("SELECT * from users WHERE MATCH(firstName, lastName) AGAINST ('*" + searchTerm + "*' IN BOOLEAN MODE) and status != 1 and ban != 1;")
				.addEntity("users", User.class);
		ArrayList<User> userResults = (ArrayList) userQuery.list();

		for (Iterator<User> userIterator = userResults.iterator(); userIterator.hasNext(); ){
			User userResult = userIterator.next();
			if (artistIds.contains(userResult.getId())) {
				userIterator.remove();
			}
		}
		searchResults.put("users", userResults);
		
		Query userNotBanQuery = getSession().createSQLQuery("SELECT * from users WHERE MATCH(firstName, lastName) AGAINST ('*" + searchTerm + "*' IN BOOLEAN MODE) and status != 1;")
				.addEntity("users", User.class);
		ArrayList<User> userNotBanResults = (ArrayList) userNotBanQuery.list();

		for (Iterator<User> userIterator = userNotBanResults.iterator(); userIterator.hasNext(); ){
			User userResult = userIterator.next();
			if (artistnotBanIds.contains(userResult.getId())) {
				userIterator.remove();
			}
		}
		searchResults.put("usersNotBan", userNotBanResults);
		return searchResults;
	}

	public boolean setLanguage(User user, Lang lang) {
		user.setLang(lang);
		try {
			getSession().saveOrUpdate(user);
			return true;
		}
		catch(HibernateException e) {
			return false;
		}

	}

	public boolean removeAdvertisement(Advertisement advertisement){

		advertisement.setStatus(1);
		try {
			getSession().saveOrUpdate(advertisement);
			return true;
		}
		catch(HibernateException e) {
			return false;
		}

	}

	public List<Advertisement> getAdvertisements() {
		String sql = "from Advertisement a where a.status = 0";
		Query q = getSession().createQuery(sql);
		List<Advertisement> result = q.list();

		System.out.println(result);

		return result;

	}


	public Advertisement getAdvertisement(int adId){
		String sql = "from Advertisement a where a.id = :adId";
		Query q = getSession().createQuery(sql)
				.setParameter("adId", adId);
		System.out.println(q);
		List<Advertisement> result = q.list();
		Advertisement a = (Advertisement) result.get(0);
		return a;

	}

	public boolean createAdvertisement(Advertisement ad){
		try {
			getSession().saveOrUpdate(ad);
			return true;
		}
		catch(HibernateException e) {
			return false;
		}


	}

	public HashMap<String, Object> getStats() {
		Query numPlays = getSession().createSQLQuery("SELECT SUM(numberOfPlays) from eagles.songs;");

		Query totalRoyalties = getSession().createSQLQuery("SELECT SUM(royalty) from eagles.artists;");

		Query totalRegUserCount = getSession().createSQLQuery("SELECT COUNT(*) from eagles.users;");

		Query totalPremiumUserCount = getSession().createSQLQuery("SELECT COUNT(*) from eagles.users WHERE premium=1;");

		Query totalArtistCount = getSession().createSQLQuery("SELECT COUNT(*) from eagles.artists;");

		Query totalSongCount = getSession().createSQLQuery("SELECT COUNT(*) from eagles.songs;");

		Query totalAlbumCount = getSession().createSQLQuery("SELECT COUNT(*) from eagles.albums;");


		ArrayList<Integer> numPlaysList = (ArrayList<Integer>) numPlays.list();
		ArrayList<Integer> royaltiesList = (ArrayList<Integer>) totalRoyalties.list();
		ArrayList<BigInteger> totalRegUserCountList = (ArrayList<BigInteger>) totalRegUserCount.list();
		ArrayList<BigInteger> totalPremiumUserCountList = (ArrayList< BigInteger>) totalPremiumUserCount.list();
		ArrayList<BigInteger> totalArtistCountList = (ArrayList<BigInteger>) totalArtistCount.list();
		ArrayList<Integer> totalSongCountList = (ArrayList<Integer>) totalSongCount.list();
		ArrayList<Integer> totalAlbumCountList = (ArrayList<Integer>) totalAlbumCount.list();

		HashMap<String, Object> stats = new HashMap<>();
		stats.put("numPlays", numPlaysList.get(0));
		stats.put("royalties", royaltiesList.get(0));
		stats.put("regUserCount", totalRegUserCountList.get(0).intValue() - totalArtistCountList.get(0).intValue() - totalPremiumUserCountList.get(0).intValue());
		stats.put("premiumUserCount", totalPremiumUserCountList.get(0).intValue());
		stats.put("artistCount", totalArtistCountList.get(0).intValue());
		stats.put("songCount", totalSongCountList.get(0));
		stats.put("albumCount", totalAlbumCountList.get(0));
		return stats;
	}


}

