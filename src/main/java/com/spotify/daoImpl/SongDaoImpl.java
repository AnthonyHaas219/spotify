package com.spotify.daoImpl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spotify.dao.AbstractDao;
import com.spotify.dao.SongDao;
import com.spotify.entities.Song;
import com.spotify.entityManagers.SongManager;

@Repository("songDao")
@Transactional
public class SongDaoImpl extends AbstractDao implements SongDao{
	
	private SongManager sm = SongManager.getSongManager();
	
	public List<Song> getSongsByPlaylistId(int playlistId){
		String sql = "from Song where id in (select sc.s.id from SongInCollection sc where sc.p.playlistId = :pid)";
		Query q =  getSession().createQuery(sql)
				.setParameter("pid", playlistId);
		@SuppressWarnings("unchecked")
		List<Song> result = q.list();
		/*Place all songs in song manager if it doesn't exist*/
		for(Song s : result){
			if(sm.getSong(s.getId()) == null){
				sm.addSong(s);
			}
		}
		return result;
	}
	
	public Song getSongById(int songId){
		Song s;
		/*checks to see if song exist in user manager*/
		if((s = sm.getSong(songId)) != null){
			return s;
		}
		/*search to see if song exist*/
		s = (Song) getSession().get(Song.class, songId);
		return s;
	}
	
	public int createSong(Song newSong){
		try{
			getSession().saveOrUpdate(newSong);
			return newSong.getId();
		}
		catch(HibernateException e){
			return -1;
		}
	}

	@Override
	public boolean deleteSong(int id) {
		String sql = "Delete from Song where id = :id";
		Query q = getSession().createQuery(sql)
				.setParameter("id", id);
		if(q.executeUpdate()>0) {
			return true;
		}
		return false;
	}
	
	@Override
	public List<Song> getSongByArtistId(int aid) {
		String sql = "from Song s where s.id in (select aos.s.id from ArtistOfSong aos where aos.a.artistID = :id) order by s.numPlays DESC)";
		Query q = getSession().createQuery(sql)
				.setParameter("id", aid);
		q.setMaxResults(10);
		List<Song> result = q.list();
		/*Place all songs in song manager if it doesn't exist*/
		for(Song s : result) {
			if(sm.getSong(s.getId()) == null) {
				sm.addSong(s);
			}
		}
		return result;
	}
	
	public List<Song> getSongByArtistIdNoLimit(int aid) {
		String sql = "from Song s where s.id in (select aos.s.id from ArtistOfSong aos where aos.a.artistID = :id) order by s.numPlays DESC)";
		Query q = getSession().createQuery(sql)
				.setParameter("id", aid);
		List<Song> result = q.list();
		/*Place all songs in song manager if it doesn't exist*/
		for(Song s : result) {
			if(sm.getSong(s.getId()) == null) {
				sm.addSong(s);
			}
		}
		return result;
	}

	@Override
	public List<Song> getSongByAlbumId(int aid) {
		String sql = "from Song s where s.album.albumId = :id";
		Query q = getSession().createQuery(sql)
				.setParameter("id", aid);
		List<Song> result = q.list();
		/*Place all songs in song manager if it doesnt exist*/
		for(Song s: result){
			if(sm.getSong(s.getId()) == null){
				sm.addSong(s);
			}
		}
		return result;
	}
	
	public List<Song> getPopSongs()
	{
		String sql = "from Song order by numPlays desc";
		Query q = getSession().createQuery(sql);
		q.setMaxResults(10);
		List<Song> result = q.list();
		return result;
	}
	public boolean updateSong(Song s){
		try{
			getSession().saveOrUpdate(s);
			return true; 
		}
		catch(Exception e){
			return false;
		}
	}
}
