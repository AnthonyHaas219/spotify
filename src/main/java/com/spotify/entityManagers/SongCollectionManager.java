package com.spotify.entityManagers;

import java.util.ArrayList;
import java.util.List;

import com.spotify.entities.Album;
import com.spotify.entities.Playlist;
import com.spotify.entities.Song;
import com.spotify.entities.SongCollection;

public class SongCollectionManager {
	private static SongCollectionManager scm;
	private List<SongCollection> songCollectionList;
	
	private SongCollectionManager() {
		songCollectionList = new ArrayList<SongCollection>();
	}
	
	public static SongCollectionManager getSongCollectionManager() {
		if(scm == null) {
			scm = new SongCollectionManager();
	    }
		return scm;
	}
	
	public List<SongCollection> getSongList() {
		return songCollectionList;
	}

	public void setSongList(List<SongCollection> songCollection) {
		this.songCollectionList = songCollection;
	}
	/*find collection of song*/
	public SongCollection getCollection(SongCollection sc)
	{
		return songCollectionList.get((songCollectionList.indexOf(sc)));
	}
	/* remove an already existing collection */	
	public void removeCollection(SongCollection sc)
	{
		this.songCollectionList.remove(sc);
	}
	/*add a new collection to list*/
	public void addCollection(SongCollection sc)
	{
		this.songCollectionList.add(sc);
	}

	public Playlist getPlaylist(int id){
		for (SongCollection sc : songCollectionList) {
			if (sc instanceof Playlist) {
				Playlist playlist = (Playlist) sc;
				if (playlist.getPlaylistId() == id)
					return playlist;
			}
		}
		return null;
	}

	public Album getAlbum(int id){
		for (SongCollection sc : songCollectionList) {
			if (sc instanceof Album) {
				Album album = (Album) sc;
				if (album.getAlbumId() == id)
					return album;
			}
		}
		return null;
	}
}
