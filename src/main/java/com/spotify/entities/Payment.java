package com.spotify.entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "payments")
public class Payment {
	
	@Id
	@Column(name="PAYMENT_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@Column(name="cardNumber")
	private String cc;
	@Column(name="expmm")
	private int expmm;
	@Column(name="expyy")
	private int expyy;
	@Column(name="securityCode")
	private int secCode;
	@Column(name="address1")
	private String address1;
	@Column(name="address2")
	private String address2;
	@Column(name="city")
	private String city;
	@Column(name="state")
	private String state;
	@Column(name="zip")
	private int zip;
	@OneToOne
	@JoinColumn(name = "USER_ID", nullable = false, referencedColumnName = "USER_ID")
	private User user;
	
	public Payment(){}
	
	public Payment(String cc, int expmm, int expyy, int sec, String addr, String addr2, String city, String state, int zip, User u){
		this.cc = cc;
		this.expmm = expmm;
		this.expyy = expyy;
		this.secCode = sec;
		this.address1 = addr;
		this.address2 = addr2;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.user = u;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getCredit() {
		return cc;
	}
	
	public void setCredit(String credit) {
		this.cc = credit;
	}
	
	public int getExpmm() {
		return expmm;
	}
	
	public void setExpmm(int expmm) {
		this.expmm = expmm;
	}
	
	public int getExpyy() {
		return expyy;
	}
	
	public void setExpyy(int expyy) {
		this.expyy = expyy;
	}
	
	public int getSecCode() {
		return secCode;
	}
	
	public void setSecCode(int secCode) {
		this.secCode = secCode;
	}
	
	public String getAddress1() {
		return address1;
	}
	
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	
	public String getAddress2() {
		return address2;
	}
	
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public int getZip() {
		return zip;
	}
	
	public void setZip(int zip) {
		this.zip = zip;
	}
	
	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean verifyCreditCard() {
		int sum = 0;
		int length = cc.length();
		int parity = length % 2;
		int digit;
		
		for(int i = 0; i < length; i ++) {
			digit = Character.getNumericValue(cc.charAt(i));
			if(i%2 == parity) {
				digit = digit*2;
				if(digit > 9) {
					digit = digit - 9;
				}
			}
			sum += digit;
		}
		if(sum % 10 == 0) {
			return true;
		}
		return false;
	}
}
