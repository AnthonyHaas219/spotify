package com.spotify.entityManagers;

import java.util.ArrayList;
import java.util.List;

import com.spotify.entities.Song;

public class SongManager {
	private static SongManager sm;
	private List<Song> songList;

	private SongManager() {
		songList = new ArrayList<Song>();
	}
	
	public static SongManager getSongManager() {
		if(sm == null) {
			sm = new SongManager();
	    }
		return sm;
	}
	
	public List<Song> getSongList() {
		return songList;
	}

	public void setSongList(List<Song> songList) {
		this.songList = songList;
	}
	
	public Song getSong(int id) {
		for(Song song : songList) {
			if(song.getId() == id)
			 return song;
		}
		return null;
	}
	
	/* search through user manager to remove the user through id
	 * returns null if no user can be removed
	 * */	
	public Song removeSong(int id) {
		Song tempSong = null;
		
		for(Song song : songList) {
			if(song.getId() == id)
				tempSong = song;
		}
		if(tempSong != null) {
			this.songList.remove(tempSong);
		}
		return tempSong;
	}
	
	/* search through user manager to remove the user through id
	 * returns null if no user can be removed
	 * */	
	public void addSong(Song s) {
		this.songList.add(s);
	}
}
