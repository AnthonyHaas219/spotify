import requests
from bs4 import BeautifulSoup
import csv

base_url = "https://api.genius.com"
headers = {'Authorization': 'Bearer B1LZte4LEcMhkG9Hamrfv4NVzorwEF73faLg41jgrPSSU232rHrHSJUdCDQCQZDL'}



def lyricsFromSongApiPath(songApiPath):
  song_url = baseUrl + songApiPath
  response = requests.get(songUrl, headers=headers)
  json = response.json()
  path = json["response"]["song"]["path"]
  pageUrl = "https://genius.com" + path
  page = requests.get(pageUrl)
  html = BeautifulSoup(page.text, "html.parser")
  [h.extract() for h in html('script')]
  lyrics = html.find("div", class_="lyrics").get_text() 
  return lyrics

if __name__ == "__main__":
  with open("spotifySongs.csv", "rb") as f:
    reader = csv.reader(f)
    test = True
    for row in reader:
      if test == True:
        test = False
        continue
      title = row[0]
      artistName = row[0]
      searchUrl = baseUrl + "/search"
      data = {'q': title}
      response = requests.get(searchUrl, data=data, headers=headers)
      json = response.json()
      song_info = None
      for hit in json["response"]["hits"]:
        if hit["result"]["primaryArtist"]["name"] == artistName:
          songInfo = hit
          break
      if songInfo:
        songApiPath = songInfo["result"]["apiPath"]
        file = open("lyrics/" + row[4] + ".txt","w") 
        file.write(lyricsFromSongApiPath(songApiPath).encode('utf-8'))
        file.close()