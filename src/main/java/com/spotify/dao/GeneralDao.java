package com.spotify.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.spotify.entities.*;

public interface GeneralDao {
	public boolean unfollow(int id, int uid, char type);
	public boolean follow(User following, User follower, Playlist p, Artist a);
	public boolean addSongToCollection(Playlist p, Album a, Song s);
	public boolean removeSongFromCollection(int songId, int id, char type);
	public boolean createOwner(User tempUser,  Playlist tempPlaylist, Album tempAlbum);
	public HashMap<String,Object> search(String searchTerm);
	public boolean setLanguage(User user, Lang lang);
	public List<User> getFollowers(int id, char type);
	public boolean removeAdvertisement(Advertisement ad);
	public List<Advertisement> getAdvertisements();
	public boolean createAdvertisement(Advertisement ad);
	public HashMap<String, Object> getStats();
	public Advertisement getAdvertisement(int id);
}
