package com.spotify.daoImpl;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spotify.dao.AbstractDao;
import com.spotify.dao.GeneralDao;
import com.spotify.dao.PlaylistDao;
import com.spotify.dao.UserDao;
import com.spotify.entities.Playlist;
import com.spotify.entities.User;
import com.spotify.entityManagers.SongCollectionManager;
import com.spotify.forms.CreatePlaylistForm;

@Repository("playlistDao")
@Transactional
public class PlaylistDaoImpl extends AbstractDao implements PlaylistDao{

    public static String UPLOAD_DIR = "src/main/resources/static/images/";
    @Autowired
    @Qualifier("userDao")
    private UserDao userDao;
    @Autowired
    @Qualifier("generalDao")
    private GeneralDao generalDao;

    public Playlist getPlaylist(int id) {
        SongCollectionManager  songCollectionManager = SongCollectionManager.getSongCollectionManager();
        Playlist playlist = songCollectionManager.getPlaylist(id);
        if (playlist != null) {
            return playlist;
        }
        playlist = (Playlist) getSession().get(Playlist.class, id);
        songCollectionManager.addCollection(playlist);
        return playlist;
    }

    public List<Playlist> getPlaylists(int userId){

        String sql = "select f.p.playlistId from Follow f where f.following.id = :UserId AND f.p.playlistId IS NOT NULL";
        Query q = getSession().createQuery(sql)
                .setParameter("UserId", userId);
        List<Integer> result = q.list();
        List<Playlist> playlists = new ArrayList<Playlist>();
        SongCollectionManager  songCollectionManager = SongCollectionManager.getSongCollectionManager();
        for(int i = 0; i < result.size(); i++) 
        {
            Playlist playlist = songCollectionManager.getPlaylist((int)result.get(i));
            if (playlist != null) {
                playlists.add(playlist);
            }
            else {
                playlist = (Playlist) getSession().get(Playlist.class, (int) result.get(i));
                playlists.add(playlist);
            }
        }
        return playlists;
    }

    public int addPlaylist(CreatePlaylistForm playlistForm, User user) {
        Playlist playlist = new Playlist();
        playlist.setDateAdded(new Date(System.currentTimeMillis()));
        playlist.setDescription(playlistForm.getPlaylistDescription());
        playlist.setTitle(playlistForm.getPlaylistName());
        String fileName = playlistForm.getPlaylistImage().getOriginalFilename();
        String filePath = UPLOAD_DIR + fileName;
        String userDir = System.getProperty("user.dir") + "/";
        File image = new File(userDir + filePath);
        try {
            if (!image.exists()) {
                image.createNewFile();
            }
            else {
                int i = 1;
                String fileNameNoExtension = fileName.substring(0, fileName.lastIndexOf("."));
                String fileNameExtension = fileName.substring(fileName.lastIndexOf("."));
                String newFileName = fileNameNoExtension + i;
                image = new File(userDir + UPLOAD_DIR + newFileName + fileNameExtension);
                while(image.exists()) {
                    i += 1;
                    newFileName = fileNameNoExtension + i;
                    image = new File(userDir + UPLOAD_DIR + newFileName + fileNameExtension);
                }
                image.createNewFile();
                fileName = newFileName + fileNameExtension;
            }
            playlistForm.getPlaylistImage().transferTo(image);
            playlist.setImageLoc(fileName);
        }
        catch (IOException e) {
            return -1;
        }
        SongCollectionManager songCollectionManager = SongCollectionManager.getSongCollectionManager();
        songCollectionManager.addCollection(playlist);
        try {
            getSession().saveOrUpdate(playlist);
            /*creates a follow for this playlist for the user*/
            generalDao.follow(user, null, playlist, null);
            /*creates owner for this playlist*/
            generalDao.createOwner(user, playlist, null);
            return playlist.getPlaylistId();
        }
        catch (HibernateException e){
            return -1;
        }
    }
    
    public boolean renamePlaylist(User u, Playlist p, String newName) {
		boolean isOwner = true;
		//checks to see if user is an owner, if not return false
		for(User owner: p.getOwners()){
			if(owner.equals(u)){
				isOwner = true;
				break;
			}
		}
		if(isOwner == false) {
			return isOwner;
		}
		p.setTitle(newName);
		try{
			getSession().saveOrUpdate(p);
			return true;
		}
		catch(HibernateException e){
			return false;
		}
	}


    
    public boolean deletePlaylist(int id){
		// TODO Auto-generated method stub
		String sql = "Delete from playlist where playlistId = :id";
		Query q = getSession().createQuery(sql)
				.setParameter("id", id);
		if(q.executeUpdate()>0) {
			return true;
		}
		return false;
    }

    @Override
    public boolean updatePlaylist(Playlist p) {
        try{
            getSession().saveOrUpdate(p);
            return true;
        }
        catch(Exception e){
            return false;
        }
    }



}
