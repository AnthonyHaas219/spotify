package com.spotify.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.spotify.entities.*;

public interface GeneralServices {
	public boolean unfollow(int id, int uid, char type);
	public boolean follow(User following, User follower, Playlist p, Artist a);
	public boolean addSongToCollection(Playlist p, Album a, Song s);
	public boolean removeSongFromCollection(int songId, int id, char type);
	public List<User> getFollowers(int id, char type);
	public HashMap<String,Object> getSearchResults(String searchTerm);
	public boolean setLang(User user, Lang lang);
	public List<Advertisement> getAdvertisements();
	public boolean removeAdvertisement(Advertisement ad);
	public boolean createAdvertisement(Advertisement ad);
	public Advertisement getAdvertisement(int adId);
	public HashMap<String, Object> getAllStats();
}
