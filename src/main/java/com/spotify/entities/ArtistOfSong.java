package com.spotify.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="ArtistOfSongs")
public class ArtistOfSong implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@OneToOne
	@JoinColumn(name = "SONG_ID", nullable = false, referencedColumnName = "SONG_ID")
	private Song s;
	@Id
	@OneToOne
	@JoinColumn(name = "ARTIST_ID", nullable = false, referencedColumnName = "ARTIST_ID")
	private Artist a;
	
	public ArtistOfSong() {}
	
	public ArtistOfSong(Song s, Artist a) {
		this.s = s;
		this.a = a;
	}
	
	public Song getS() {
		return s;
	}
	
	public Artist getA() {
		return a;
	}
	
	public void setS(Song s) {
		this.s = s;
	}
	
	public void setA(Artist a) {
		this.a = a;
	}	
}
