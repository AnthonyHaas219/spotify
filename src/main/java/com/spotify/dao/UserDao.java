package com.spotify.dao;

import java.util.List;

import com.spotify.entities.Album;
import com.spotify.entities.Artist;
import com.spotify.entities.Password;
import com.spotify.entities.Payment;
import com.spotify.entities.Playlist;
import com.spotify.entities.User;

public interface UserDao {	
	public User getUser(String email);
	public Password getPassword(User user);
	public boolean updateLogin(User u);
	public boolean createUserDao(User u);
	public boolean checkEmailAvailability(String newUserEmail);
	public List<User> getOwner(int id);
	public boolean upgradePremium(Payment info, User u);
	public boolean downgradePremium(User u);
	public boolean deleteAccount(User u);
	public boolean changePassword(User u);
	public User getUserById(int id);
	public List<Artist> getFollowingArtist(int uid);
	public List<Playlist> getFollowingPlaylist(int uid);
	public List<User> getFollowing(int id);
	public List<Playlist> getPublicPlaylist(int id);
	public boolean updateUser(User u);
	public List<Album> getLibraryAlbums(int id);
	public List<Artist> getLibraryArtists(int id);
	public boolean isArtist(int id);
	public Artist getArtistByUserId(int id);
}
