package com.spotify.servicesImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.spotify.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spotify.dao.GeneralDao;
import com.spotify.services.GeneralServices;

@Service("generalServices")
@Transactional
public class GeneralServicesImpl implements GeneralServices{
	
	@Autowired
	@Qualifier("generalDao")
	private GeneralDao generalDao;

	@Override
	public boolean unfollow(int id, int uid, char type) {
		// TODO Auto-generated method stub
		return generalDao.unfollow(id, uid, type);	
	}
	
	public boolean follow(User following, User follower, Playlist p, Artist a) {
		return generalDao.follow(following, follower, p, a);
	}
	
	public boolean addSongToCollection(Playlist p, Album a, Song s) {
		return generalDao.addSongToCollection(p, a, s);
	}
	
	public boolean removeSongFromCollection(int songId, int id, char type) {
		return generalDao.removeSongFromCollection(songId, id, type);
	}
	
	public List<User> getFollowers(int id, char type)
	{
		return generalDao.getFollowers(id, type);
	}

	@Override
	public HashMap<String,Object> getSearchResults(String searchTerm) {
		return generalDao.search(searchTerm);
	}

	@Override
	public boolean setLang(User user, Lang lang) {
		return generalDao.setLanguage(user, lang);
	}

	public List<Advertisement> getAdvertisements(){
		return generalDao.getAdvertisements();
	}

	public boolean removeAdvertisement(Advertisement ad){
		return generalDao.removeAdvertisement(ad);
	}

	public boolean createAdvertisement(Advertisement ad){

		return generalDao.createAdvertisement(ad);
	}

	@Override
	public Advertisement getAdvertisement(int adId) {
		return generalDao.getAdvertisement(adId);
	}

	@Override
	public HashMap<String, Object> getAllStats() {
		return generalDao.getStats();
	}
}
