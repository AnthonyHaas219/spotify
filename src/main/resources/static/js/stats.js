angular.module("mySpotify").controller("statsCtrl", function ($scope, $rootScope, $http, $routeParams, $dialogs) {

    function loadStats() {
        $http.get("/api/getStats").success(function (response) {
            $scope.numPlays = response.numPlays;
            $scope.regUserCount = response.regUserCount;
            $scope.premiumUserCount = response.premiumUserCount;
            $scope.artistCount = response.artistCount;
            $scope.songCount = response.songCount;
            $scope.albumCount = response.albumCount;
            $scope.royalties = parseInt($scope.numPlays) * 0.05;
        })
    }
    loadStats();
});