package com.spotify.forms;

import org.springframework.web.multipart.MultipartFile;

public class AddProfileImageForm {

    private MultipartFile profileImage;

    public MultipartFile getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(MultipartFile profileImage) {
        this.profileImage = profileImage;
    }
}