package com.spotify.forms;

import org.springframework.web.multipart.MultipartFile;

import java.sql.Date;

public class AddSongForm {
    private MultipartFile songFile;
    private String songTitle;
    private int songLength;
    private String lyrics;
    private String dateAdded;
    private int ALBUM_ID;

    public MultipartFile getSongFile() {
        return songFile;
    }

    public void setSongFile(MultipartFile songFile) {
        this.songFile = songFile;
    }

    public String getSongTitle() {
        return songTitle;
    }

    public void setSongTitle(String songTitle) {
        this.songTitle = songTitle;
    }

    public int getSongLength() {
        return songLength;
    }

    public void setSongLength(int songLength) {
        this.songLength = songLength;
    }

    public String getLyrics() {
        return lyrics;
    }

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public int getALBUM_ID() {
        return ALBUM_ID;
    }

    public void setALBUM_ID(int ALBUM_ID) {
        this.ALBUM_ID = ALBUM_ID;
    }
}
