package com.spotify.forms;

import org.springframework.web.multipart.MultipartFile;

public class UpdatePlaylistForm {

    private MultipartFile playlistImage;

    public MultipartFile getPlaylistImage() {
        return playlistImage;
    }

    public void setPlaylistImage(MultipartFile playlistImage) {
        this.playlistImage = playlistImage;
    }
}