angular.module("mySpotify").controller("searchCtrl", function ($scope, $rootScope, $http, $routeParams) {
     var searchWord = $routeParams.searchWord;
   $scope.doSearch = function() {
        $http.get("api/search/" + searchWord).success(function (response) {
            $scope.results = response;
            console.log(response);
        });
    }

    $scope.doSearchBan = function(searchWord) {
        $http.get("api/search/" + searchWord).success(function (response) {
            $scope.searchResults = response;
            console.log(response);
        });
   	}

    $scope.doFriendSearch = function(searchWord) {
        console.log("in doFriendSearch");
        var list = $scope.$parent.friends;
        $scope.friendResults = [];
        for (var j=0; j<list.length; j++) {
            if (list[j].firstName.match(searchWord) || list[j].lastName.match(searchWord)){
                $scope.friendResults.push(list[j]);
            }
        }
        console.log($scope.friendResults);
    }

    $scope.deleteArtist = function(id){
        $http({
            url: '/api/adminDeleteAccount',
            method: "POST",
            data: { "id" : id}
        })
        .then(function(response) {
            for(i=0; i<$scope.searchResults.artistsNotBan.length; i++){
                if(id == $scope.searchResults.artistsNotBan[i].user.id)
                {
                    $scope.searchResults.artistsNotBan[i].user.status=true;
                    $scope.searchResults.artistsNotBan.splice(i,1);
                    break;
                }
            }
            console.log("account Banned:" + id)
        });
    }

    $scope.deleteUser = function(id){
        $http({
            url: '/api/adminDeleteAccount',
            method: "POST",
            data: { "id" : id}
        })
        .then(function(response) {
            for(i=0; i<$scope.searchResults.usersNotBan.length; i++){
                if(id == $scope.searchResults.usersNotBan[i].id)
                {
                    $scope.searchResults.usersNotBan[i].status=false;
                    $scope.searchResults.usersNotBan.splice(i,1);
                    break;
                }
            }
            for(var j=0; j<$scope.$parent.friends.length;j++){
                if(id==$scope.$parent.friends[j].id){
                    $scope.$parent.friends.splice(j,1);
                }
            }
            console.log("account Unbanned:" + id)
        });
    }

    $scope.setBanUser = function(user){
        if(user.ban){
            $http({
                url: '/api/unbanAccount',
                method: "POST",
                data: { "unbanId" : user.id}
            })
            .then(function(response) {
                for(i=0; i<$scope.searchResults.usersNotBan.length; i++){
                    if(user.id == $scope.searchResults.usersNotBan[i].id)
                    {
                        $scope.searchResults.usersNotBan[i].ban=false;
                        break;
                    }
                }
                console.log("account Unbanned:" + user.id)
            });
        }else{
            $http({
                url: '/api/banAccount',
                method: "POST",
                data: { "banId" : user.id}
            })
            .then(function(response) {
                for(i=0; i<$scope.searchResults.usersNotBan.length; i++){
                    if(user.id == $scope.searchResults.usersNotBan[i].id)
                    {
                        $scope.searchResults.usersNotBan[i].ban=true;
                        break;
                    }
                }
                console.log("account Banned:" + user.id)
            });
        }
    }

    $scope.setBanArtist = function(artist){
        if(artist.user.ban){
            $http({
                url: '/api/unbanAccount',
                method: "POST",
                data: { "unbanId" : artist.user.id}
            })
            .then(function(response) {
                for(i=0; i<$scope.searchResults.artistsNotBan.length; i++){
                    if(artist.user.id == $scope.searchResults.artistsNotBan[i].user.id)
                    {
                        $scope.searchResults.artistsNotBan[i].user.ban=false;
                        break;
                    }
                }
                console.log("account Unbanned:" + artist.user.id)
            });
        }else{
            $http({
                url: '/api/banAccount',
                method: "POST",
                data: { "banId" : artist.user.id}
            })
            .then(function(response) {
                for(i=0; i<$scope.searchResults.artistsNotBan.length; i++){
                    if(artist.user.id == $scope.searchResults.artistsNotBan[i].user.id)
                    {
                        $scope.searchResults.artistsNotBan[i].user.ban=true;
                        break;
                    }
                }
                console.log("account Banned:" + artist.user.id)
            });
        }
    }
});