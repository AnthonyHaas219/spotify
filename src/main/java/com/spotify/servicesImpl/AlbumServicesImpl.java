package com.spotify.servicesImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spotify.dao.AlbumDao;
import com.spotify.entities.Album;
import com.spotify.services.AlbumServices;

@Service("albumServices")
@Transactional
public class AlbumServicesImpl implements AlbumServices{
	
	@Autowired
	@Qualifier("albumDao")
	private AlbumDao albumDao;
	
	public Album getAlbumById(int id) {
		return albumDao.getAlbum(id);
	}

	@Override
	public List<Album> getFullAlbumList() {
		return albumDao.getFullAlbumList();
	}

	@Override
	public List<Album> getPopularAlbums() {
		// TODO Auto-generated method stub
		return albumDao.getPopularAlbums();
	}
}
