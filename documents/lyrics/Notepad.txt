

[Chorus]
Feels like we're on the edge right now
I wish that I could say I'm proud
I'm sorry that I let you down
Let you down
All these voices in my head get loud
I wish that I could shut them out
I'm sorry that I let you down
Le-le-let you down

[Verse 1]
Yeah, I guess I'm a disappointment
Doin' everything I can, I don't wanna make you disappointed, it's annoying
I just wanna make you feel like everything I ever did wasn't ever tryna make an issue for you, but, I guess the more you
Thought about everything, you were never even wrong in the first place, right? Yeah, I'ma just ignore you
Walking towards you with my head down, lookin' at the ground, I'm embarrassed for you
Paranoia, what did I do wrong this time? That's parents for you, very loyal?
Shoulda had my back, but you put a knife in it—my hands are full
What else should I carry for you? I cared for you, but

[Chorus]
Feels like we're on the edge right now
I wish that I could say I'm proud
I'm sorry that I let you down
Le-le-let you down
All these voices in my head get loud
I wish that I could shut them out
I'm sorry that I let you down
Le-le-let you down

[Verse 2]
Yeah, you don't wanna make this work, you just wanna make this worse
Want me to listen to you, but you don't ever hear my words, you don't wanna know my hurt yet
Let me guess, you want an apology, probably, how can we
Keep going at a rate like this? We can't, so I guess I'ma have to leave
Please don't come after me, I just wanna be alone right now, I don't really wanna think at all
Go ahead, just drink it off, both know you're gonna call tomorrow like nothing's wrong
Ain't that what you always do? I feel like every time I talk to you, you're in an awful mood
What else can I offer you? There's nothing left right now, I gave it all to you

[Chorus]
Feels like we're on the edge right now
I wish that I could say I'm proud
I'm sorry that I let you down
Le-le-let you down
All these voices in my head get loud
I wish that I could shut them out
I'm sorry that I let you down
Le-le-let you down

[Verse 3]
Yeah, don't talk down to me, that's not gonna work now
Packed all my clothes and I moved out, I don't even wanna go to your house
Everytime I sit on that couch, I feel like you lecture me, eventually, I bet that we
Could have made this work, and probably woulda figured things out
But I guess I'm a letdown, but it's cool, I checked out, oh, you wanna be friends now?
Okay, let's put my fake face on and pretend now, sit around, and
Talk about the good times that didn't even happen, I mean, why are you laughing?
Must have missed that joke, let me see if I can find a reaction, no, but at least you're happy

[Chorus]
Feels like we're on the edge right now
I wish that I could say I'm proud
I'm sorry that I let you down
Oh, I let you down
All these voices in my head get loud
And I wish that I could shut them out
I'm sorry that I let you down
Oh, let you down

[Outro]
I'm sorry
I'm so sorry now
I'm sorry
That I let you down

