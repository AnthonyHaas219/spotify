package com.spotify.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "songs")
public class Song {

	@Id
	@Column(name="SONG_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name="songTitle")
	private String title;
	@Transient
	private List<Artist> artists;
	@Column(name="songLength")
	private int length;
	@Column(name="dateAdded")
	private Date dateAdded;
	@Column(name="lyric")
	private String lyrics;
	@Column(name="numberOfPlays")
	private int numPlays;
	@Column(name="songLocation")
	private String songLocation;
	@OneToOne
	@JoinColumn(name = "ALBUM_ID", nullable = false, referencedColumnName = "ALBUM_ID")
	private Album album;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Artist> getArtists() {
		return artists;
	}

	public void setArtists(List<Artist> artists) {
		this.artists = artists;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public Date getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

	public String getLyrics() {
		return lyrics;
	}

	public void setLyrics(String lyrics) {
		this.lyrics = lyrics;
	}

	public int getNumPlays() {
		return numPlays;
	}

	public void setNumPlays(int numPlays) {
		this.numPlays = numPlays;
	}

	public String getSongLocation() {
		return songLocation;
	}

	public void setSongLocation(String songLocation) {
		this.songLocation = songLocation;
	}

	/*used to add and remvoe from artist list*/
	public Album getAlbum() {
		return album;
	}

	public void setAlbum(Album songAlbum) {
		this.album = songAlbum;
	}

	public void addArtist(Artist artistToAdd) {
		this.artists.add(artistToAdd);
	}

	public void removeArtist(Artist artistToRemove) {
		this.artists.remove(artistToRemove);
	}

	/*add to number of plays*/
	public void addPlay() {
		this.numPlays++;
	}

	public void printSong() {
	    System.out.printf("Song: %d %s %d %s %s %d %s\n", this.id, this.title, this.length, this.dateAdded.toString(), this.lyrics, this.numPlays, this.songLocation);
	}
/*methods needed - get song file and get song file(time)*/
}
