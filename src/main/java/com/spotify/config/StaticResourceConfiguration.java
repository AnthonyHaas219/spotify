package com.spotify.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.spotify.interceptor.CheckLoginStatusInterceptor;

@Configuration
public class StaticResourceConfiguration extends WebMvcConfigurerAdapter {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("images/**")
        	.addResourceLocations("file:src/main/resources/static/images/");
        registry.addResourceHandler("songs/**")
                .addResourceLocations("file:src/main/resources/static/songs/");
    }


    
	@Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new CheckLoginStatusInterceptor())
            .excludePathPatterns("/css/**")
            .excludePathPatterns("/images/**")
            .excludePathPatterns("/js/**");
    }
}
