package com.spotify.servicesImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spotify.dao.GeneralDao;
import com.spotify.dao.PlaylistDao;
import com.spotify.entities.Playlist;
import com.spotify.entities.User;
import com.spotify.forms.CreatePlaylistForm;
import com.spotify.services.PlaylistServices;

@Service("playlistServices")
@Transactional
public class PlaylistServicesImpl implements PlaylistServices{
	
	@Autowired
	@Qualifier("playlistDao")
	private PlaylistDao playlistDao;
	@Autowired
	@Qualifier("generalDao")
	private GeneralDao generalDao;

	public int createPlaylistService(CreatePlaylistForm playlistForm, User user) {
		return playlistDao.addPlaylist(playlistForm, user);
	}
	
	public boolean removePlaylist(int id, int uid, char type) {
		return generalDao.unfollow(id, uid, 'p');		
	}

	public Playlist getPlaylist(int id) {
		Playlist playlist = playlistDao.getPlaylist(id);
		return playlist == null ? null : playlist;
	}

	public List<Playlist> getPlaylists(int userId) {
		return playlistDao.getPlaylists(userId);
	}
	
	public boolean renamePlaylist(User u, Playlist p, String newName) {
		return playlistDao.renamePlaylist(u, p, newName);
	}

	public boolean updatePlaylist(Playlist p){
		return playlistDao.updatePlaylist(p);
	}
}
