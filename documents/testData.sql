#Playlist Load
INSERT INTO PLAYLISTS (title, imageLoc, description, dateAdded, private) VALUES('test', 'test', 'test', '1994/05/05',0);
INSERT INTO PLAYLISTS (title, imageLoc, description, dateAdded, private) VALUES('test2', 'test2', 'test2', '1994/05/05',0);
INSERT INTO PLAYLISTS (title, imageLoc, description, dateAdded, private) VALUES('test3', 'test3', 'test3', '1994/05/05',0);

#insert album
INSERT INTO ALBUMS (artistid, title ,imageLoc, dateAdded) VALUES(1,'test1', 'test1', '1994/05/05');

#insert owner
INSERT INTO owner(USER_ID, PLAYLIST_ID, ALBUM_ID) Values(1,9, NULL);
INSERT INTO owner(USER_ID, PLAYLIST_ID, ALBUM_ID) Values(2,2, NULL);

#insert into followers table
INSERT INTO follows(FOLLOWING_USER_ID,FOLLOW_USER_ID, PLAYLIST_ID,ARTIST_ID) Values(1,2,NULL,NULL);

# insert song
INSERT INTO songs(songTitle, songLength, dateAdded, ALBUM_ID, numberOfPlays, songLocation, lyric) values('test', 180, '2017/11/19', 1, 0, "test1", NULL);
INSERT INTO songs(songTitle, songLength, dateAdded, ALBUM_ID, numberOfPlays, songLocation, lyric) values('test2', 180, '2017/11/19', 1, 0, "test2", NULL);

#song in playlist
INSERT INTO songincollection(SONG_ID, ALBUM_ID, PLAYLIST_ID) values(1,NULL,3);
INSERT INTO songincollection(SONG_ID, ALBUM_ID, PLAYLIST_ID) values(2,NULL,3);

#insert artist
INSERT INTO artists(groupName, USER_ID, listenCount, artistRank, followCount, royalty, bio) values('test1', 2, 0, 1, 0, 0, NULL);

#insert song owner
INSERT INTO artistOfSongs(song_id, artist_id) values(1,1)

