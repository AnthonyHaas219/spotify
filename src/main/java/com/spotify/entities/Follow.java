package com.spotify.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Follows")
public class Follow {
	@Id
	@Column(name="FOLLOW_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int followId;
	@OneToOne
	@JoinColumn(name = "FOLLOWING_USER_ID", nullable = true, referencedColumnName = "USER_ID")
	private User following;
	@OneToOne
	@JoinColumn(name = "FOLLOW_USER_ID", nullable = true, referencedColumnName = "USER_ID")
	private User follower;
	@OneToOne
	@JoinColumn(name = "PLAYLIST_ID", nullable = true, referencedColumnName = "PLAYLIST_ID")
	private Playlist p;
	@OneToOne
	@JoinColumn(name = "ARTIST_ID", nullable = true, referencedColumnName = "ARTIST_ID")
	private Artist a;
	
	public Follow() {}
	
	public Follow(User following, User follower, Playlist p, Artist a) {
		this.following = following;
		this.follower = follower;
		this.p = p;
		this.a = a;
	}
	
	public int getFollowId() {
		return followId;
	}
	
	public User getFollowing() {
		return following;
	}
	
	public User getFollower() {
		return follower;
	}
	
	public Playlist getP() {
		return p;
	}
	
	public Artist getA() {
		return a;
	}
	
	public void setFollowId(int followId) {
		this.followId = followId;
	}
	
	public void setFollowing(User following) {
		this.following = following;
	}
	
	public void setFollower(User follower) {
		this.follower = follower;
	}
	
	public void setP(Playlist p) {
		this.p = p;
	}
	
	public void setA(Artist a) {
		this.a = a;
	}	
}
