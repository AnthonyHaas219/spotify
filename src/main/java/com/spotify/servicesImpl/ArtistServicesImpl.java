package com.spotify.servicesImpl;

import java.util.List;

import com.spotify.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spotify.dao.AlbumDao;
import com.spotify.dao.ArtistDao;
import com.spotify.services.ArtistServices;

@Service("artistServices")
@Transactional
public class ArtistServicesImpl implements ArtistServices{

	@Autowired
	@Qualifier("artistDao")
	private ArtistDao artistDao;
	
	public Artist getArtistById(int id) {
		Artist tempArtist = artistDao.getArtistById(id);
		//get albums for playlist
		tempArtist.setAlbums(artistDao.getArtistAlbumList(id));
		tempArtist.setConcerts(artistDao.getArtistConcertList(id));
		return tempArtist;
	}
	
	public List<Album> getArtistAlbumList(int aid) {
		return artistDao.getArtistAlbumList(aid);
	}
	
	public List<Artist> getArtistBySong(int sid){
		return artistDao.getArtistBySong(sid);
	}
	
	public Artist getArtistByName(String name){
		Artist tempArtist = artistDao.getArtistByName(name);
		//get albums for playlist
		tempArtist.setAlbums(artistDao.getArtistAlbumList(tempArtist.getArtistID()));
		tempArtist.setConcerts(artistDao.getArtistConcertList(tempArtist.getArtistID()));
		return tempArtist;
	}

	@Override
	public List<Concert> getArtistConcertList(int id) {
		// TODO Auto-generated method stub
		return artistDao.getArtistConcertList(id);
	}

	public boolean createArtist(Artist a) {
		return artistDao.createArtist(a);
	}
	
	public boolean updateArtist(Artist a){
		return artistDao.updateArtist(a);
	}
}
