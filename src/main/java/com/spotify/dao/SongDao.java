package com.spotify.dao;

import java.util.List;

import com.spotify.entities.Song;

public interface SongDao {
	public List<Song> getSongsByPlaylistId(int playlistId);
	public Song getSongById(int songId);
	public int createSong(Song newSong);
	public boolean deleteSong(int id);
	public List<Song> getSongByAlbumId(int aid);
	public List<Song> getSongByArtistId(int aid);
	public List<Song> getPopSongs();
	public List<Song> getSongByArtistIdNoLimit(int aid);
	public boolean updateSong(Song s);
}
