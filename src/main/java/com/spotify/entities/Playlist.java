package com.spotify.entities;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "playlists")
public class Playlist extends SongCollection{
	@Id
	@Column(name="PLAYLIST_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int playlistId;	
	@Column(name="dateAdded")
	private Date dateAdded;
	@Column(name="Description")
	private String description;
	@Column(name="private")
	private boolean playlistPrivate;
	@Transient
	private List<User> followers;
	@Transient
	private List<Genre> listOfGenre;
	@Transient
	private int followCount;
	
	public Playlist() {
		super();
		this.dateAdded = null;
		this.description = "";
		this.followers = new ArrayList<User>();
		this.listOfGenre = new ArrayList<Genre>();
		this.playlistPrivate = false;
	}
	
	public Playlist(Date createdDate, boolean playlistMode, String playlistTitle, String imageLoc) {
		super(playlistTitle,imageLoc);
		this.dateAdded = createdDate;
		this.playlistPrivate = playlistMode;
		this.followers = null;
		this.listOfGenre = null;
	}

	public int getPlaylistId() { 
		return playlistId; 	
	}
	
	public void setPlaylistId(int playlistId) { 
		this.playlistId = playlistId; 
	}

	public Date getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<User> getFollowers() {
		return followers;
	}
	
	public void setFollowers(List<User> followers) {
		this.followers = followers;
	}
	
	public boolean isPlaylistPrivate() {
		return playlistPrivate;
	}
	
	public void setPlaylistPrivate(boolean playlistPrivate) {
		this.playlistPrivate = playlistPrivate;
	}
	
	public List<Genre> getListOfGenre() {
		return listOfGenre;
	}
	
	public void setListOfGenre(List<Genre> listOfGenre) {
		this.listOfGenre = listOfGenre;
	}
	
	/*Used to add a user to the follow table*/
	public void addFollowUser(User u){
		this.followers.add(u);
	}
	
	/*used to remove user to the follow table*/
	public void removeFollowUser(User u) {
		this.followers.remove(u);
	}
	
	/*add type of genre to user list*/
	public void addGenreType(Genre type) {
		this.listOfGenre.add(type);
	}
	
	/*remove type of genre from user list*/
	public void removeGenreType(Genre type) {
		this.listOfGenre.remove(type);
	}
	
	/*see if the person is an owner of this play list*/
	public boolean isOwner(User u) {
		return this.getOwners().contains(u);
	}
		
	public int getFollowCount() {
		return followCount;
	}

	public void setFollowCount(int followCount) {
		this.followCount = followCount;
	}

	public void printPlaylist(){
		System.out.printf("Playlist: %d %s %s %s %s\n", this.playlistId, this.getTitle(), this.dateAdded.toString(), this.description, this.getImageLoc());
	}
}
