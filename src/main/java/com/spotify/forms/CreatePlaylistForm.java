package com.spotify.forms;

import org.springframework.web.multipart.MultipartFile;

public class CreatePlaylistForm {

    private MultipartFile playlistImage;
    private String playlistName;
    private String playlistDescription;
    
    public MultipartFile getPlaylistImage() {
        return playlistImage;
    }

    public void setPlaylistImage(MultipartFile playlistImage) {
        this.playlistImage = playlistImage;
    }

    public String getPlaylistName() {
        return playlistName;
    }

    public void setPlaylistName(String playlistName) {
        this.playlistName = playlistName;
    }

    public String getPlaylistDescription() {
        return playlistDescription;
    }

    public void setPlaylistDescription(String playlistDescription) {
        this.playlistDescription = playlistDescription;
    }
}
