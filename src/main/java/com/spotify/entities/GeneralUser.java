package com.spotify.entities;

import java.sql.Date;
import java.util.List;

public class GeneralUser{
	
	private User u;
	private List<Artist> artistFollowing;
	private List<User> following;
	private List<User> followers;
	private List<Playlist> playlistFollowing;
	private List<Playlist> publicPlaylist;
	
	public GeneralUser() {
		super();
	}
	
	public GeneralUser(User u, List<Artist> la, List<User> following, List<User> follower, List<Playlist> fp, List<Playlist> publicPlaylist) {
		this.u = u;
		this.artistFollowing = la;
		this.following = following;
		this.followers = follower;
		this.playlistFollowing = fp;
		this.publicPlaylist = publicPlaylist;
	}
	
	public List<Artist> getArtistFollowing() {
		return artistFollowing;
	}
	
	public void setArtistFollowing(List<Artist> artistFollowing) {
		this.artistFollowing = artistFollowing;
	}
	
	public List<User> getFollowing() {
		return following;
	}
	
	public void setFollowing(List<User> following) {
		this.following = following;
	}
	
	public List<User> getFollowers() {
		return followers;
	}
	
	public void setFollowers(List<User> followers) {
		this.followers = followers;
	}
	
	public List<Playlist> getPlaylistFollowing() {
		return playlistFollowing;
	}
	
	public void setPlaylistFollowing(List<Playlist> playlistFollowing) {
		this.playlistFollowing = playlistFollowing;
	}
	
	/*add to following artist*/
	public void addFollowArtist(Artist a) {
		this.artistFollowing.add(a);
	}
	
	/*remove from following artist*/
	public void removeFollowArtist(Artist a) {
		this.artistFollowing.remove(a);
	}
	
	/*add to following user*/
	public void addFollowingUser(User u) {
		this.following.add(u);
	}
	
	/*remove from following user*/
	public void removeFollowingUser(User u) {
		this.following.remove(u);
	}
	
	/*add to follower user*/
	public void addFollowerUser(User u) {
		this.followers.add(u);
	}
	
	/*remove from follower user*/
	public void removeFollowerUser(User u) {
		this.followers.remove(u);
	}
	
	/*add to playlist follow list*/
	public void addFollowingPlaylist(Playlist p) {
		this.playlistFollowing.add(p);
	}
	
	/*remove from playlist follow list*/
	public void removeFollowingPlaylist(Playlist p) {
		this.playlistFollowing.remove(p);
	}

	public List<Playlist> getPublicPlaylist() {
		return publicPlaylist;
	}

	public void setPublicPlaylist(List<Playlist> publicPlaylist) {
		this.publicPlaylist = publicPlaylist;
	}

	public User getU() {
		return u;
	}

	public void setU(User u) {
		this.u = u;
	}
}
