package com.spotify.daoImpl;

import java.util.List;

import com.spotify.entities.*;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spotify.dao.AbstractDao;
import com.spotify.dao.ArtistDao;
import com.spotify.entityManagers.SongManager;

@Repository("artistDao")
@Transactional
public class ArtistDaoImpl extends AbstractDao implements ArtistDao{
	private SongManager sm = SongManager.getSongManager();
	
	@Override
	public Artist getArtistById(int id) {
        Artist a = (Artist) getSession().get(Artist.class, id);
        return a;
	}

	@Override
	public boolean updateArtist() {
		return false;
	}
	
	public List<Artist> getArtistBySong(int sid) {
		String sql = "from Artist a where a.artistID in (select aos.a.artistID from ArtistOfSong aos where aos.s.id = :id)";
		Query q = getSession().createQuery(sql)
				.setParameter("id", sid);
		List<Artist> result = q.list();
		return result;
	}
	
	@Override
	public List<Album> getArtistAlbumList(int aid) {
		String sql = "from Album a where a.a.artistID = :id)";
		Query q = getSession().createQuery(sql)
				.setParameter("id", aid);
		List<Album> result = q.list();
		return result;
	}

	@Override
	public boolean createArtist(Artist newArtist) {
		try {
			getSession().saveOrUpdate(newArtist);
			return true;
		}
		catch(HibernateException e) {
			return false;
		}
	}

	@Override
	public boolean removeArtist(int id) {
		String sql = "Delete from Artist where artistID = :id";
		Query q = getSession().createQuery(sql)
				.setParameter("id", id);
		if(q.executeUpdate()>0) {
			return true;
		}
		return false;
	}

	@Override
	public Artist getArtistByName(String name) {
		String sql = "from Artist a where a.name = :name)";
		Query q = getSession().createQuery(sql)
				.setParameter("name", name);
		List<Artist> result = q.list();
		return result.isEmpty() ? null : result.get(0);
	}

	@Override
	public List<Concert> getArtistConcertList(int id) {
		String sql = "from Concert c where c.id in (select cfa.c.id from ConcertForArtist cfa where cfa.a.artistID = :id)";
		Query q = getSession().createQuery(sql)
				.setParameter("id", id);
		List<Concert> result = q.list();
		return result;
	}
	
	public boolean updateArtist(Artist a)
	{
		try{
			getSession().saveOrUpdate(a);
			return true; 
		}
		catch(Exception e){
			return false;
		}
	}

}
