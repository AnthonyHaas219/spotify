package com.spotify.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "albums")
public class Album extends SongCollection{
	@Id
	@Column(name="ALBUM_ID")
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int albumId;
	@OneToOne
	@JoinColumn(name = "ARTIST_ID", nullable = false, referencedColumnName = "ARTIST_ID")
	private Artist a;
	@Column(name="dateAdded")
	private Date dateAdded;
	
	public int getAlbumId() {
		return albumId;
	}

	public void setAlbumId(int albumId) {
		this.albumId = albumId;
	}

	public Date getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

	@JsonManagedReference
	public Artist getArtist() {
		return a;
	}

	public void setArtist(Artist a) {
		this.a = a;
	}

}
