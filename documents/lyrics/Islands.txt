

[Verse 1: Shakira]
I left a note on my bedpost
Said, "Not to repeat yesterday’s mistakes"
What I tend to do when it comes to you
I see only the good, selective memory
The way you make me feel, yeah, you gotta hold on me
I've never met someone so different
Oh, here we go
You a part of me now
You a part of me
So where you go I follow, follow, follow

[Chorus: Shakira]
Ohh Ohh I can't remember to forget you
Ohh Ohh I keep forgetting I should let you go
But when you look at me
The only memory is us kissing in the moonlight
Ohh Ohh I can't remember to forget you
Ohh Ohh I can't remember to forget you

[Verse 2: Rihanna]
I go back again
Fall off the train
Land in his bed
Repeat yesterday’s mistakes
What I'm trying to say is not to forget
You see only the good, selective memory
The way he makes me feel like
The way he makes me feel
I never seemed to act so stupid
Oh, here we go
He a part of me now
He a part of me
So where he goes I follow, follow, follow

[Chorus: Shakira & Rihanna]
Ohh Ohh I can't remember to forget you
Ohh Ohh I keep forgetting I should let you go
But when you look at me
The only memory is us kissing in the moonlight
Ohh Ohh I can't remember to forget you

[Bridge: Shakira]
I rob and I kill to keep him with me
I do anything for that boy
I'd give my last dime to hold him tonight
I do anything for that boy

[Bridge: Rihanna]
I rob and I kill to keep him with me
I do anything for that boy
I'd give my last dime to hold him tonight
I do anything for that boy

[Chorus: Shakira & Rihanna]
Ohh Ohh I can't remember to forget you
Ohh Ohh I keep forgetting I should let you go
But when you look at me
The only memory is us kissing in the moonlight

[Outro: Shakira & Rihanna]
Ohh Ohh I can't remember to forget you
But when you look at me
The only memory
Is us kissing in the moonlight
Ohh Ohh I can't remember to forget you

