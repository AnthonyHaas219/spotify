

[Verse 1]
We should take this back to my place
That's what she said right to my face
'Cause I want you bad
Yeah, I want you, baby
I've been thinking 'bout it all day
And I hope you feel the same way, yeah
'Cause I want you bad
Yeah, I want you, baby

[Chorus]
Slow, slow hands
Like sweat dripping down our dirty laundry
No, no chance
That I'm leaving here without you on me
I, I know
Yeah, I already know that there ain't no stopping
Your plans and those
Slow hands (woo)
Slow hands

[Verse 2]
I just wanna take my time
We could do this, baby, all night, yeah
'Cause I want you bad
Yeah, I want you, baby

[Chorus]
Slow, slow hands
Like sweat dripping down our dirty laundry
No, no chance
That I'm leaving here without you on me
I, I know
Yeah, I already know that there ain't no stopping
Your plans and those
Slow hands (woo)

[Bridge]
Fingertips puttin' on a show
Got me now and I can't say no
Wanna be with you all alone
Take me home, take me home
Fingertips puttin' on a show
Can't you tell that I want you, baby, yeah

[Chorus]
Slow hands
Like sweat dripping down our dirty laundry
No, no chance
That I'm leaving here without you on me
I, I know
Yeah, I already know that there ain't no stopping
Slow hands
Like sweat dripping down our dirty laundry
No, no chance
That I'm leaving here without you on me
I, I know
Yeah, I already know that there ain't no stopping
Your plans and those slow hands (woo)
Yeah, those slow hands
Woo, slow hands

