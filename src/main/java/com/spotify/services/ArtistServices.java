package com.spotify.services;

import java.util.List;

import com.spotify.entities.*;

public interface ArtistServices {
	public Artist getArtistById(int id);
	public List<Album> getArtistAlbumList(int aid);
	public List<Artist> getArtistBySong(int sid);
	public Artist getArtistByName(String name);
	public List<Concert> getArtistConcertList(int id);
	public boolean createArtist(Artist a);
	public boolean updateArtist(Artist a);
}
