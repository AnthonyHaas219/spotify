angular.module("mySpotify").controller("artistsCtrl", function($scope, $rootScope, $http, $routeParams) {
    $scope.loadInfo = function() {
        $http.get("api/getArtists/").success(function (response) {
            $scope.artistList = response;
            for(var i = 0; i < $scope.artistList.length; i++) {
                if ($scope.artistList[i].albums.length == 0) {
                    $scope.artistList[i].albums = [{imageLoc: "images/question.jpg"}];
                    $scope.artistList[i].noImage = true;
                }
            }
   		 });
	}
});