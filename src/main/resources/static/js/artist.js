angular.module("mySpotify").controller("artistCtrl", function($scope, $rootScope, $http, $routeParams) 
{

    var id = parseInt($routeParams.id);

    $scope.loadInfo = function() {
        $http.get("api/artist/" + id).success(function (response) {
            $scope.currentUser = false;
            $scope.artistUser = response.user;
            $scope.artistName = response.name;
            $scope.listenCount = response.listenCount;
            $scope.rank = response.rank;
            $scope.followCount = response.followCount;
            $scope.royalties = response.royalties;
            $scope.bio = response.bio;
            $scope.concerts = response.concerts;
            $scope.albums = response.albums;
            $scope.popSong = response.popularSong;
            $scope.isFollowing = false;
             $scope.songs = response.popularSong;
             $scope.songOrder = JSON.parse(JSON.stringify($scope.songs));
             $scope.$parent.songs = response.popularSong;
             $scope.$parent.songOrder = JSON.parse(JSON.stringify($scope.songs));


            console.log($scope.artistName)

            if ($scope.albums.length == 0) {
                $scope.albums = [{imageLoc: "images/question.jpg"}];
            }

            if (id === $scope.$parent.userID){
                $scope.currentUser = true;
            }
            var i = 0;
            for(i=0; i<$scope.$parent.followingArtist.length; i++){
                if(id == $scope.$parent.followingArtist[i].artistID){
                    $scope.isFollowing = true;
                    break;
                }
            }
            console.log($scope)
            console.log(response.popularSong);

              i=0;
             for(i=0; i<$scope.$parent.songs.length; i++){
                 $scope.$parent.songs[i].length = $scope.$parent.convertTime(response.popularSong[i].length);
             }
        });
    }

    $scope.setFollowArtist = function(){
        var artist = {
            artistID : id,
            name: $scope.artistName
        }
        if($scope.isFollowing === true){
            $http({
                url: '/api/unfollowArtist',
                method: "POST",
                data: { "unfollowId" : id}
            })
            .then(function(response) {
                var i =0;
                for(i= 0; i<$scope.$parent.followingArtist.length; i++){
                    if(id == $scope.$parent.followingArtist[i].artistID){
                        $scope.$parent.followingArtist.splice(i, 1);
                        break;
                    }
                }
                $scope.loadInfo();
            });
        }else{
            $http({
                url: '/api/followArtist',
                method: "POST",
                data: { "followId" : id}
            })
            .then(function(response) {
                $scope.$parent.followingArtist.push(artist)
                $scope.loadInfo();
            });
        }
    }
});