package com.spotify.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "artists")
public class Artist{
	@Id
	@Column(name="ARTIST_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int artistID;
	@Column(name="groupName")
	private String name;
	@OneToOne
	@JoinColumn(name = "USER_ID", nullable = false, referencedColumnName = "USER_ID")
	private User user;
	@Column(name="listenCount")
	private int listenCount;
	@Column(name="artistRank")
	private int rank;
	@Column(name="followCount")
	private int followCount;
	@Column(name="royalty")
	private double royalties;
	@Column(name="bio")
	private String bio;
	//List based on class diagram
	@Transient
	private List<Concert> concerts;
	@Transient
	private List<Album> albums;
	@Transient
	private List<Song> popularSong;

	public Artist() {
	}

	public int getArtistID() { 
		return artistID; 
	}
	
	public void setArtistID(int artistID){ 
		this.artistID = artistID; 
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getListenCount() {
		return listenCount;
	}
	
	public void setListenCount(int listenCount) {
		this.listenCount = listenCount;
	}
	
	public int getRank() {
		return rank;
	}
	
	public void setRank(int rank) {
		this.rank = rank;
	}
	
	public int getFollowCount() {
		return followCount;
	}
	
	public void setFollowCount(int followCount) {
		this.followCount = followCount;
	}
	
	public double getRoyalties() {
		return royalties;
	}
	
	public void setRoyalties(double royalties) {
		this.royalties = royalties;
	}
	
	public String getBio() {
		return bio;
	}
	
	public void setBio(String bio) {
		this.bio = bio;
	}

	@JsonBackReference
	public User getUser() {
		return user;
	}

	public void setUser(User u) {
		this.user = u;
	}

	public List<Concert> getConcerts() {
		return concerts;
	}
	
	public void setConcerts(List<Concert> concerts) {
		this.concerts = concerts;
	}
	
	public List<Album> getAlbums() {
		return albums;
	}
	
	public void setAlbums(List<Album> albums) {
		this.albums = albums;
	}
	
	public List<Song> getPopularSong() {
		return popularSong;
	}

	public void setPopularSong(List<Song> popularSong) {
		this.popularSong = popularSong;
	}

	/*remove a follower from counter*/
	public void removeFollower() {
		this.followCount--;
	}
	
	/*add follower to counter*/
	public void addFollower() {
		this.followCount++;
	}
	
	/*add to royalties*/
	public void addRoyalty(double add) {
		this.royalties += add;
	}
	
	/*add to listening count*/
	public void removeListenCount() {
		this.listenCount--;
	}
	
	/*add to listening count*/
	public void addListenCount() {
		this.listenCount++;
	}
	
	/*remove concert info for artist*/
	public void removeConcert(Concert concertToRemove) {
		this.concerts.remove(concertToRemove);
	}
	
	/*add to concert list*/
	public void addConcert(Concert addConcert) {
		this.concerts.add(addConcert);
	}
	
	/*remove album info for artist*/
	public void removeAlbum(Album removeAlbum) {
		this.albums.remove(removeAlbum);
	}
	
	/*add to album list*/
	public void addAlbum(Album addAlbum) {
		this.albums.add(addAlbum);
	}
}
