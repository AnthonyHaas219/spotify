$( function () 
{
    $("#overView").on("click", function () 
    {
        $("#userContent").load("userPageOverview.html");
    });

    $("#recent").on("click", function () 
    {
        $("#userContent").load("userPageRecentPlayed.html");
    });

    $("#publicPlaylist").on("click", function () 
    {
        $("#userContent").load("userPagePublicPlaylist.html");
    });

    $("#following").on("click", function () 
    {
        $("#userContent").load("userPageFollowing.html");
    });
}
);