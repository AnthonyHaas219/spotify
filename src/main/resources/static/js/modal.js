angular.module("mySpotify").controller('modalCtrl',function($scope, $modalInstance, $rootScope, data){
    $scope.user = {playlistName : '',
        imageFile: '',
        description: ''};
    $scope.card = {cardName: '',
    creditCardNumber: '',
    expMonth: '',
    expYear: '',
    sec: '',
    billAddr1: '',
    billAddr2: '',
    billZip: ''};
    $scope.lyrics = data.lyrics
    $scope.playlists = data.selectedPlaylists
    $scope.albums = data.response;
    console.log($scope)
    console.log(data)
    console.log($scope.playlists)
    $scope.profile = {
        imageFile: ''
    };

    $scope.song ={
        file :'',
        albumId : '',
        songName: '',
        lyrics: '',
        length : ''
    }

    $scope.addProfileImage = function(){
            console.log($scope.profile);
          $modalInstance.close($scope.profile);
    }

    $scope.playlist = {playlistId : '',
        songId: ''};
    $scope.cancel = function(){
        console.log($scope)
        console.log($modalInstance)
        $modalInstance.dismiss('canceled');
    };

    $scope.createPlaylist = function (){

        console.log($scope.user);
        console.log($modalInstance)
        $modalInstance.close($scope.user);
    }

   $scope.upgrade = function(){
        console.log($scope.card.date)
        $scope.card.expMonth = $scope.card.date.getMonth();
        $scope.card.expYear = $scope.card.date.getFullYear();
        delete $scope.card.date
        console.log($scope.card);
        console.log($modalInstance)
        $modalInstance.close($scope.card);
   }

   $scope.addToPlaylist = function (playlistId){
        console.log(playlistId);
        console.log($modalInstance)
        $modalInstance.close(playlistId);
    }

    $scope.selectAlbum = function(id){
        $scope.song.albumId = id;
        $scope.selectedAlbum = id;
    }
    $scope.addSong = function(){
            $modalInstance.close($scope.song);
    }
})

angular.module("mySpotify").directive('imageInput', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attributes) {
            element.bind('change', function () {
                $parse(attributes.imageInput)
                .assign(scope,element[0].files)
                scope.$apply()
            });
        }
    };
}]);


angular.module("mySpotify").run(['$templateCache',function($templateCache){
  $templateCache.put('createPlaylistModal.html',
    `<div class="modal">
    <div class="modal-dialog"> 
    <div class="modal-content"> 
    <div class="modal-header"> 
    <button class="close" data-dismiss="modal" ng-click = "cancel()">&times;</button>
    <p class="modal-title" >Create New Playlist</p> 
    </div> 
    <div class="modal-body"> 
    <form class="upgradeForm" > 
    <div class="form-group"> 
    <label>Image</label> 
    <input type="file" image-input="user.imageFile"> 
    </div> <div class="form-group"> 
    <label for="Name">Name</label> 
    <input class="form-control" placeholder="Name" type="text" ng-model="user.playlistName"> 
    </div> 
    <div class="form-group"> 
    <label for="Name">Description</label> 
    <textarea class="form-control" placeholder="Description" type="textarea" ng-model="user.description" rows="5"></textarea>
    </div> 
    </form> 
    </div> 
    <div class="modal-footer"> 
    <button  ng-click = "createPlaylist()" class="btn btn-primary">Create</button> 
    <button class="btn btn-primary" ng-click="cancel()" >Cancel</button> 
    </div> 
    </div> 
    </div>`);

   $templateCache.put('upgradeToPremium.html',
   `<div class="modal">
   <div class="modal-dialog">
   <div class="modal-content">
   <div class="modal-header">
   <button class="close" data-dismiss="modal" ng-click = "cancel()">&times;
   </button>
   <p class="modal-title" >Upgrade To Premium</p>
   </div>
   <div class="modal-body">
   <form class="upgradeForm" name="upgradeForm" >
   <div class="form-group">
   <div class="premiumInfo">
   <h1 style = "margin-top: -8px;">$9.99</h1>
   <ul>
   </ul>
   </div>
   <div class="form-group">
   <label for="Name">Name</label> <input class="form-control" placeholder="Name" type="text" id="cardName" ng-model="card.cardName" required>
   </div>
   <div class="form-group">
   <label for="Name">Address Line 1</label>
   <input class="form-control" placeholder="Address" type="text" id="cardNum" ng-model="card.billAddr1" required>
   </div>
   <div class="form-group">
   <label for="Name">Address Line 2</label>
   <input class="form-control" placeholder="Address" type="text"  id="cardNum" ng-model="card.billAddr2">
   </div>
   <div class="form-group">
   <label for="Name">Zip Code</label>
   <input class="form-control" placeholder="Zip Code" type="number" ng-pattern="/^[0-9]+$/" ng-minlength="5" ng-maxlength = "9" id="cardNum" ng-model="card.billZip" required>
   </div>
   <div class="form-group">
   <label for="Name">State</label>
   <input class="form-control" placeholder="State" type="text"  id="cardNum" ng-minlength = "2" ng-maxlength = "2" ng-model="card.billState" required>
   </div>
   <div class="form-group">
   <label for="Name">City</label>
   <input class="form-control" placeholder="City" type="text" id="cardNum" ng-model="card.billCity;" required>
   </div>
   <div class="form-group">
   <label for="Name">Card Number</label>
   <input class="form-control" placeholder="Card Number" type="text" ng-pattern="/^[0-9]+$/" ng-minlength="16"  ng-maxlength="16" id="cardNum" ng-model="card.creditCardNumber" required>
   </div>
   <div class="form-group">
   <label for="Name">Expiration Date</label>
   <input class="form-control"  type="month" id="cardDate" ng-model = "card.date">
   </div>
   <div class="form-group">
   <label for="Name">Security Code</label>
   <input class="form-control" placeholder="Security Code" type="number" id="cardCode" ng-pattern="/^[0-9]+$/" ng-minlength="3" ng-maxlength="3" ng-model = "card.sec" required> </div>
   </form>
   </div>
   </div>
   <div class="modal-footer">
   <button class="btn btn-primary" ng-click = "upgrade()"  ng-disabled="upgradeForm.$invalid">Start Premium</button>
   <button class="btn btn-primary" ng-click = "cancel()">Cancel</button>
   </div>
   </div>
   </div>
   </div>`);


   $templateCache.put('lyricsModal.html',`<div class="modal" >
                                             <div class="modal-dialog">
                                             <div class="modal-content"  style = "background: black;">
                                             <div class="modal-header">
                                             <button class="close" data-dismiss="modal" ng-click = "cancel()" style = "color: white; margin-top: -15px; margin-right: -9px; font-size: 29px; ">&times;
                                             </button>
                                             <p>
                                             <pre style = "overflow-y: scroll; height: 600px; background: black; color: white; border: 0;">{{lyrics}}</pre>
                                             </p>
                                             </div>
                                             </div>
                                             </div>`
);
  $templateCache.put('addToPlaylist.html',
`<div class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button  class="close" data-dismiss="modal" ng-click = "cancel()">&times;</button>
                <p class="modal-title" >Add To Playlist</p>
            </div>
            <div class="modal-body" style = "overflow-y: scroll; height: 600px;">
                <form class="upgradeForm" >
                    <div class="form-group">
                        <div id="playlistSongs">
                            <table class="table table-hover table-responsive">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Cover</th>
                                </tr>
                                </thead>
                                <tr ng-repeat = "playlist in playlists" data-dismiss="modal" ng-click="addToPlaylist(playlist.playlistId)">
                                    <td> {{playlist.title}} </td>
                                    <td><img ng-model = "playlist.imageLoc" ng-src="images/{{playlist.imageLoc}}" onerror="this.src = 'images/chance.jpg'" style="float: left; margin-right: 2em; height: 300px; width:300px;"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" ng-click="cancel()" >Cancel</button>
            </div>
        </div>
    </div>
</div>`
);
  $templateCache.put('addProfileImageModal.html',
    `<div class="modal">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
    <button class="close" data-dismiss="modal" ng-click = "cancel()">&times;</button>
    <p class="modal-title" >Change Image</p>
    </div>
    <div class="modal-body">
    <form class="upgradeForm" >
    <div class="form-group">
    <label>Image</label>
    <input type="file" image-input="profile.imageFile">
    </div>
    </form>
    </div>
    <div class="modal-footer">
    <button  ng-click = "addProfileImage()" class="btn btn-primary">Create</button>
    <button class="btn btn-primary" ng-click="cancel()" >Cancel</button>
    </div>
    </div>
    </div>`);


      $templateCache.put('addSongToAlbumModal.html',
        `<div class="modal">
        <div class="modal-dialog">
        <div class="modal-content" style = "overflow-y: scroll; height: 600px;">
        <div class="modal-header">
        <button class="close" data-dismiss="modal" ng-click = "cancel()">&times;</button>
        <p class="modal-title" >Add Song</p>
        </div>
        <div class="modal-body">
        <form class="upgradeForm" >
        <div class="form-group">
        <label>Song</label>
        <input type="file" image-input="song.file">
        </div>
         <div class="form-group">
           <label for="Name">Name</label>
           <input class="form-control" placeholder="Name" type="text" id="cardNum" ng-model="song.songName" required>
           </div>
            <div class="form-group">
                      <label for="Name">Song Length</label>
                      <input class="form-control" placeholder="Length" type="number" id="cardNum" ng-model="song.length" required>
                      </div>
            <div class="form-group">
              <label for="Name">Lyrics</label>
              <textarea class="form-control" type="text" id="cardNum" ng-model="song.lyrics"></textarea>
              </div>
                <div id="playlistSongs">
                                    <table class="table table-hover table-responsive">
                                        <thead>
                                        <tr style = "background: black;">
                                            <th>Title</th>
                                            <th>Cover</th>
                                        </tr>
                                        </thead>
                                        <tr ng-repeat = "album in albums" ng-click="selectAlbum(album.albumId)" ng-class="{selected: album.albumId === selectedAlbum}">
                                            <td style = "background: grey;"> {{album.title}} </td>
                                            <td><img ng-model = "album.imageLoc" ng-src="images/{{album.imageLoc}}" onerror="this.src = 'images/chance.jpg'" style="float: left; margin-right: 2em; height: 300px; width:300px;"></td>
                                        </tr>
                                    </table>
                                </div>
        </form>
        </div>
        <div class="modal-footer">
        <button  ng-click = "addSong()" class="btn btn-primary">Create</button>
        <button class="btn btn-primary" ng-click="cancel()" >Cancel</button>
        </div>
        </div>
        </div>`);



}]);