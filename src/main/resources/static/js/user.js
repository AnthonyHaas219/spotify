angular.module("mySpotify").controller("userCtrl", function($scope, $rootScope, $http, $routeParams, $dialogs) {

    var id = parseInt($routeParams.id);
    $scope.currentUser = false;
    $scope.loadInfo = function() {
        $http.get("api/userPage/" + id).success(function (response) {
            $scope.currentUser = false;
            $scope.user = response.u;
            $scope.artistFollowing = response.artistFollowing;
            $scope.publicPlaylist = response.publicPlaylist;
            $scope.followingPlaylist = response.playlistFollowing;
            $scope.following = response.following;
            $scope.follower = response.followers;
            $scope.isFollowing = false;
            $scope.userImageLoc = response.u.userImageLoc;
            if (id === $scope.$parent.userID){
                $scope.currentUser = true;
            }
            for(i=0; i<$scope.follower.length; i++){
                if($scope.$parent.userID == $scope.follower[i].id){
                    $scope.isFollowing = true;
                    break;
                }
            }
        });
    }

    $scope.setUserFollow = function(user){
        if($scope.isFollowing === true){
            $http({
                url: '/api/unfollowUser',
                method: "POST",
                data: { "unfollowId" : user.id}
            })
            .then(function(response) {
                for(var i = 0; i < $scope.$parent.friends.length;i++){
                    if($scope.$parent.friends[i].id == user.id){
                        $scope.$parent.friends.splice(i, 1);
                    }
                }
                $scope.loadInfo()
            });
        }else{
            $http({
                url: '/api/followUser',
                method: "POST",
                data: { "followId" : user.id}
            })
            .then(function(response) {
                $scope.$parent.friends.push(user);
                $scope.loadInfo()
            });
        }
    }

    $scope.changePic = function(){
           var dialog = null;

            dialog = $dialogs.create('addProfileImageModal.html','modalCtrl',{},{key: false, back: 'static'});


            dialog.result.then(function(profile){
                console.log(profile)
                var fd = new FormData();
                fd.append("profileImage", profile.imageFile[0]);
                var request = {
                    method: 'POST',
                    url: '/api/addProfileImage',
                    data: fd,
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                };
                console.log("here")
                $http(request)
                    .then(function (d) {
                        console.log("here")
                        console.log("!!!")
//                        console.log(d);
                        console.log(d);
                       $scope.userImageLoc = d.data.userImageLoc;
                       console.log($scope.userImageLoc)
                    })

            });
    }

    $scope.removePic = function(){
        $http.post("/api/removeProfilePic").then(function (d) {
                    $scope.userImageLoc = "";
                })


    }
});