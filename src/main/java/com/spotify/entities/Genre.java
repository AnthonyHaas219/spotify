package com.spotify.entities;

public enum Genre {
    ROCK,
    POP,
    HIPHOP,
    CLASSICAL,
    METAL,
    HEAVYMETAL,
    ALTERNATIVE,
    COUNTRY,
    EDM,
    RB,
    LATIN,
    JAZZ,
    ROMANCE,
    KPOP,
    PUNK,
    BLUES;
}
