var app = angular.module("mySpotify", ["ngRoute","ui.bootstrap","dialogs"]);
app.config(function($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl : "homepage.html"
        })
        .when("/playlist/:id", {
            templateUrl : "playlist.html",
            controller: 'playlistCtrl'
        })
        .when("/song", {
            templateUrl : "song.html"
        })
        .when("/queue", {
            templateUrl : "queue.html"
        })
        .when("/albums", {
            templateUrl : "albums.html",
            controller: 'albumsCtrl'
        })
        .when("/artist/:id", {
            templateUrl : "artist.html",
            controller: 'artistCtrl'
        })
        .when("/artists", {
            templateUrl : "artists.html",
            controller: 'artistsCtrl'
        })
        .when("/userPage/:id", {
            templateUrl : "user.html",
            controller: 'userCtrl'
        })
        .when("/manageAccount", {
            templateUrl : "account.html"
        })
        .when("/artistAccount", {
            templateUrl : "artistAccount.html"
        })
        .when("/album/:id", {
            templateUrl : "album.html",
            controller: 'albumCtrl'
        })
        .when("/search/:searchWord", {
            templateUrl : "search.html",
            controller: "searchCtrl"
        })
        .when("/addAccount", {
            templateUrl : "addAccount.html",
            controller: "addAccountCtrl"
        })
        .when("/banUser", {
            templateUrl : "banUser.html"
        })
        .when("/friendSearch", {
            templateUrl : "friendSearch.html"
        })
        .when("/changePassword", {
            templateUrl : "changePassword.html",
            controller: "changeCtrl"
        })
        .when("/advertisements", {
            templateUrl : "advertisement.html",
            controller: "advertisementCtrl"
        })
        .when("/stats", {
            templateUrl: "stats.html",
            controller: "statsCtrl"
        })
        .when("/artistStats", {
            templateUrl: "artistStats.html",
            controller: "artistStatsCtrl"
        })
});

app.controller("browseCtrl", function($scope, $window, $http, $rootScope, $timeout, $dialogs, $interval, $sce, $location) {
   $scope.oldVolume = 1;
   $scope.volume = "100%";
   $scope.showAd = true;
   $scope.playToggle = true;
   $scope.repeatToggleMultiple = false;
   $scope.repeatToggleSingle = false;
   $scope.shuffleToggle = false;
   $scope.nextToggle = false;
   $scope.previousToggle = false;
   $scope.lyricsToggle = false;
   $scope.songOrder = '';
 function loadInfo() {
   $http.get("/api/user").success(function (response){
    $scope.firstName = response.firstName;
    $scope.lastName = response.lastName;
    $scope.playlists = response.playlists;
    console.log($scope.playlists);
    $scope.userID = response.userID;
    $scope.email = response.email;
    $scope.dob = response.dob;
    $scope.isAdmin = response.admin;
    $scope.library = response.library;
    $scope.queue = response.queue;
    $scope.isPremium = response.premium;
    $scope.followingArtist = response.followingArtist;
    $scope.lang = response.lang;
    $scope.lastPlayedSong = response.lastSong;
    $scope.friends = response.friends;
    $scope.isArtist = response.isArtist;
    if($scope.isArtist){
        $scope.artistInfo = response.artistInfo;
    }
    console.log("Last Song: " + $scope.lastPlayedSong);
    console.log($scope.friends);
    console.log(response.premium)
    console.log(response.dob)

        if($scope.isPremium === false){
              var ads = '';
                     $http.get("api/showAdvertisements").success(function (response) {
                                console.log(response);
                                ads = response;
                     }).then(function(response) {
                  console.log(ads);
                                  console.log(ads.length);

                                if(ads.length>0){ var num = Math.floor(Math.random() * ads.length);

                                 $scope.displayedAd = ads[num].imageLoc;
                                 console.log($scope.displayedAd);
                                 }
                                 else{
                                     $scope.displayedAd = "";
                                 }
                                    });


        }

    for(i=0;i<$scope.library.songs.length;i++){
        for(j=0;j<$scope.queue.songs.length;j++)
        {
            if($scope.library.songs[i].id == $scope.queue.songs[j].id){
                $scope.queue.songs[j].inLibrary = true;
                break;
            }
        }
        continue;
    }
    });

 }

 loadInfo();

$scope.setQueue = function(){
            console.log("SET QUEUE");


          $scope.songs = $scope.queue.songs;
          $scope.songOrder = JSON.parse(JSON.stringify($scope.songs));
           for(i=0; i<$scope.songs.length; i++){
              $scope.songs[i].length = $scope.convertTime($scope.songs[i].length);
           }

}

$scope.playQueue = function(){


}

$scope.playLibrary = function(){


}
$scope.setLibrary = function(){
          console.log("SET LIBRARY");
          $scope.songs = $scope.library.songs;
          $scope.songOrder = JSON.parse(JSON.stringify($scope.songs));
           for(i=0; i<$scope.songs.length; i++){
                          $scope.songs[i].length = $scope.convertTime($scope.songs[i].length);
                      }

}
 $scope.open = function(){
    var dialog = null;

    dialog = $dialogs.create('createPlaylistModal.html','modalCtrl',{},{key: false, back: 'static'});


    dialog.result.then(function(user){

        var fd = new FormData();
        fd.append("playlistImage", user.imageFile[0]);
        fd.append("playlistName", user.playlistName);
        fd.append("playlistDescription", user.description);
        var request = {
            method: 'POST',
            url: '/api/newPlaylist/',
            data: fd,
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined
            }
        };

        $http(request)
            .success(function (d) {
                $scope.playlists.push(d);
                console.log($scope.playlists)
                console.log(d)
            })
            .error(function () {
                 
            });
    });
    }

    $scope.deleteAccount = function(){
        $http.get("api/deleteAccount").success(function (response) {
            console.log("Account Deleted")
            $window.location.href = '/loginpage.html';
        }).error(function(response){
        });
    }

    $scope.changeAccount = function(){
        var data = {
            firstName: angular.element('#formFirstName').val(),
            lastName: angular.element('#formLastName').val(),
            dob: angular.element('#formDob').val()
        }
        $http.post("/api/updateUser", data).success(function (response) {
            $scope.firstName = response.firstName;
            $scope.lastName = response.lastName;
            $scope.email = response.email;
            $scope.dob = response.dob;
            $window.location.href = '#manageAccount';
        }).error(function(response){
        });
    }
    
    $scope.changeAccountArtist = function(){
        var data = {
            firstName: angular.element('#formFirstName').val(),
            lastName: angular.element('#formLastName').val(),
            dob: angular.element('#formDob').val(),
            artistName: angular.element('#artistName').val(),
            bio: angular.element('#artistBio').val()
        }
        $http.post("/api/updateArtist", data).success(function (response) {
            $scope.firstName = response.user.firstName;
            $scope.lastName = response.user.lastName;
            $scope.email = response.user.email;
            $scope.dob = response.user.dob;
            $scope.artistInfo.bio = response.bio;
            $scope.artistInfo.name = response.name;
            $window.location.href = '#artistAccount';
        }).error(function(response){
        });
    }

    $scope.upgradeToPremium = function(){
        var dialog = null;

        dialog = $dialogs.create('upgradeToPremium.html','modalCtrl',{},{key: false, back: 'static'});


        dialog.result.then(function(card){
            $http.post("/api/upgradeToPremium", card) .then(function(response) {
                $scope.isPremium = true;
            });
            });
    }
    $scope.downgradePremium = function(){
        $http.post("/api/cancelPremium") .then(function(response) {
                        $scope.isPremium = false;
                    });

    }
    $scope.play =  function(){
        $scope.playToggle = true;
        document.getElementById('mySong').play();
    }

    $scope.pause =  function(){
        $scope.playToggle = false;
        document.getElementById('mySong').pause();
    }

    $scope.convertTime =function(time){
        var minutes = Math.floor(time /60);
        var seconds = time - (minutes * 60);
        seconds = Math.ceil(seconds);
        if(seconds < 10){
            seconds = "0" + seconds;
        }
       return (minutes + ":" + seconds);
    }

     function calculatePercentage(){
        var mySong = document.getElementById('mySong');
        $scope.progress = mySong.currentTime / mySong.duration * 100 + '%';
        $scope.currentTime = $scope.convertTime(mySong.currentTime);
        $scope.duration = $scope.convertTime(mySong.duration);
        if($scope.duration === $scope.currentTime){
            console.log("testdhshjshjs")
            songEnded();
        }
     }
    $interval(calculatePercentage, 200);

    $scope.scrubSong = function($event){
        var mySong = document.getElementById('mySong');
        var e = $event;
        mySong.currentTime = $event.offsetX/ $event.delegateTarget.clientWidth * mySong.duration;
    }

    $scope.changeVolume = function($event){
        $scope.volume = $event.offsetX/ $event.delegateTarget.clientWidth * 100 + '%';
         $scope.oldVolume = $event.offsetX/ $event.delegateTarget.clientWidth;
        document.getElementById('mySong').volume = $scope.oldVolume;
        if($scope.oldVolume === 0){
            $scope.muteToggle = true;

        }else{
            $scope.muteToggle = false;
        }

    }

    $scope.mute = function(){
        $scope.volume = "0%";
        document.getElementById('mySong').volume = 0;
        $scope.muteToggle = true;
    }
    $scope.unmute = function(){
        $scope.volume = $scope.oldVolume * 100 + "%";
        document.getElementById('mySong').volume = $scope.oldVolume;
        $scope.muteToggle = false;
    }

    $scope.closeAd = function(){
        $scope.showAd = false;
    }

    $scope.openAd = function(){
        $scope.showAd = true;
    }

    $scope.loadSong = function(song){
    $scope.song = song;
    console.log(song);
         var index = -1;
         console.log($scope.songOrder);
             for(a =0; a< $scope.songOrder.length; a++){
                 if ($scope.songOrder[a].id === $scope.song.id){
                     index = a;
                 }

             }

            $http({
                      url: '/api/lastPlayedSong',
                      method: "POST",
                      data: { "song" : song.id}
                  })
                  .then(function(response) {
                    console.log("last song changed!");
                    $scope.lastPlayedSong = song;
                });


        $scope.songLoaded = true;
        $scope.playToggle = true;
        $scope.songLocation = "songs/" + song.songLocation;
        $scope.artists = song.artists;
        $scope.songName = song.title;
         if(index <= ($scope.songOrder.length -2) || $scope.repeatToggleMultiple == true){
                       $scope.nextToggle = true;
         }
         else{
            $scope.nextToggle = false;
         }
         if(index === 0){
            $scope.previousToggle = false;
         }
         else{
            $scope.previousToggle = true;
         }
         document.getElementById('mySong').currentTime  = 0;
         console.log($scope.song.lyrics)
         if($scope.song.lyrics != null){
            $scope.lyricsToggle = true;
         }
         else{
            $scope.lyricsToggle = false;
         }

         if($location.path().indexOf("queue")>-1){

                var index = -1;
                      console.log($scope.songs);
                          for(a =0; a< $scope.songs.length; a++){
                              if ($scope.songs[a].id === $scope.song.id){
                                  index = a;
                              }

                          }
               console.log(index);
               if(index>-1){
                  $http({
                             url: '/api/removeSongFromPlaylist',
                             method: "POST",
                             data: { "song" : $scope.song.id}
                     })
             $scope.songs.splice(index, 1);
             }
         }

        if($scope.repeatToggleSingle === true){
                     $scope.previousToggle = true;
                     $scope.nextToggle = true;
             }


    }

    function songEnded(){
               if($scope.repeatToggleSingle === true){
                           $scope.loadSong($scope.song);
                           return;
               }
            var index = -1;
         for(a =0; a< $scope.songOrder.length; a++){
             if ($scope.songOrder[a].id === $scope.song.id){
                 index = a;
             }

         }
        console.log(index)
        if ((index === $scope.songOrder.length -1) && $scope.repeatToggleMultiple === true){
            $scope.song = $scope.songOrder[0]
            $scope.loadSong($scope.song)

        }
         else if (index <= ($scope.songOrder.length -2)){
                 $scope.song = $scope.songOrder[index + 1]
                 $scope.loadSong($scope.song)
                 }


    }
     function shuffle(songs) {
            for (var a = songs.length - 1; a > 0; a--) {
                var b = Math.floor(Math.random() * (a + 1));
                var tempSong = songs[a];
                songs[a] = songs[b];
                songs[b] = tempSong;
            }
            return songs;
        }
    $scope.setShuffleToggle = function(){
        $scope.shuffleToggle = !$scope.shuffleToggle
        if($scope.shuffleToggle === true){
            $scope.songOrder = JSON.parse(JSON.stringify($scope.songs));
            $scope.songOrder = shuffle($scope.songOrder)
        }
        else{
            $scope.songOrder = JSON.parse(JSON.stringify($scope.songs));
        }
             var index = -1;
                                               for(a =0; a< $scope.songOrder.length; a++){
                                                   if ($scope.songOrder[a].id === $scope.song.id){
                                                       index = a;
                                                   }

                                               }
        if(index <= ($scope.songOrder.length -2) || $scope.repeatToggleMultiple === true){
                               $scope.nextToggle = true;
                 }
                 else{
                    $scope.nextToggle = false;
                 }
                 if(index === 0){
                    $scope.previousToggle = false;
                 }
                 else{
                    $scope.previousToggle = true;
                 }
           if($scope.repeatToggleSingle === true){
                        $scope.previousToggle = true;
                        $scope.nextToggle = true;
                }
    }

    $scope.setRepeatToggle = function(){
            console.log("here");

            if($scope.repeatToggleSingle == false){
                $scope.repeatToggleSingle = true;
            }else{
                $scope.repeatToggleMultiple = true;
                $scope.repeatToggleSingle = false;
                }

                 var index = -1;
               for(a =0; a< $scope.songOrder.length; a++){
                   if ($scope.songOrder[a].id === $scope.song.id){
                       index = a;
                   }

               }

   if(index <= ($scope.songOrder.length -2) || $scope.repeatToggleMultiple === true){
                          $scope.nextToggle = true;
            }
            else{
               $scope.nextToggle = false;
            }
            if(index === 0){
               $scope.previousToggle = false;
            }
            else{
               $scope.previousToggle = true;
            }
        if($scope.repeatToggleSingle === true){
                $scope.previousToggle = true;
                $scope.nextToggle = true;
        }
    }

    $scope.turnOffRepeat = function(){
            $scope.repeatToggleMultiple = false;
            $scope.repeatToggleSingle = false;

               var index = -1;
                           for(a =0; a< $scope.songOrder.length; a++){
                               if ($scope.songOrder[a].id === $scope.song.id){
                                   index = a;
                               }

                           }

               if(index <= ($scope.songOrder.length -2) || $scope.repeatToggleMultiple == true){
                                      $scope.nextToggle = true;
                        }
                        else{
                           $scope.nextToggle = false;
                        }
                        if(index === 0){
                           $scope.previousToggle = false;
                        }
                        else{
                           $scope.previousToggle = true;
                        }

    }

    $scope.nextSong = function(){
               if($scope.repeatToggleSingle === true){
                                       $scope.loadSong($scope.song);
                                       return;
                           }
            var index = -1;
            for(a =0; a< $scope.songOrder.length; a++){
                if ($scope.songOrder[a].id === $scope.song.id){
                    index = a;
                }
            }
           console.log(index)
           console.log($scope.song)
           console.log($scope.songOrder)
          if ((index === ($scope.songOrder.length -1)) && $scope.repeatToggleMultiple === true){
           $scope.song = $scope.songOrder[0]
           $scope.loadSong($scope.song)
           }
           else if (index <= ($scope.songOrder.length -2)){
               $scope.song = $scope.songOrder[index + 1]
               $scope.loadSong($scope.song)
             }
    }

    $scope.previousSong = function(){
               if($scope.repeatToggleSingle === true){
                                       $scope.loadSong($scope.song);
                                       return;
                           }
           var index = -1;
                             for(a =0; a< $scope.songOrder.length; a++){
                                 if ($scope.songOrder[a].id === $scope.song.id){
                                     index = a;
                                 }

                             }
        if (index>0){
            $scope.song = $scope.songOrder[index-1]
            $scope.loadSong($scope.song )
        }


    }

    $scope.showLyrics = function(){
            var dialog = null;
            var lyrics = $scope.song.lyrics
            dialog = $dialogs.create('lyricsModal.html','modalCtrl',{lyrics},{key: false, back: 'static'});

    }

    $scope.removeSongFromLibrary = function(song){
        var library = $scope.library;
        console.log(library.playlistId)
        console.log(song.id)
        $http({
              url: '/api/removeSongFromLibrary',
              method: "POST",
              data: { "songId" : song.id,
                    "playlistId" : library.playlistId
            }
        })
        .then(function(response) {
            for(i= 0; i<$scope.library.songs.length; i++){
                if (song.id === $scope.library.songs[i].id){
                    console.log($scope.library.songs[i])
                    $scope.library.songs.splice(i, 1);
                    break;
                }
            }
            song.inLibrary = false;
        });
    }

    $scope.saveLang = function(type) {
        console.log("Type is: " + type);
        var num = parseInt(type);
        console.log("Num is: " + num);
        var data = {
            langNum: num
        }
        $http.post("/api/changeLanguage", data).success(function (response) {
            console.log("Success!");
        })
        .error(function (response) {
            console.log("It failed :c");
        })
    }

    $scope.resetQueue = function() {
        var queue = $scope.queue;
        console.log(queue.playlistId)
        var i = 0;
        for(i = 0; i < $scope.queue.songs.length;i++)
        {
            $http({
                  url: '/api/removeSongFromLibrary',
                  method: "POST",
                  data: { "songId" : $scope.queue.songs[i].id,
                        "playlistId" : queue.playlistId
                }
            })
            .then(function(response) {
                console.log("removed from queue")
            });
        }
        queue.songs = [];
    }


    $scope.loadHomePage = function() {
        $http.get("api/getHomepage/").success(function (response) {
            $scope.popularAlbumList = response.popularAlbumList;
            $scope.popularSongList = response.popularSongList;
            console.log("MEEMSES");
            console.log($scope.popularAlbumList);
            console.log($scope.popularSongList);
            $scope.songs = response.popularSongList;
            $scope.songOrder = JSON.parse(JSON.stringify($scope.songs));
            for(i=0; i<$scope.songs.length; i++){
                $scope.songs[i].length = $scope.convertTime($scope.songs[i].length);
            }
       });
    }

    $scope.addSong = function(){
        $http.get("api/getArtistAlbums").success(function (response) {
            console.log(response)
             dialog = $dialogs.create('addSongToAlbumModal.html','modalCtrl',{response},{key: false, back: 'static'});
                        dialog.result.then(function(song)
                        {
                                var fd = new FormData();
                                fd.append("songFile", song.file[0]);
                                fd.append("songTitle", song.songName);
                                fd.append("ALBUM_ID", song.albumId);
                                fd.append("lyrics", song.lyrics);
                                fd.append("songLength", song.length);
                                var request = {
                                    method: 'POST',
                                    url: '/api/newSong/',
                                    data: fd,
                                    transformRequest: angular.identity,
                                    headers: {
                                        'Content-Type': undefined
                                    }
                                };

                                $http(request)
                                    .success(function (d) {
//                                    $scope.playlists.push(d);
//                                    console.log($scope.playlists)
//                                    console.log(d)
                                    })
                                    .error(function () {

                                    });

        });
    });
    }
});





