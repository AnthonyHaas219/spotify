package com.spotify.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "passwords")
public class Password {
	@Id
	@Column(name="PW_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int passwordId;
	@Column(name="salt")
	private byte[] salt;
	@Column(name="password")
	private byte[] password;//hashed password
	@Column(name="lastDateChg")
	private Date lastDateChanged;
	@OneToOne
	@JoinColumn(name = "USER_ID", nullable = false, referencedColumnName = "USER_ID")
	private User user;
	
	//default constructor on class
	public Password() {
		this.passwordId = 0;
		this.salt = null;
		this.lastDateChanged = null;
	}
	
	public Password(String newPassword) {
		this.salt = this.createSalt();
		this.password = this.hashPassword(newPassword);
		java.util.Date date = new java.util.Date();
		lastDateChanged = new java.sql.Date(date.getTime());
	}
	public byte[] getSalt() {
		return salt;
	}
	
	public void setSalt(byte[] salt) {
		this.salt = salt;
	}
	
	public byte[] getPassword() {
		return password;
	}
	
	public void setPassword(byte[] password) {
		this.password = password;
	}
	
	public Date getLastDateChanged() {
		return lastDateChanged;
	}
	
	public void setLastDateChanged(Date lastDateChanged) {
		this.lastDateChanged = lastDateChanged;
	}
	
	public int getPasswordId() {
		return passwordId;
	}
	
	public void setPasswordId(int passwordId) {
		this.passwordId = passwordId;
	}

	@JsonIgnore
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public void changePassword(String newPass)
	{
		this.salt = this.createSalt();
		this.password = this.hashPassword(newPass);
	}
	
	/*used to see if password that is passed is same as password in user profile*/
	public boolean comparePassword(String inputPassword) {
		byte[] inputHash = this.hashPassword(inputPassword);
		String dbPass = hashToHex(this.password);
		String inputPass = hashToHex(inputHash);
		if(dbPass.compareTo(inputPass) == 0)
			return true;
		return false;
	}
	
	/*used to hash the password*/
	public byte[] hashPassword(String inputPassword) {
		 byte[] hashedPassword;
		 try {
	         MessageDigest md = MessageDigest.getInstance("SHA-256");
	         md.update(this.salt);  
	         hashedPassword = md.digest(inputPassword.getBytes());
	         return hashedPassword;
	     } 
	     catch (NoSuchAlgorithmException e) {
	    	 e.printStackTrace();
	     }
		return null;
	}
	
	/*used to create salt for hashing passwords*/
	public byte[] createSalt() {
		SecureRandom random = new SecureRandom();
		byte[] newSalt = new byte[16];
		random.nextBytes(newSalt);
		return newSalt;
	}
	
	private String hashToHex(byte[] hashedValue) {
		String hexValue = "";
		for (int i = 0; i < hashedValue.length; i++) {
			hexValue += Integer.toHexString((0x000000ff & hashedValue[i]) | 0xffffff00).substring(6);
		}
		return hexValue;
	}
}
