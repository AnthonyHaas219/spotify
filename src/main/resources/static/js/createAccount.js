angular.module("mySpotify").controller("addAccountCtrl", function($scope, $rootScope, $http, $routeParams)
{   $scope.createArtistSuccess = false;
    $scope.createUserSuccess = false;
    $scope.createUserFailure = false;
    $scope.createArtistFailure = false;
    $scope.userEmail = '';
    $scope.artistEmail = '';
    $scope.createUser = function() {
        console.log($scope.newemail);
        console.log($scope.newdate);
        console.log($scope.newpassword);
        console.log($scope.newfirstName);
        console.log($scope.newlastName);

        var data = {
            email: $scope.newemail,
            date: $scope.newdate,
            Password1: $scope.newpassword,
            FirstName: $scope.newfirstName,
            LastName: $scope.newlastName,
            isArtist: false
        };

        $http.post("/api/createUser", data).success(function (response) {
            console.log("success!");
             $scope.createUserFailure = false;
             $scope.createUserSuccess = true;
             $scope.userEmail = $scope.newemail;
             console.log($scope);

            $scope.newemail = "";
            $scope.newdate = "";
            $scope.newpassword = "";
            $scope.newpassword2 = "";
            $scope.newfirstName = "";
            $scope.newlastName = "";
        }).error(function (response) {
            console.log("error");
            $scope.createUserFailure = true;
            $scope.createUserSuccess = false;
        })
    }

    $scope.createArtist = function() {
        var data = {
            email: $scope.newArtistEmail,
            date: $scope.newArtistDate,
            Password1: $scope.newArtistPassword,
            FirstName: $scope.newgroupName,
            LastName: $scope.newgroupName,
            groupName: $scope.newgroupName,
            bio: $scope.newbio,
            isArtist: true,
        }

        $http.post("/api/createUser", data).success(function (response) {
            console.log("success!");
             $scope.createArtistFailure = false;
             $scope.createArtistSuccess = true;
             $scope.artistEmail = $scope.newArtistEmail;
            $scope.newArtistEmail = "";
            $scope.newArtistDate = "";
            $scope.newArtistPassword = "";
            $scope.newArtistPassword2 = "";
            $scope.newfirstName = "";
            $scope.newlastName = "";
            $scope.newgroupName = "";
            $scope.newbio = "";
        }).error(function (response) {
            console.log("error");
            $scope.createArtistFailure = true;
            $scope.createArtistSuccess = false;
        })

    }
});