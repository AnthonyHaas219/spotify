package com.spotify.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.spotify.entities.User;

public class CheckLoginStatusInterceptor extends HandlerInterceptorAdapter {
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,  Object handler) throws Exception 
	{
		HttpSession session = request.getSession();
		String path = request.getRequestURI().substring(request.getContextPath().length());
		
		if(path.equals("/login") || path.equals("/") || path.equals("loginpage.html") 
				|| path.equalsIgnoreCase("/verifyUser") || path.equalsIgnoreCase("/redirect")
				|| path.equalsIgnoreCase("/signup")){
            return true;
        }
		if(((User) session.getAttribute("user")) == null) {
			response.sendRedirect("/redirect");
			return false;
		}
	    return true;
	}
}
