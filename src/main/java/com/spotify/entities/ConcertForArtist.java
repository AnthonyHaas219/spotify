package com.spotify.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="ConcertForArtists")
public class ConcertForArtist implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@OneToOne
	@JoinColumn(name = "CONCERT_ID", nullable = false, referencedColumnName = "CONCERT_ID")
	private Concert c;
	@Id
	@OneToOne
	@JoinColumn(name = "ARTIST_ID", nullable = false, referencedColumnName = "ARTIST_ID")
	private Artist a;
	
	public ConcertForArtist() {}
	
	public ConcertForArtist(Concert c, Artist a) {
		this.c = c;
		this.a = a;
	}
	
	public Concert getC() {
		return c;
	}
	
	public Artist getA() {
		return a;
	}
	
	public void setC(Concert c) {
		this.c = c;
	}
	
	public void setA(Artist a) {
		this.a = a;
	}
}
