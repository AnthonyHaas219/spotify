angular.module("mySpotify").controller("playlistCtrl", function($scope, $window, $rootScope, $dialogs, $http, $routeParams) {

    var id = parseInt($routeParams.id);

    $scope.loadInfo = function() {
        $http.get("api/playlist/" + id).success(function (response) {
            $scope.title = response.title;
            $scope.description = response.description;
            $scope.imageFile = response.imageLoc;
            $scope.songs = response.songs;
            $scope.owners = response.owners;
            $scope.dataAdded = response.dateAdded;
            $scope.owners = response.owners;
            $scope.followerCount =  response.followers.length - 1;
            $scope.following = false;
            $scope.ownerToggle = false;
            $scope.songs.inLibrary = false;
            console.log("MEEEEEMMMMES");
            var i=0;
            for(j=0;j<$scope.songs.length;j++)
            {
                $scope.songs[j].inLibrary = false;
                console.log($scope.songs[j].artists);
            }
            for(i=0; i<$scope.playlists.length; i++){
                if (id === $scope.playlists[i].playlistId){
                    $scope.following = true;
                    break;
                }
            }

            i=0;
            for(i=0; i<$scope.songs.length; i++){
                $scope.songs[i].length = $scope.$parent.convertTime($scope.songs[i].length);
            }

            i=0;
            for(i=0; i<$scope.owners.length; i++){
               if($scope.$parent.userID == $scope.owners[i].id){
                    $scope.ownerToggle = true;
                    break;
                }
            }
            for(i=0;i<$scope.$parent.library.songs.length;i++){
                for(j=0;j<$scope.songs.length;j++)
                {
                    if($scope.$parent.library.songs[i].id == $scope.songs[j].id){
                        $scope.songs[j].inLibrary = true;
                        break;
                    }
                }
                continue;
            }
        });
    }

    $scope.setFollow = function(){
        if($scope.following === true){
        $http.post("api/unfollowPlaylist").success(function (response) {
            $scope.following = false;
            $scope.followerCount -= 1;
            if($scope.followerCount < 1){
                $scope.followerCount = 0;
            }
            var i =0;
            for(i= 0; i<$scope.playlists.length; i++){
                console.log($scope.playlists[i]);
                if (id === $scope.playlists[i].playlistId){
                    $scope.playlists.splice(i, 1);
                    break;
                }
            }
        }).error(function(response){
        });
        }
        else{
        $http.post("api/followPlaylist").success(function (response) {
          $scope.following = true;
          $scope.followerCount += 1;
          $scope.playlists.push({"title"  : $scope.title, "playlistId":  id});
        }).error(function(response){

        });
        }
    }

    $scope.deletePlaylist = function(){
        if($scope.following === true){
        $http.post("api/unfollowPlaylist").success(function (response) {
            $scope.following = false;
            $scope.followerCount -= 1;
            if($scope.followerCount < 1){
                $scope.followerCount = 0;
            }
            var i =0;
            for(i= 0; i<$scope.playlists.length; i++){
                console.log($scope.playlists[i]);
                if (id === $scope.playlists[i].playlistId){
                    $scope.playlists.splice(i, 1);
                    break;
                }
            }
            $window.location.href = '#';
        }).error(function(response){
        });
        }
        else{
        $http.post("api/followPlaylist").success(function (response) {
          $scope.following = true;
          $scope.followerCount += 1;
          $scope.playlists.push({"title"  : $scope.title, "playlistId":  id});
        }).error(function(response){

        });
        }
    }

    $scope.setTitleEditable = function(){
        $scope.editedTitle = $scope.title;
        $scope.titleEditToggle = true;
    }

    $scope.titleEditSave = function(){
        var param = {newPlaylistName: $scope.editedTitle}
        $http.post("api/renamePlaylist", param).success(function (response) {
            $scope.title = $scope.editedTitle;
            var i =0;
            for(i= 0; i<$scope.playlists.length; i++){
                if (id === $scope.playlists[i].playlistId){
                    $scope.playlists[i].title = $scope.editedTitle;
                    break;
                }
            }
        }).error(function(response){
        });
        $scope.titleEditToggle = false;
    }

    $scope.titleEditCancel = function(){
        $scope.editedTitle= '';
        $scope.titleEditToggle = false;
    }

    $scope.playSong = function(song){
        $scope.$parent.songLoaded = true;
        $scope.$parent.playToggle = true;
        $scope.$parent.songLocation = "songs/" + song.songLocation;
        $scope.$parent.artists = song.artists;
        $scope.$parent.songName = song.title;
        $scope.$parent.songs = $scope.songs;
        $scope.$parent.songOrder = JSON.parse(JSON.stringify($scope.songs));
        $scope.songOrder = $scope.songs;
        console.log($scope.$parent.songOrder);
        console.log($scope.songs);
        $scope.$parent.song = song;
        $scope.$parent.loadSong(song);

////         var index = $scope.$parent.songOrder.indexOf(song);
//          var index = -1;
//                     for(a =0; a< $scope.$parent.songOrder.length; a++){
//                         if ($scope.$parent.songOrder[a].id === song.id){
//                             index = a;
//                         }
//
//                     }
//                 if(index <= ($scope.$parent.songOrder.length -2)){
//                               $scope.$parent.nextToggle = true;
//                       }
//         document.getElementById('mySong').currentTime  = 0;
    }

    function shuffle(songs) {
        for (var a = songs.length - 1; a > 0; a--) {
            var b = Math.floor(Math.random() * (a + 1));
            var tempSong = songs[a];
            songs[a] = songs[b];
            songs[b] = tempSong;
        }
        return songs;
    }

    $scope.playAll = function(){


       $scope.$parent.songs = $scope.songs;
       console.log($scope.songs)
       $scope.$parent.songOrder = JSON.parse(JSON.stringify($scope.songs));
       if($scope.$parent.shuffleToggle === true){
                $scope.$parent.songOrder = shuffle($scope.$parent.songOrder)
       }
       console.log($scope.$parent.songOrder)
       var song = $scope.$parent.songOrder[0]
       $scope.$parent.song = song;
        $scope.$parent.songLoaded = true;
        $scope.$parent.playToggle = true;
        $scope.$parent.songLocation = "songs/" + song.songLocation;
        $scope.$parent.artists = song.artists;
        $scope.$parent.songName = song.title;
        $scope.$parent.loadSong(song);

//         var index = $scope.$parent.songOrder.indexOf(song);
//         console.log(index)
//         console.log($scope.$parent.songOrder.length)
//                 if(index <= ($scope.$parent.songOrder.length -2)){
//                                console.log("here")
//                                console.log($scope.$parent)
//                               $scope.$parent.nextToggle = true;
//                       }
//        document.getElementById('mySong').currentTime  = 0;
    }

    $scope.removeSong = function(id){
       $http({
              url: '/api/removeSongFromPlaylist',
              method: "POST",
              data: { "song" : id}
          })
          .then(function(response) {
                  // success
            var i =0;
            for(i= 0; i<$scope.songs.length; i++){
                console.log($scope.songs);
                if (id === $scope.songs[i].id){
                    console.log($scope.songs[i])
                    $scope.songs.splice(i, 1);
                    break;
                }
            }

        });
    }

    $scope.addSong = function(song)
    {
        var playlists = $scope.playlists;
        var selectedPlaylists = [];
        console.log($scope.playlists)
        var i =0;
        for(i = 0; i< playlists.length; i++)
        {
            var j = 0;
            var owner = false;
            console.log("AAAAAAAAAAAAAAAAAAAAAAAAAA");
            console.log(playlists[i].songs.indexOf(song));
            console.log("BBBBBBBBBBBBBBBBBBBBBBBBB");
            for( j = 0; j< playlists[i].owners.length; j++)
            {
                if((playlists[i].owners[j].id === $scope.$parent.userID) &&  playlists[i].songs.indexOf(song) === -1)
                {
                    console.log("HERE!")
                    owner = true;
                }
                if (owner === true){
                    console.log("here!")

                    selectedPlaylists.push(playlists[i]);
                }
            }
            }
            console.log(selectedPlaylists);
            dialog = $dialogs.create('addToPlaylist.html','modalCtrl',{selectedPlaylists},{key: false, back: 'static'});
            dialog.result.then(function(playlistId)
            {
                console.log(playlistId)
                console.log("here!");
                $http({
                    url: '/api/addSongToPlaylist',
                    method: "POST",
                    data: { "songId" : song.id,
                        "playlistId" : playlistId
                    }
                })
                .then(function(response) {
//                    if(playlistId = id){
//                        $scope.songs.push(song)
//                        console.log("nice")
//                    }
                });
            })

    }

    $scope.addSongToQueue = function(song){
        var queue = $scope.$parent.queue;
        console.log(queue.playlistId)
        console.log(song.id)
        $http({
            url: '/api/addSongToPlaylist',
            method: "POST",
            data: { "songId" : song.id,
                "playlistId" : queue.playlistId
            }
        })
        .then(function(response) {
            $scope.$parent.queue.songs.push(song)
            console.log("nice")        
        });
    }

    $scope.addSongToLibrary = function(song){
        var library = $scope.$parent.library;
        console.log(library.playlistId)
        console.log(song.id)
        $http({
            url: '/api/addSongToPlaylist',
            method: "POST",
            data: { "songId" : song.id,
                "playlistId" : library.playlistId
            }
        })
        .then(function(response) {
            $scope.$parent.library.songs.push(song)
            song.inLibrary = true;
            console.log("nice")
        });
    }

    $scope.removeSongFromLibrary = function(song){
        var library = $scope.$parent.library;
        console.log(library.playlistId)
        console.log(song.id)
        $http({
              url: '/api/removeSongFromLibrary',
              method: "POST",
              data: { "songId" : song.id,
                    "playlistId" : library.playlistId
            }
        })
        .then(function(response) {
            // success
            if(id = $scope.$parent.library.playlistId){
                for(i= 0; i<$scope.$parent.library.songs.length; i++){
                    if (song.id === $scope.$parent.library.songs[i].id){
                        console.log($scope.$parent.library.songs[i])
                        $scope.$parent.library.songs.splice(i, 1);
                        break;
                    }
                }
            }
            song.inLibrary = false;
        });
    }

    $scope.removeSongFromQueue = function(song){
        var queue = $scope.$parent.queue;
        console.log(queue.playlistId)
        console.log(song.id)
        $http({
              url: '/api/removeSongFromLibrary',
              method: "POST",
              data: { "songId" : song.id,
                    "playlistId" : queue.playlistId
            }
        })
        .then(function(response) {
            // success
            if(id = $scope.$parent.queue.playlistId){
                for(i= 0; i<$scope.$parent.queue.songs.length; i++){
                    if (song.id === $scope.$parent.queue.songs[i].id){
                        console.log($scope.$parent.queue.songs[i])
                        $scope.$parent.queue.songs.splice(i, 1);
                        break;
                    }
                }
            }
            song.inLibrary = false;
        });
    }

    $scope.changePic = function(){
             var dialog = null;

            dialog = $dialogs.create('addProfileImageModal.html','modalCtrl',{},{key: false, back: 'static'});


            dialog.result.then(function(playlist){
                var fd = new FormData();
                fd.append("playlistImage", playlist.imageFile[0]);
                var request = {
                    method: 'POST',
                    url: '/api/setPlaylistImage',
                    data: fd,
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                };
                console.log("here")
                $http(request)
                    .then(function (d) {
                        console.log("here")
                        console.log("!!!")
//                        console.log(d);
                        console.log(d);
                       $scope.imageFile = d.data.imageLoc;
                    })

            });

    }
});