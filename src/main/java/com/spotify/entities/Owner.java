package com.spotify.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="owner")
public class Owner {
	@Id
	@Column(name="OWNER_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int ownerID;
	@OneToOne
	@JoinColumn(name = "USER_ID", nullable = false, referencedColumnName = "USER_ID")
	private User u;
	@OneToOne
	@JoinColumn(name = "PLAYLIST_ID", nullable = true, referencedColumnName = "PLAYLIST_ID")
	private Playlist p;
	@OneToOne
	@JoinColumn(name = "ALBUM_ID", nullable = true, referencedColumnName = "ALBUM_ID")
	private Album a;
	
	public Owner() {}
	
	public Owner(User uId, Playlist pId, Album aId) {
		this.a = aId;
		this.p = pId;
		this.u = uId;
	}
	
	public int getOwnerID() {
		return ownerID;
	}
	
	public User getU() {
		return u;
	}
	
	public Playlist getP() {
		return p;
	}
	
	public Album getA() {
		return a;
	}
	
	public void setOwnerID(int ownerID) {
		this.ownerID = ownerID;
	}
	
	public void setU(User u) {
		this.u = u;
	}
	
	public void setP(Playlist p) {
		this.p = p;
	}
	
	public void setA(Album a) {
		this.a = a;
	}
}
