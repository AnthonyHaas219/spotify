package com.spotify.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="songincollection")
public class SongInCollection {
	
	@Id
	@Column(name="SONG_COLLECTION_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@OneToOne
	@JoinColumn(name = "SONG_ID", nullable = true, referencedColumnName = "SONG_ID")
	private Song s;
	@OneToOne
	@JoinColumn(name = "ALBUM_ID", nullable = true, referencedColumnName = "ALBUM_ID")
	private Album a;
	@OneToOne
	@JoinColumn(name = "PLAYLIST_ID", nullable = true, referencedColumnName = "PLAYLIST_ID")
	private Playlist p;
	
	public SongInCollection() {}
	
	public SongInCollection(Song s, Album a, Playlist p) {
		this.s = s;
		this.a = a;
		this.p = p;
	}
	
	public int getId() {
		return id;
	}
	
	public Song getS() {
		return s;
	}
	
	public Album getA() {
		return a;
	}
	
	public Playlist getP() {
		return p;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setS(Song s) {
		this.s = s;
	}
	
	public void setA(Album a) {
		this.a = a;
	}
	
	public void setP(Playlist p) {
		this.p = p;
	}
}
