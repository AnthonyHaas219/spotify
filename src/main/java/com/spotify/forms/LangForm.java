package com.spotify.forms;

public class LangForm {

    public int langNum;

    public int getLangNum() {
        return langNum;
    }

    public void setLangNum(int langNum) {
        this.langNum = langNum;
    }
}
