package com.spotify.controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.spotify.entities.Song;
import com.spotify.entities.User;
import com.spotify.entityManagers.UserManager;
import com.spotify.services.AlbumServices;
import com.spotify.services.ArtistServices;
import com.spotify.services.GeneralServices;
import com.spotify.services.PlaylistServices;
import com.spotify.services.SongServices;
import com.spotify.services.UserServices;

@Controller
public class LoginController {

    @Autowired
    @Qualifier("userServices")
    private UserServices us;
    @Autowired
    @Qualifier("playlistServices")
    protected PlaylistServices playlistServices; 
    @Autowired
    @Qualifier("songServices")
    protected SongServices songServices;
    @Autowired
    @Qualifier("userServices")
    protected UserServices userServices; 
    @Autowired
    @Qualifier("generalServices")
    protected GeneralServices generalServices;
    @Autowired
    @Qualifier("artistServices")
    protected ArtistServices artistServices;
    @Autowired
    @Qualifier("albumServices")
    protected AlbumServices albumServices;

    @RequestMapping(value ="/", method = RequestMethod.POST)
    public String home(HttpSession httpSession) {
        return "redirect:/";
    }

    @RequestMapping(value = "/browse/**", method = RequestMethod.GET)
    public String browse() {
        return "browse.html";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String welcome(HttpSession httpSession, HttpServletRequest req, HttpServletResponse res) {
    	return "loginpage.html";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String displayHome(HttpSession httpSession) {
        if ((httpSession.getAttribute("id") == null) || (((User) httpSession.getAttribute("user")) == null)){
            return "loginpage.html";
        }
        return "browse.html";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutGet(HttpSession httpSession) 
    {
    	User tempUser = null;
        httpSession.removeAttribute("id");
        tempUser = (User) httpSession.getAttribute("user");
        us.switchLogin(tempUser);
        httpSession.removeAttribute("user");
        return "redirect:/";
    }
    
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public String logout(HttpSession httpSession) 
    {
    	User tempUser = null;
        httpSession.removeAttribute("id");
        tempUser = (User) httpSession.getAttribute("user");
        us.switchLogin(tempUser);
        httpSession.removeAttribute("user");
        return "redirect:/";
    }
    
    @RequestMapping(value = "/verifyUser", method = RequestMethod.POST)
    public String verifyUser(HttpSession httpSession,HttpServletRequest req, HttpServletResponse res) 
    {
    	User tempUser = null;
    	UserManager um = UserManager.getUserManager();
    	String userEmail = req.getParameter("userName");
    	String userPass = req.getParameter("Passwords"); 	
    	//try to see if user is valid
    	tempUser = us.verifyUser(userEmail, userPass);
    	try 
		{
    	    if(tempUser != null)
	    	{
	        	playlistServices.getPlaylist(tempUser.getQueue().getPlaylistId());
	        	playlistServices.getPlaylist(tempUser.getLibrary().getPlaylistId());
	        	tempUser.getLibrary().setSongs(songServices.getSongsByPlaylistId(tempUser.getLibrary().getPlaylistId()));
	        	tempUser.getQueue().setSongs(songServices.getSongsByPlaylistId(tempUser.getQueue().getPlaylistId()));
	        	for(Song s : tempUser.getLibrary().getSongs()) {
	            	s.setArtists(artistServices.getArtistBySong(s.getId()));
	            }
	        	for(Song s : tempUser.getQueue().getSongs()) {
	            	s.setArtists(artistServices.getArtistBySong(s.getId()));
	            }
	    		httpSession.setAttribute("user", tempUser);
	    		um.addUser(tempUser);
	    		res.sendRedirect("/browse");
	    	}
	    	else
	    	{
	    		res.sendRedirect("/login");
	    	}
		} 
    	catch (IOException e) 
    	{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
    }
    
    @RequestMapping(value = "/redirect", method = RequestMethod.GET)
    public String redirect() {
        return "redirect:/login";
    }
}
