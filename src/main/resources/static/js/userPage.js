$(function () 
{
    $("#publicPlaylistButton").click(function() {
        $('#userOverview').hide();
        $('#followingPlaylist').hide();
        $('#followingArtist').hide();
        $('#following').hide();
        $('#followers').hide();
        $('#publicPlaylist').show();
    });

    $("#followingPlaylistButton").click(function() {
        $('#userOverview').hide();
        $('#followingPlaylist').show();
        $('#followingArtist').hide();
        $('#following').hide();
        $('#followers').hide();
        $('#publicPlaylist').hide();
    });

    $("#followingArtistButton").click(function() {
        $('#userOverview').hide();
        $('#followingPlaylist').hide();
        $('#followingArtist').show();
        $('#following').hide();
        $('#followers').hide();
        $('#publicPlaylist').hide();
    });

    $("#followingButton").click(function() {
        $('#userOverview').hide();
        $('#followingPlaylist').hide();
        $('#followingArtist').hide();
        $('#following').show();
        $('#followers').hide();
        $('#publicPlaylist').hide();
    });

    $("#followersButton").click(function() {
        $('#userOverview').hide();
        $('#followingPlaylist').hide();
        $('#followingArtist').hide();
        $('#following').hide();
        $('#followers').show();
        $('#publicPlaylist').hide();
    });

    $("#overViewButton").click(function() {
        $('#userOverview').show();
        $('#followingPlaylist').hide();
        $('#followingArtist').hide();
        $('#following').hide();
        $('#followers').hide();
        $('#publicPlaylist').hide();
    });

    $("#artistConcertButton").click(function() {
        $('#artistOverview').hide();
        $('#aboutArtist').hide();
        $('#artistConcert').show();
    });

    $("#aboutArtistButton").click(function() {
        $('#artistOverview').hide();
        $('#aboutArtist').show();
        $('#artistConcert').hide();
    });

    $("#artistOverviewButton").click(function() {
        $('#artistOverview').show();
        $('#aboutArtist').hide();
        $('#artistConcert').hide();
    });
});